package SBCR.JitsiSimulator;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class Configuration {

	// May be

	public String protocol;
	public String listenport;
	public String transportprotocol;
	public String encryption;
	public String sipzrtpattribute;
	public int maxpeers;
	public int mtu;
	public int defaultcallrate;
	public int maxreceivecallrate;
	public int maxtransmitcallrate;
	public String audiocodec;
	public String videocodec;
	public int maxresolution;
	
	


	
	public Configuration() {
		// getDefaultConfigurations(); // TODO Auto-generated constructor stub
	}

	// {AIM}-> None Audio{Auto},Video{Auto}
	// {GoogleTalk}-> Audio{Auto,
	// opus-48000,SILK-24000,SILK-16000,G722-16000,speex-32000,speex-16000,PCMU-8000,PCMA-8000,iLBC-8000,GSM-8000,speex-8000,telephone-event-80000},
	// Video{Auto, h264}
	// {iPPi}-> Audio{Auto,
	// opus-48000,SILK-24000,SILK-16000,G722-16000,speex-32000,speex-16000,PCMU-8000,PCMA-8000,iLBC-8000,GSM-8000,speex-8000,telephone-event-80000},
	// Video{Auto, h264}
	// {iptel.org}-> Audio{Auto,
	// opus-48000,SILK-24000,SILK-16000,G722-16000,speex-32000,speex-16000,PCMU-8000,PCMA-8000,iLBC-8000,GSM-8000,speex-8000},
	// Video{Auto, h264}
	// {IRC}-> None Audio{Auto},Video{Auto}
	// {SIP}-> Audio{Auto,
	// opus-48000,SILK-24000,SILK-16000,G722-16000,speex-32000,speex-16000,PCMU-8000,PCMA-8000,iLBC-8000,GSM-8000,speex-8000,telephone-event-80000},
	// Video{Auto, h264}
	// {ICQ}-> None Audio{Auto},Video{Auto}

	public static Configuration getDefaultConfigurations() {
		Configuration receiverConfiguration = new Configuration();

	
		
		receiverConfiguration.protocol="Auto";
		receiverConfiguration.listenport="On";
		receiverConfiguration.transportprotocol="UDP";
		receiverConfiguration.encryption="BestEffort";
		receiverConfiguration.sipzrtpattribute="true";
		receiverConfiguration.maxpeers=3;
		receiverConfiguration.mtu=2000;
		receiverConfiguration.defaultcallrate=6000;
		receiverConfiguration.maxreceivecallrate=6000;
		receiverConfiguration.maxtransmitcallrate=6000;
		receiverConfiguration.audiocodec="X";
		receiverConfiguration.videocodec="Y";
		receiverConfiguration.maxresolution=1080;
		
		
		return receiverConfiguration;
	}


	public static Configuration[] ReadConfigurationFromCSVFile(String sourchFilePath) throws IOException {
		    List<String> lines= Files.readAllLines(Paths.get(sourchFilePath), StandardCharsets.UTF_8);
		    Configuration caller= new Configuration();
		    Configuration callee1= new Configuration();
		    Configuration callee2= new Configuration();
		    
		    boolean readflag = false;
		    String allConfigurations="";
		    for (String line : lines) {
//		    	System.out.println(line);
				String[] configurationArray= line.split(";");
//				System.out.println("Length is:- "+configurationArray.length);
//				if (true) {
					if (!line.contains("protocol") && configurationArray[39].contains("NotDone") && !readflag ) {
						caller.protocol=configurationArray[0];
						callee1.protocol=configurationArray[1];
						callee2.protocol=configurationArray[2];
						caller.listenport=configurationArray[3];
						callee1.listenport=configurationArray[4];
						callee2.listenport=configurationArray[5];
						caller.transportprotocol=configurationArray[6];
						callee1.transportprotocol=configurationArray[7];
						callee2.transportprotocol=configurationArray[8];
						caller.encryption=configurationArray[9];
						callee1.encryption=configurationArray[10];
						callee2.encryption=configurationArray[11];
						caller.sipzrtpattribute=configurationArray[12];
						callee1.sipzrtpattribute=configurationArray[13];
						callee2.sipzrtpattribute=configurationArray[14];
						caller.maxpeers=Integer.parseInt(configurationArray[15]);
						callee1.maxpeers=Integer.parseInt(configurationArray[16]);
						callee2.maxpeers=Integer.parseInt(configurationArray[17]);
						caller.mtu=Integer.parseInt(configurationArray[18]);
						callee1.mtu=Integer.parseInt(configurationArray[19]);
						callee2.mtu=Integer.parseInt(configurationArray[20]);
						caller.defaultcallrate=Integer.parseInt(configurationArray[21]);
						callee1.defaultcallrate=Integer.parseInt(configurationArray[22]);
						callee2.defaultcallrate=Integer.parseInt(configurationArray[23]);
						caller.maxreceivecallrate=Integer.parseInt(configurationArray[24]);
						callee1.maxreceivecallrate=Integer.parseInt(configurationArray[25]);
						callee2.maxreceivecallrate=Integer.parseInt(configurationArray[26]);
						caller.maxtransmitcallrate=Integer.parseInt(configurationArray[27]);
						callee1.maxtransmitcallrate=Integer.parseInt(configurationArray[28]);
						callee2.maxtransmitcallrate=Integer.parseInt(configurationArray[29]);
						caller.audiocodec=configurationArray[30];
						callee1.audiocodec=configurationArray[31];
						callee2.audiocodec=configurationArray[32];
						caller.videocodec=configurationArray[33];
						callee1.videocodec=configurationArray[34];
						callee2.videocodec=configurationArray[35];
						caller.maxresolution=Integer.parseInt(configurationArray[36]);
						callee1.maxresolution=Integer.parseInt(configurationArray[37]);
						callee2.maxresolution=Integer.parseInt(configurationArray[38]);
						line = line.replaceAll("NotDone", "Done");
						readflag=true;
					} //if
//				}// if 
				allConfigurations+=line+ "\n";
			}//for
		    Files.write(Paths.get(sourchFilePath), allConfigurations.getBytes());
		Configuration[] systems= {caller,callee1,callee2};
		return systems;

	}
	
	public static void main(String[] args) throws IOException {

		Configuration[] systems=ReadConfigurationFromCSVFile("Configurations/ExecutedConfigurations/VAR.csv");
System.out.println(systems[0].protocol);
		
		
		
//		System.out.println(configuraiton.AudioCodec);
		// TODO Auto-generated method stub

	}

//	@Override
//	public String toString() {
//		return "My_Configuration [desktopSharingSettings=" + ", SIPListenPort=" + SIPListenPort + ", TransportProtocol="
//				+ ", encryption=" + Encryption + ", sipZrtpAttribute=" + SipZrtpAttribute + ", receivedCallType="
//				+ MaxPeersAllowedInConferenceCall + ", MTU=" + MTU + ", defaultCallRate=" + DefaultCallRate
//				+ ", MaxReceiveCallRate=" + MaxReceiveCallRate + ", MaxTransmitCallRate=" + MaxTransmitCallRate
//				+ ", audioCodec=" + AudioCodec + ", videoCodec=" + VideoCodec + ", maxResolution=" + MaxResolution
//				+ "]";
//	}

}
