package SBCR.JitsiSimulator;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.List;
import java.util.Random;


public class JitsiSimulation
{


 
    public static boolean CallStatusBasedOnRules(Configuration caller,
        Configuration peer1) throws IOException{
       
        //**************Rule for call*************
          if (caller.protocol.toLowerCase().contains("sip") && peer1.listenport.toLowerCase().contains("off")){ // sip-listen port
        	  Files.write(Paths.get("MostFailedRule.txt"), "R-1\n".getBytes(),StandardOpenOption.APPEND);
              return false;
          }if (caller.encryption.toLowerCase().contains("on") && peer1.encryption.toLowerCase().contains("off")){    // encryption on/off
        	  Files.write(Paths.get("MostFailedRule.txt"), "R-2\n".getBytes(),StandardOpenOption.APPEND);
              return false;
          }if (caller.encryption.toLowerCase().contains("off") && peer1.encryption.toLowerCase().contains("on")){     // encryption off/on
        	  Files.write(Paths.get("MostFailedRule.txt"), "R-3\n".getBytes(),StandardOpenOption.APPEND);
              return false;
//          }if (caller.MaxPeersAllowedInConferenceCall() <= 2 || peer1.MaxPeersAllowedInConferenceCall()<= 2){ // max peers limit
//              Files.write(Paths.get("MostFailedRule.txt"), "R-4\n".getBytes(),StandardOpenOption.APPEND);
//              return false;
          }if (caller.protocol.toLowerCase().contains("sip") && ( (!caller.transportprotocol.toLowerCase().contains("auto")) || (!peer1.transportprotocol.toLowerCase().contains("auto")) ||(! caller.transportprotocol.toLowerCase().equals(peer1.transportprotocol.toLowerCase())))){ // transport protocol should be same
        	  Files.write(Paths.get("MostFailedRule.txt"), "R-5\n".getBytes(),StandardOpenOption.APPEND);
              return false;
          }if (caller.maxresolution>peer1.maxresolution){ // MaxResolution of caller should be less than receiver
              Random rand = new Random(System.currentTimeMillis()*System.nanoTime());
              int c= rand.nextInt(100);
              if (c==1){
            	  Files.write(Paths.get("MostFailedRule.txt"), "R-6\n".getBytes(),StandardOpenOption.APPEND);
                  return false;
              }
          }if (caller.protocol.toLowerCase().contains("sip") && ((!caller.sipzrtpattribute.toLowerCase().equals(peer1.sipzrtpattribute.toLowerCase()))||(!caller.sipzrtpattribute.toLowerCase().contains("auto"))||(!peer1.sipzrtpattribute.toLowerCase().contains("auto")))){ // SipZrtpAttribute should be same for caller and receiver
        	  Files.write(Paths.get("MostFailedRule.txt"), "R-7\n".getBytes(),StandardOpenOption.APPEND);
              return false;
          }if ((caller.protocol.toLowerCase().contains("googletalk")||caller.protocol.toLowerCase().contains("ippi")||caller.protocol.toLowerCase().contains("iptel.org")||caller.protocol.toLowerCase().contains("sip"))  && ((peer1.videocodec.toLowerCase().contains("vp8"))||(peer1.videocodec.toLowerCase().contains("ulpfec")))){// GoogleTalk,iPPi, iptel.org, SIP support h264,red,rtx video codec  {,rtx,ulpfec,VP8}
        	  Files.write(Paths.get("MostFailedRule.txt"), "R-8\n".getBytes(),StandardOpenOption.APPEND);
              return false;
          }if (caller.protocol.toLowerCase().contains("iptel.org")  && (peer1.audiocodec.toLowerCase().contains("telephone-event-80000"))){//  iptel.org does not support telephone-event-80000 audio codec
        	  Files.write(Paths.get("MostFailedRule.txt"), "R-9\n".getBytes(),StandardOpenOption.APPEND);
              return false;
//          }if ((caller.protocol.toLowerCase().contains("aim")||caller.protocol.toLowerCase().contains("irc")||caller.protocol.toLowerCase().contains("icq")) && (!caller.audiocodec.toLowerCase().contains("auto"))){ // AIM,IRC, and ICQ support auto audio codec  
//              Files.write(Paths.get("MostFailedRule.txt"), "R-10\n".getBytes(),StandardOpenOption.APPEND);
//              return false;
//          }if ((caller.protocol.toLowerCase().contains("aim")||caller.protocol.toLowerCase().contains("irc")||caller.protocol.toLowerCase().contains("icq")) && (!caller.videocodec.toLowerCase().contains("auto"))){// AIM,IRC, and ICQ support auto video codec
//              Files.write(Paths.get("MostFailedRule.txt"), "R-11\n".getBytes(),StandardOpenOption.APPEND);
//              return false;

          }if (peer1.audiocodec.toLowerCase().contains("amr-wb-16000")||peer1.audiocodec.toLowerCase().contains("silk-12000,")||peer1.audiocodec.toLowerCase().contains("silk-8000")){// These audio codecs are not supported at the moment with given prootcols
        	  Files.write(Paths.get("MostFailedRule.txt"), "R-12\n".getBytes(),StandardOpenOption.APPEND);
              return false; 
          }if (peer1.videocodec.toLowerCase().contains("vp8")){// These video codecs are not supported at the moment with given prootcol
        	  Files.write(Paths.get("MostFailedRule.txt"), "R-13\n".getBytes(),StandardOpenOption.APPEND);
              return false;          
          }if (caller.mtu > peer1.mtu){ // MTU of caller should not be greater than the the receiver
              Random rand = new Random(System.currentTimeMillis()*System.nanoTime());
              int c= rand.nextInt(100);
              if (c==1){
            	  Files.write(Paths.get("MostFailedRule.txt"), "R-14\n".getBytes(),StandardOpenOption.APPEND);
                  return false;
              }
          }if (caller.defaultcallrate>caller.maxtransmitcallrate){ // callrate should be less than maxtransmit call rate
              Random rand = new Random(System.currentTimeMillis()*System.nanoTime());
              int c= rand.nextInt(100);
              if (c==1){
            	  Files.write(Paths.get("MostFailedRule.txt"), "R-15\n".getBytes(),StandardOpenOption.APPEND);
                  return false;
              }
          }if (caller.defaultcallrate>peer1.maxtransmitcallrate){ // callrate should be less than maxreceived call rate of receiver
              Random rand = new Random(System.currentTimeMillis()*System.nanoTime());
              int c= rand.nextInt(100);
              if (c==1){
            	  Files.write(Paths.get("MostFailedRule.txt"), "R-16\n".getBytes(),StandardOpenOption.APPEND);
                  return false;
              }
          }


   
          


        // private String Protocol;
        // //{SIP,AIM,GoogleTalk,ICQ,iPPi,iptel.org,IRC}
        return true;
    }

/*	
	public static void getCallStatuses(String pathForInitialConfigurations, String pathForFinalConfigurations ) {
		String finalConfigurationsData = "";
		try {
			
			List<String> configurationRows = Files.readAllLines(Paths.get(pathForInitialConfigurations), Charset.defaultCharset());
			
			
			for (int i = 0; i < configurationRows.size(); i++) { //total configuraitons 
				String[] row = configurationRows.get(i).split(";");
//				if (row[42].contains("ToBe")){
			
					Configuration caller= new Configuration();
					Configuration callee1= new Configuration();
					Configuration callee2= new Configuration();
					
					caller.protocol=row[1];
					callee1.protocol=row[2];
					callee2.protocol=row[3];
					
					caller.SIPListenPort=row[4];
					callee1.SIPListenPort=row[5];
					callee2.SIPListenPort=row[6];
					
					caller.transportprotocol=row[7];
					callee1.transportprotocol=row[8];
					callee2.transportprotocol=row[9];
					
					caller.encryption=row[10];
					callee1.encryption=row[11];
					callee2.encryption=row[12];
					
					caller.sipzrtpattribute=row[13];
					callee1.sipzrtpattribute=row[14];
					callee2.sipzrtpattribute=row[15];
					
					caller.MaxPeersAllowedInConferenceCall=Integer.parseInt(row[16]);
					callee1.MaxPeersAllowedInConferenceCall=Integer.parseInt(row[17]);
					callee2.MaxPeersAllowedInConferenceCall=Integer.parseInt(row[18]);
					
					caller.mtu=Integer.parseInt(row[19]);
					callee1.mtu=Integer.parseInt(row[20]);
					callee2.mtu=Integer.parseInt(row[21]);
					
					caller.defaultcallrate=Integer.parseInt(row[22]);
					callee1.defaultcallrate=Integer.parseInt(row[23]);
					callee2.defaultcallrate=Integer.parseInt(row[24]);
					
					caller.maxtransmitcallrate=Integer.parseInt(row[25]);
					callee1.maxtransmitcallrate=Integer.parseInt(row[26]);
					callee2.maxtransmitcallrate=Integer.parseInt(row[27]);
					
					caller.maxtransmitcallrate=Integer.parseInt(row[28]);
					callee1.maxtransmitcallrate=Integer.parseInt(row[29]);
					callee2.maxtransmitcallrate=Integer.parseInt(row[30]);
					
					caller.audiocodec=row[31];
					callee1.audiocodec=row[32];
					callee2.audiocodec=row[33];
					
					caller.videocodec=row[34];
					callee1.videocodec=row[35];
					callee2.videocodec=row[36];
					
					caller.maxresolution=Integer.parseInt(row[37]);
					callee1.maxresolution=Integer.parseInt(row[38]);
					callee2.maxresolution=Integer.parseInt(row[39]);
					
					//calling and getting statuses
	                boolean flagCall1 = CallStatusBasedOnRules(caller,callee1);
	                boolean flagCall2 = CallStatusBasedOnRules(caller,callee2);
	                
	                String[] CallStatus ={ "", "", "" };
	                CallStatus[0] = String.valueOf(flagCall1).toLowerCase()
	                    .replaceAll("true", "Connected")
	                    .replaceAll("false", "Failed");
	                CallStatus[1] = String.valueOf(flagCall2).toLowerCase()
	                    .replaceAll("true", "Connected")
	                    .replaceAll("false", "Failed");
	                CallStatus[2] = CallStatus[0] + CallStatus[1];
	               
	               
		
					//combining data to write back into file
					for (int j = 0; j < row.length; j++) {
						finalConfigurationsData+=row[j]+";";	
					}
					finalConfigurationsData+=CallStatus[0]+";";
					finalConfigurationsData+=CallStatus[1]+";";
					finalConfigurationsData+=CallStatus[2]+"\n";
			
			} // end of looping all initial configurations,i.e., 100			
			Files.write(Paths.get(pathForFinalConfigurations), finalConfigurationsData.getBytes());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

    
    */

	public static void executedAllConfigurations(String sourchFilePath) throws IOException { 
		    List<String> lines= Files.readAllLines(Paths.get(sourchFilePath), StandardCharsets.UTF_8);
		    Configuration caller= new Configuration();
		    Configuration callee1= new Configuration();
		    Configuration callee2= new Configuration();
		    
//		    boolean readflag = false;
		    String allConfigurations="";
		    for (String line : lines) {
		    	System.out.println(line);
				String[] configurationArray= line.split(";");
//				System.out.println("Length is:- "+configurationArray.length);
//				if (true) {
					if (!line.contains("protocol") && configurationArray[39].contains("NotDone")) {
						caller.protocol=configurationArray[0];
						callee1.protocol=configurationArray[1];
						callee2.protocol=configurationArray[2];
						caller.listenport=configurationArray[3];
						callee1.listenport=configurationArray[4];
						callee2.listenport=configurationArray[5];
						caller.transportprotocol=configurationArray[6];
						callee1.transportprotocol=configurationArray[7];
						callee2.transportprotocol=configurationArray[8];
						caller.encryption=configurationArray[9];
						callee1.encryption=configurationArray[10];
						callee2.encryption=configurationArray[11];
						caller.sipzrtpattribute=configurationArray[12];
						callee1.sipzrtpattribute=configurationArray[13];
						callee2.sipzrtpattribute=configurationArray[14];
						caller.maxpeers=Integer.parseInt(configurationArray[15]);
						callee1.maxpeers=Integer.parseInt(configurationArray[16]);
						callee2.maxpeers=Integer.parseInt(configurationArray[17]);
						caller.mtu=Integer.parseInt(configurationArray[18]);
						callee1.mtu=Integer.parseInt(configurationArray[19]);
						callee2.mtu=Integer.parseInt(configurationArray[20]);
						caller.defaultcallrate=Integer.parseInt(configurationArray[21]);
						callee1.defaultcallrate=Integer.parseInt(configurationArray[22]);
						callee2.defaultcallrate=Integer.parseInt(configurationArray[23]);
						caller.maxtransmitcallrate=Integer.parseInt(configurationArray[24]);
						callee1.maxtransmitcallrate=Integer.parseInt(configurationArray[25]);
						callee2.maxtransmitcallrate=Integer.parseInt(configurationArray[26]);
						caller.maxtransmitcallrate=Integer.parseInt(configurationArray[27]);
						callee1.maxtransmitcallrate=Integer.parseInt(configurationArray[28]);
						callee2.maxtransmitcallrate=Integer.parseInt(configurationArray[29]);
						caller.audiocodec=configurationArray[30];
						callee1.audiocodec=configurationArray[31];
						callee2.audiocodec=configurationArray[32];
						caller.videocodec=configurationArray[33];
						callee1.videocodec=configurationArray[34];
						callee2.videocodec=configurationArray[35];
						caller.maxresolution=Integer.parseInt(configurationArray[36]);
						callee1.maxresolution=Integer.parseInt(configurationArray[37]);
						callee2.maxresolution=Integer.parseInt(configurationArray[38]);
						line = line.replaceAll("NotDone", "Done");

						//calling and getting statuses
		                boolean flagCall1 = CallStatusBasedOnRules(caller,callee1);
		                boolean flagCall2 = CallStatusBasedOnRules(caller,callee2);
		                
		                String[] CallStatus ={ "", "", "" };
		                CallStatus[0] = String.valueOf(flagCall1).toLowerCase()
		                    .replaceAll("true", "Connected")
		                    .replaceAll("false", "Failed");
		                CallStatus[1] = String.valueOf(flagCall2).toLowerCase()
		                    .replaceAll("true", "Connected")
		                    .replaceAll("false", "Failed");
		                CallStatus[2] = CallStatus[0] + CallStatus[1];
		               
//						line+=";"+CallStatus[0];
//						line+=";"+CallStatus[1];
						line+=";"+CallStatus[2];
//						readflag=true;
						
					} //if
//				}// if 
				allConfigurations+=line+ "\n";
			}//for
		    Files.write(Paths.get(sourchFilePath), allConfigurations.getBytes());
		    System.out.println("All configurations are executed, see at: "+sourchFilePath);
	
	}
	
    
	//Execute the Jitsi to get call statuses
//	JitsiSimulation.simulate(requiredPopulation);
    
    
    
	
	public static void main(String[] args) throws IOException {
//		String [] searchAlgos={"NSGA2","IBEA","MoCell","SPEA2","SMPSO","PAES","RS"};
		String [] searchAlgos={"RS"};
		for (int i = 0; i < searchAlgos.length; i++) {
			String filePath= "ExperimentResultsWithBP/Jitsi_"+searchAlgos[i]+"/AllRecommenededConfigurations"+searchAlgos[i]+".csv";
			executedAllConfigurations(filePath);
		}

	}

}
