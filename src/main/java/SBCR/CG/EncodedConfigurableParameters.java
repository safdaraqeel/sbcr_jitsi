package SBCR.CG;

import jmetal.util.JMException;
import jmetal.util.wrapper.XInt;

import java.util.Random;

public class EncodedConfigurableParameters {
	public int caller_protocol;
	public int callee1_protocol;
	public int callee2_protocol;
	public int caller_listenport;
	public int callee1_listenport;
	public int callee2_listenport;
	public int caller_transportprotocol;
	public int callee1_transportprotocol;
	public int callee2_transportprotocol;
	public int caller_encryption;
	public int callee1_encryption;
	public int callee2_encryption;
	public int caller_sipzrtpattribute;
	public int callee1_sipzrtpattribute;
	public int callee2_sipzrtpattribute;
	public int caller_maxpeers;
	public int callee1_maxpeers;
	public int callee2_maxpeers;
	public int caller_mtu;
	public int callee1_mtu;
	public int callee2_mtu;
	public int caller_defaultcallrate;
	public int callee1_defaultcallrate;
	public int callee2_defaultcallrate;
	public int caller_maxreceivecallrate;
	public int callee1_maxreceivecallrate;
	public int callee2_maxreceivecallrate;
	public int caller_maxtransmitcallrate;
	public int callee1_maxtransmitcallrate;
	public int callee2_maxtransmitcallrate;
	public int caller_audiocodec;
	public int callee1_audiocodec;
	public int callee2_audiocodec;
	public int caller_videocodec;
	public int callee1_videocodec;
	public int callee2_videocodec;
	public int caller_maxresolution;
	public int callee1_maxresolution;
	public int callee2_maxresolution;

	
	
	
	
	public int getCaller_protocol() {
		return caller_protocol;
	}

	public void setCaller_protocol(int caller_protocol) {
		this.caller_protocol = caller_protocol;
	}

	public int getCallee1_protocol() {
		return callee1_protocol;
	}

	public void setCallee1_protocol(int callee1_protocol) {
		this.callee1_protocol = callee1_protocol;
	}

	public int getCallee2_protocol() {
		return callee2_protocol;
	}

	public void setCallee2_protocol(int callee2_protocol) {
		this.callee2_protocol = callee2_protocol;
	}

	public int getCaller_listenport() {
		return caller_listenport;
	}

	public void setCaller_listenport(int caller_listenport) {
		this.caller_listenport = caller_listenport;
	}

	public int getCallee1_listenport() {
		return callee1_listenport;
	}

	public void setCallee1_listenport(int callee1_listenport) {
		this.callee1_listenport = callee1_listenport;
	}

	public int getCallee2_listenport() {
		return callee2_listenport;
	}

	public void setCallee2_listenport(int callee2_listenport) {
		this.callee2_listenport = callee2_listenport;
	}

	public int getCaller_transportprotocol() {
		return caller_transportprotocol;
	}

	public void setCaller_transportprotocol(int caller_transportprotocol) {
		this.caller_transportprotocol = caller_transportprotocol;
	}

	public int getCallee1_transportprotocol() {
		return callee1_transportprotocol;
	}

	public void setCallee1_transportprotocol(int callee1_transportprotocol) {
		this.callee1_transportprotocol = callee1_transportprotocol;
	}

	public int getCallee2_transportprotocol() {
		return callee2_transportprotocol;
	}

	public void setCallee2_transportprotocol(int callee2_transportprotocol) {
		this.callee2_transportprotocol = callee2_transportprotocol;
	}

	public int getCaller_encryption() {
		return caller_encryption;
	}

	public void setCaller_encryption(int caller_encryption) {
		this.caller_encryption = caller_encryption;
	}

	public int getCallee1_encryption() {
		return callee1_encryption;
	}

	public void setCallee1_encryption(int callee1_encryption) {
		this.callee1_encryption = callee1_encryption;
	}

	public int getCallee2_encryption() {
		return callee2_encryption;
	}

	public void setCallee2_encryption(int callee2_encryption) {
		this.callee2_encryption = callee2_encryption;
	}

	public int getCaller_sipzrtpattribute() {
		return caller_sipzrtpattribute;
	}

	public void setCaller_sipzrtpattribute(int caller_sipzrtpattribute) {
		this.caller_sipzrtpattribute = caller_sipzrtpattribute;
	}

	public int getCallee1_sipzrtpattribute() {
		return callee1_sipzrtpattribute;
	}

	public void setCallee1_sipzrtpattribute(int callee1_sipzrtpattribute) {
		this.callee1_sipzrtpattribute = callee1_sipzrtpattribute;
	}

	public int getCallee2_sipzrtpattribute() {
		return callee2_sipzrtpattribute;
	}

	public void setCallee2_sipzrtpattribute(int callee2_sipzrtpattribute) {
		this.callee2_sipzrtpattribute = callee2_sipzrtpattribute;
	}

	public int getCaller_maxpeers() {
		return caller_maxpeers;
	}

	public void setCaller_maxpeers(int caller_maxpeers) {
		this.caller_maxpeers = caller_maxpeers;
	}

	public int getCallee1_maxpeers() {
		return callee1_maxpeers;
	}

	public void setCallee1_maxpeers(int callee1_maxpeers) {
		this.callee1_maxpeers = callee1_maxpeers;
	}

	public int getCallee2_maxpeers() {
		return callee2_maxpeers;
	}

	public void setCallee2_maxpeers(int callee2_maxpeers) {
		this.callee2_maxpeers = callee2_maxpeers;
	}

	public int getCaller_mtu() {
		return caller_mtu;
	}

	public void setCaller_mtu(int caller_mtu) {
		this.caller_mtu = caller_mtu;
	}

	public int getCallee1_mtu() {
		return callee1_mtu;
	}

	public void setCallee1_mtu(int callee1_mtu) {
		this.callee1_mtu = callee1_mtu;
	}

	public int getCallee2_mtu() {
		return callee2_mtu;
	}

	public void setCallee2_mtu(int callee2_mtu) {
		this.callee2_mtu = callee2_mtu;
	}

	public int getCaller_defaultcallrate() {
		return caller_defaultcallrate;
	}

	public void setCaller_defaultcallrate(int caller_defaultcallrate) {
		this.caller_defaultcallrate = caller_defaultcallrate;
	}

	public int getCallee1_defaultcallrate() {
		return callee1_defaultcallrate;
	}

	public void setCallee1_defaultcallrate(int callee1_defaultcallrate) {
		this.callee1_defaultcallrate = callee1_defaultcallrate;
	}

	public int getCallee2_defaultcallrate() {
		return callee2_defaultcallrate;
	}

	public void setCallee2_defaultcallrate(int callee2_defaultcallrate) {
		this.callee2_defaultcallrate = callee2_defaultcallrate;
	}

	public int getCaller_maxreceivecallrate() {
		return caller_maxreceivecallrate;
	}

	public void setCaller_maxreceivecallrate(int caller_maxreceivecallrate) {
		this.caller_maxreceivecallrate = caller_maxreceivecallrate;
	}

	public int getCallee1_maxreceivecallrate() {
		return callee1_maxreceivecallrate;
	}

	public void setCallee1_maxreceivecallrate(int callee1_maxreceivecallrate) {
		this.callee1_maxreceivecallrate = callee1_maxreceivecallrate;
	}

	public int getCallee2_maxreceivecallrate() {
		return callee2_maxreceivecallrate;
	}

	public void setCallee2_maxreceivecallrate(int callee2_maxreceivecallrate) {
		this.callee2_maxreceivecallrate = callee2_maxreceivecallrate;
	}

	public int getCaller_maxtransmitcallrate() {
		return caller_maxtransmitcallrate;
	}

	public void setCaller_maxtransmitcallrate(int caller_maxtransmitcallrate) {
		this.caller_maxtransmitcallrate = caller_maxtransmitcallrate;
	}

	public int getCallee1_maxtransmitcallrate() {
		return callee1_maxtransmitcallrate;
	}

	public void setCallee1_maxtransmitcallrate(int callee1_maxtransmitcallrate) {
		this.callee1_maxtransmitcallrate = callee1_maxtransmitcallrate;
	}

	public int getCallee2_maxtransmitcallrate() {
		return callee2_maxtransmitcallrate;
	}

	public void setCallee2_maxtransmitcallrate(int callee2_maxtransmitcallrate) {
		this.callee2_maxtransmitcallrate = callee2_maxtransmitcallrate;
	}

	public int getCaller_audiocodec() {
		return caller_audiocodec;
	}

	public void setCaller_audiocodec(int caller_audiocodec) {
		this.caller_audiocodec = caller_audiocodec;
	}

	public int getCallee1_audiocodec() {
		return callee1_audiocodec;
	}

	public void setCallee1_audiocodec(int callee1_audiocodec) {
		this.callee1_audiocodec = callee1_audiocodec;
	}

	public int getCallee2_audiocodec() {
		return callee2_audiocodec;
	}

	public void setCallee2_audiocodec(int callee2_audiocodec) {
		this.callee2_audiocodec = callee2_audiocodec;
	}

	public int getCaller_videocodec() {
		return caller_videocodec;
	}

	public void setCaller_videocodec(int caller_videocodec) {
		this.caller_videocodec = caller_videocodec;
	}

	public int getCallee1_videocodec() {
		return callee1_videocodec;
	}

	public void setCallee1_videocodec(int callee1_videocodec) {
		this.callee1_videocodec = callee1_videocodec;
	}

	public int getCallee2_videocodec() {
		return callee2_videocodec;
	}

	public void setCallee2_videocodec(int callee2_videocodec) {
		this.callee2_videocodec = callee2_videocodec;
	}

	public int getCaller_maxresolution() {
		return caller_maxresolution;
	}

	public void setCaller_maxresolution(int caller_maxresolution) {
		this.caller_maxresolution = caller_maxresolution;
	}

	public int getCallee1_maxresolution() {
		return callee1_maxresolution;
	}

	public void setCallee1_maxresolution(int callee1_maxresolution) {
		this.callee1_maxresolution = callee1_maxresolution;
	}

	public int getCallee2_maxresolution() {
		return callee2_maxresolution;
	}

	public void setCallee2_maxresolution(int callee2_maxresolution) {
		this.callee2_maxresolution = callee2_maxresolution;
	}

	public EncodedConfigurableParameters intializeConfigurationInstance(XInt x) throws JMException {
		caller_protocol = x.getValue(0);
		callee1_protocol = x.getValue(1);
		callee2_protocol = x.getValue(2);
		caller_listenport = x.getValue(3);
		callee1_listenport = x.getValue(4);
		callee2_listenport = x.getValue(5);
		caller_transportprotocol = x.getValue(6);
		callee1_transportprotocol = x.getValue(7);
		callee2_transportprotocol = x.getValue(8);
		caller_encryption = x.getValue(9);
		callee1_encryption = x.getValue(10);
		callee2_encryption = x.getValue(11);
		caller_sipzrtpattribute = x.getValue(12);
		callee1_sipzrtpattribute = x.getValue(13);
		callee2_sipzrtpattribute = x.getValue(14);
		caller_maxpeers = x.getValue(15);
		callee1_maxpeers = x.getValue(16);
		callee2_maxpeers = x.getValue(17);
		caller_mtu = x.getValue(18);
		callee1_mtu = x.getValue(19);
		callee2_mtu = x.getValue(20);
		caller_defaultcallrate = x.getValue(21);
		callee1_defaultcallrate = x.getValue(22);
		callee2_defaultcallrate = x.getValue(23);
		caller_maxreceivecallrate = x.getValue(24);
		callee1_maxreceivecallrate = x.getValue(25);
		callee2_maxreceivecallrate = x.getValue(26);
		caller_maxtransmitcallrate = x.getValue(27);
		callee1_maxtransmitcallrate = x.getValue(28);
		callee2_maxtransmitcallrate = x.getValue(29);
		caller_audiocodec = x.getValue(30);
		callee1_audiocodec = x.getValue(31);
		callee2_audiocodec = x.getValue(32);
		caller_videocodec = x.getValue(33);
		callee1_videocodec = x.getValue(34);
		callee2_videocodec = x.getValue(35);
		caller_maxresolution = x.getValue(36);
		callee1_maxresolution = x.getValue(37);
		callee2_maxresolution = x.getValue(38);
		return this;
	}

	public static int randInt(int min, int max) {
		Random rand = new Random(System.nanoTime() * System.currentTimeMillis());
		// nextInt is normally exclusive of the top value,
		// so add 1 to make it inclusive
		int randomNum = rand.nextInt((max - min) + 1) + min;
		return randomNum;
	}

	public EncodedConfigurableParameters() {
		// TODO Auto-generated constructor stub
	}

}
