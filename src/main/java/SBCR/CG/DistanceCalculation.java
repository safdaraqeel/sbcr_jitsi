package SBCR.CG;

import SBCR.CPLRules.CPLRule;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class DistanceCalculation {

	public DistanceCalculation() {
		// TODO Auto-generated constructor stub
	}

	// for normalization
	public double nor(double x) {
		return x / (x + 1);
	}

	// for x==y
	public double xEqualsY(double k, double x, double y) {
		// for categorical variables
		if (x < 5) {
			if (x - y == 0) {
				return 0;
			} else {
				return k * nor(Math.abs(x - y));
			}
		}
		// for numeric variables
		else {

			if (x - y == 0) {
				return 0;
			} else {
				return k * nor(Math.abs(x - y));
			}

		}

	}

	// for x!=y
	public double xNotEqualsY(double k, double x, double y) {

		if (x - y != 0) {
			return 0;
		} else {
			return k * nor(Math.abs(x - y) + 1);
		}
	}

	// for x<y
	public double xLessThanY(double k, double x, double y) {

		if (x - y < 0) {
			return 0;
		} else {
			return k * nor(Math.abs(x - y) + 1);
		}
	}

	// for x<=y
	public double xLessEqualsThanY(double k, double x, double y) {

		if (x - y <= 0) {
			return 0;
		} else {
			return k * nor(Math.abs(x - y) + 1);
		}
	}

	// for x>y
	public double xGreaterThanY(double k, double x, double y) {

		if (y - x < 0) {
			return 0;
		} else {
			return k * nor(Math.abs(x - y) + 1);
		}
	}

	// for x>=y
	public double xGreaterEqualsThanY(double k, double x, double y) {

		if (y - x <= 0) {
			return 0;
		} else {
			return k * nor(Math.abs(x - y) + 1);
		}
	}

	public List<CPLRule> CalculateDistanceForCallerRelatedRules(Stream<CPLRule> ruleList, EncodedConfigurableParameters configuration) {
		int maxPredicatesInAnyRule[] = { 0 };
		DistanceCalculation disObj = new DistanceCalculation();
		return ruleList.map(rule -> {

//			 System.out.println(rule.getExpression());
			String[] predicates = rule.getExpression().split(" and");
			// getting maximum predicates in any rule
			if (predicates.length > maxPredicatesInAnyRule[0]) {
				maxPredicatesInAnyRule[0] = predicates.length;
			}

			// calculating distance for all the predicates
			for (int j = 0; j < predicates.length; j++) {
				// System.out.println(predicates[j]);
				predicates[j]=predicates[j].replaceAll(" ", "");
				
				// caller_protocol;
				// callee1_protocol;
				// callee2_protocol;
			if (predicates[j].contains("caller_protocol")||predicates[j].contains("callee1_protocol")||predicates[j].contains("callee2_protocol")) {
				predicates[j] = predicates[j].replace("caller_protocol",Integer.toString(configuration.getCaller_protocol()));
				predicates[j] = predicates[j].replace("callee1_protocol",Integer.toString(configuration.getCallee1_protocol()));
				predicates[j] = predicates[j].replace("callee2_protocol",Integer.toString(configuration.getCallee2_protocol()));
				predicates[j] = predicates[j].replace("sip", "0");
				predicates[j] = predicates[j].replace("aim", "1");
				predicates[j] = predicates[j].replace("googletalk", "2");
				predicates[j] = predicates[j].replace("icq", "3");
				predicates[j] = predicates[j].replace("ippi", "4");
				predicates[j] = predicates[j].replace("iptel.org", "5");
				predicates[j] = predicates[j].replace("irc", "6");
				// caller_listenport;
				// callee1_listenport;
				// callee2_listenport;
			} else if (predicates[j].contains("caller_listenport")||predicates[j].contains("callee1_listenport")||predicates[j].contains("callee2_listenport")) {
				predicates[j] = predicates[j].replace("caller_listenport",Integer.toString(configuration.getCaller_listenport()));
				predicates[j] = predicates[j].replace("callee1_listenport",Integer.toString(configuration.getCallee1_listenport()));
				predicates[j] = predicates[j].replace("callee2_listenport",Integer.toString(configuration.getCallee2_listenport()));
				predicates[j] = predicates[j].replace("off", "0");
				predicates[j] = predicates[j].replace("on", "1");
				// caller_transportprotocol;
				// callee1_transportprotocol;
				// callee2_transportprotocol;
			} else if (predicates[j].contains("caller_transportprotocol")||predicates[j].contains("callee1_transportprotocol")||predicates[j].contains("callee2_transportprotocol")) {
				predicates[j] = predicates[j].replace("caller_transportprotocol",Integer.toString(configuration.getCaller_transportprotocol()));
				predicates[j] = predicates[j].replace("callee1_transportprotocol",Integer.toString(configuration.getCallee1_transportprotocol()));
				predicates[j] = predicates[j].replace("callee2_transportprotocol",Integer.toString(configuration.getCallee2_transportprotocol()));
				predicates[j] = predicates[j].replace("auto", "0");
				predicates[j] = predicates[j].replace("tcp", "1");
				predicates[j] = predicates[j].replace("tls", "2");
				predicates[j] = predicates[j].replace("udp", "3");
				// caller_encryption;
				// callee1_encryption;
				// callee2_encryption;
			} else if (predicates[j].contains("caller_encryption")||predicates[j].contains("callee1_encryption")||predicates[j].contains("callee2_encryption")) {
				predicates[j] = predicates[j].replace("caller_encryption",Integer.toString(configuration.getCaller_encryption()));
				predicates[j] = predicates[j].replace("callee1_encryption",Integer.toString(configuration.getCallee1_encryption()));
				predicates[j] = predicates[j].replace("callee2_encryption",Integer.toString(configuration.getCallee2_encryption()));
				predicates[j] = predicates[j].replace("off", "0");
				predicates[j] = predicates[j].replace("on", "1");
				predicates[j] = predicates[j].replace("besteffort", "2");
				// caller_sipzrtpattribute;
				// callee1_sipzrtpattribute;
				// callee2_sipzrtpattribute;
			} else if (predicates[j].contains("caller_sipzrtpattribute")||predicates[j].contains("callee1_sipzrtpattribute")||predicates[j].contains("callee2_sipzrtpattribute")) {
				predicates[j] = predicates[j].replace("caller_sipzrtpattribute",Integer.toString(configuration.getCaller_sipzrtpattribute()));
				predicates[j] = predicates[j].replace("callee1_sipzrtpattribute",Integer.toString(configuration.getCallee1_sipzrtpattribute()));
				predicates[j] = predicates[j].replace("callee2_sipzrtpattribute",Integer.toString(configuration.getCallee2_sipzrtpattribute()));
				predicates[j] = predicates[j].replace("auto", "0");
				predicates[j] = predicates[j].replace("true", "1");
				predicates[j] = predicates[j].replace("false", "2");
				// caller_maxpeers;
				// callee1_maxpeers;
				// callee2_maxpeers;
			} else if (predicates[j].contains("caller_maxpeers")||predicates[j].contains("callee1_maxpeers")||predicates[j].contains("callee2_maxpeers")) {
				predicates[j] = predicates[j].replace("caller_maxpeers",Integer.toString(configuration.getCaller_maxpeers()));
				predicates[j] = predicates[j].replace("callee1_maxpeers",Integer.toString(configuration.getCallee1_maxpeers()));
				predicates[j] = predicates[j].replace("callee2_maxpeers",Integer.toString(configuration.getCallee2_maxpeers()));
				predicates[j] = predicates[j].replace("2", "0");
				predicates[j] = predicates[j].replace("3", "1");
				predicates[j] = predicates[j].replace("4", "2");
				predicates[j] = predicates[j].replace("5", "3");
				// caller_mtu;
				// callee1_mtu;
				// callee2_mtu;
			}else if (predicates[j].contains("caller_mtu")||predicates[j].contains("callee1_mtu")||predicates[j].contains("callee2_mtu")) {
				predicates[j] = predicates[j].replace("caller_mtu", Integer.toString(configuration.getCaller_mtu()));
				predicates[j] = predicates[j].replace("callee1_mtu", Integer.toString(configuration.getCallee1_mtu()));
				predicates[j] = predicates[j].replace("callee2_mtu", Integer.toString(configuration.getCallee2_mtu()));
			
				// caller_defaultcallrate;
				// callee1_defaultcallrate;
				// callee2_defaultcallrate;
			} else if (predicates[j].contains("caller_defaultcallrate")||predicates[j].contains("callee1_defaultcallrate")||predicates[j].contains("callee2_defaultcallrate")) {
				predicates[j] = predicates[j].replace("caller_defaultcallrate",Double.toString(configuration.getCaller_defaultcallrate()));
				predicates[j] = predicates[j].replace("callee1_defaultcallrate",Double.toString(configuration.getCallee1_defaultcallrate()));
				predicates[j] = predicates[j].replace("callee2_defaultcallrate",Double.toString(configuration.getCallee2_defaultcallrate()));
				// caller_maxreceivecallrate;
				// callee1_maxreceivecallrate;
				// callee2_maxreceivecallrate;
			} else if (predicates[j].contains("caller_maxreceivecallrate")||predicates[j].contains("callee1_maxreceivecallrate")||predicates[j].contains("callee2_maxreceivecallrate")) {
				predicates[j] = predicates[j].replace("caller_maxreceivecallrate",Integer.toString(configuration.getCaller_maxreceivecallrate()));
				predicates[j] = predicates[j].replace("callee1_maxreceivecallrate",Integer.toString(configuration.getCallee1_maxreceivecallrate()));
				predicates[j] = predicates[j].replace("callee2_maxreceivecallrate",Integer.toString(configuration.getCallee2_maxreceivecallrate()));
				// caller_maxtransmitcallrate;
				// callee1_maxtransmitcallrate;
				// callee2_maxtransmitcallrate;
			} else if (predicates[j].contains("caller_maxtransmitcallrate")||predicates[j].contains("callee1_maxtransmitcallrate")||predicates[j].contains("callee2_maxtransmitcallrate")) {
				predicates[j] = predicates[j].replace("caller_maxtransmitcallrate",Integer.toString(configuration.getCaller_maxtransmitcallrate()));
				predicates[j] = predicates[j].replace("callee1_maxtransmitcallrate",Integer.toString(configuration.getCallee1_maxtransmitcallrate()));
				predicates[j] = predicates[j].replace("callee2_maxtransmitcallrate",Integer.toString(configuration.getCallee2_maxtransmitcallrate()));
				
				// caller_audiocodec;
				// callee1_audiocodec;
				// callee2_audiocodec;
			} else if (predicates[j].contains("caller_audiocodec")||predicates[j].contains("callee1_audiocodec")||predicates[j].contains("callee2_audiocodec")) {
				predicates[j] = predicates[j].replace("caller_audiocodec",Integer.toString(configuration.getCaller_audiocodec()));
				predicates[j] = predicates[j].replace("callee1_audiocodec",Integer.toString(configuration.getCallee1_audiocodec()));
				predicates[j] = predicates[j].replace("callee2_audiocodec",Integer.toString(configuration.getCallee2_audiocodec()));
				predicates[j]=predicates[j].replace("gsm-8000", "10");
				predicates[j]=predicates[j].replace("speex-8000", "11");
				predicates[j]=predicates[j].replace("amr-wb-16000", "12");
				predicates[j]=predicates[j].replace("silk-12000", "13");
				predicates[j]=predicates[j].replace("silk-8000", "14");
				predicates[j]=predicates[j].replace("telephone-event-80000", "15");
				predicates[j]=predicates[j].replace("auto", "0");
				predicates[j]=predicates[j].replace("opus-48000", "1");
				predicates[j]=predicates[j].replace("silk-24000", "2");
				predicates[j]=predicates[j].replace("silk-16000", "3");
				predicates[j]=predicates[j].replace("g722-16000", "4");
				predicates[j]=predicates[j].replace("speex-32000", "5");
				predicates[j]=predicates[j].replace("speex-16000", "6");
				predicates[j]=predicates[j].replace("pcmu-8000", "7");
				predicates[j]=predicates[j].replace("pcma-8000", "8");
				predicates[j]=predicates[j].replace("ilbc-8000", "9");
				// caller_videocodec;
				// callee1_videocodec;
				// callee2_videocodec;
			} else if (predicates[j].contains("caller_videocodec")||predicates[j].contains("callee1_videocodec")||predicates[j].contains("callee2_videocodec")) {
				predicates[j] = predicates[j].replace("caller_videocodec",Integer.toString(configuration.getCaller_videocodec()));
				predicates[j] = predicates[j].replace("callee1_videocodec",Integer.toString(configuration.getCallee1_videocodec()));
				predicates[j] = predicates[j].replace("callee2_videocodec",Integer.toString(configuration.getCallee2_videocodec()));
				predicates[j]=predicates[j].replace("auto", "0");
				predicates[j]=predicates[j].replace("h264", "1");
				predicates[j]=predicates[j].replace("red", "2");
				predicates[j]=predicates[j].replace("rtx", "3");
				predicates[j]=predicates[j].replace("ulpfec", "4");
				predicates[j]=predicates[j].replace("vp8", "5");
				// caller_maxresolution;
				// callee1_maxresolution;
				// callee2_maxresolution;
				
			} else if (predicates[j].contains("caller_maxresolution")||predicates[j].contains("callee1_maxresolution")||predicates[j].contains("callee2_maxresolution")) {
				predicates[j] = predicates[j].replace("caller_maxresolution",Integer.toString(configuration.getCaller_maxresolution()));
				predicates[j] = predicates[j].replace("callee1_maxresolution",Integer.toString(configuration.getCallee1_maxresolution()));
				predicates[j] = predicates[j].replace("callee2_maxresolution",Integer.toString(configuration.getCallee2_maxresolution()));
				predicates[j]=predicates[j].replace("1080", "0");
				predicates[j]=predicates[j].replace("720", "1");
				predicates[j]=predicates[j].replace("480", "2");
				predicates[j]=predicates[j].replace("360", "3");
				predicates[j]=predicates[j].replace("240", "4");
			}

			if (predicates[j].contains("<=")) {
				// System.out.println("<= operator found");
				String[] operands = predicates[j].split("<=");
				// System.out.println(operands[0]+" <= " + operands[1]);
				rule.setDistance (rule.getDistance()+ disObj.xLessEqualsThanY(1, Double.parseDouble(operands[0]),Double.parseDouble(operands[1])));
				// System.out.println("distance is:-
				// "+disObj.xLessEqualsThanY(1,
				// Double.parseDouble(operands[0]),
				// Double.parseDouble(operands[1])));
			} else if (predicates[j].contains(">=")) {
				// System.out.println(">= operator found");
				String[] operands = predicates[j].split(">=");
				// System.out.println(operands[0]+" >= " + operands[1]);
				rule.setDistance (rule.getDistance()+ disObj.xGreaterEqualsThanY(1, Double.parseDouble(operands[0]),
						Double.parseDouble(operands[1])));
				// System.out.println("distance is:-
				// "+disObj.xGreaterEqualsThanY(1,
				// Double.parseDouble(operands[0]),
				// Double.parseDouble(operands[1])));
			} else if (predicates[j].contains("<")) {
				// System.out.println("< operator found");
				String[] operands = predicates[j].split("<");
				// System.out.println(operands[0]+" < " + operands[1]);
				rule.setDistance (rule.getDistance()+ disObj.xLessThanY(1, Double.parseDouble(operands[0]),
						Double.parseDouble(operands[1])));
				// System.out.println("distance is:- "+disObj.xLessThanY(1,
				// Double.parseDouble(operands[0]),
				// Double.parseDouble(operands[1])));
			} else if (predicates[j].contains(">")) {
				// System.out.println("> operator found");
				String[] operands = predicates[j].split(">");
				// System.out.println(operands[0]+" > " + operands[1]);
				rule.setDistance (rule.getDistance()+ disObj.xGreaterThanY(1, Double.parseDouble(operands[0]),
						Double.parseDouble(operands[1])));
				// System.out.println("distance is:-
				// "+disObj.xGreaterThanY(1, Double.parseDouble(operands[0]),
				// Double.parseDouble(operands[1])));
			} else if (predicates[j].contains("=")) {
				// System.out.println(predicates[j]);
				String[] operands = predicates[j].split("=");
				// System.out.println(operands[0]+" = " + operands[1]);
				rule.setDistance (rule.getDistance()+ disObj.xEqualsY(1, Double.parseDouble(operands[0]), Double.parseDouble(operands[1])));
				// System.out.println("distance is:- "+disObj.xEqualsY(1,
				// Double.parseDouble(operands[0]),
				// Double.parseDouble(operands[1])));
			} else {
				System.out.println("No operator found");
			}
		}

		// constraints[i].setDistance((distance[i]/predicates.length));
		rule.setDistance (rule.getDistance() / maxPredicatesInAnyRule[0]);
		// System.out.println(rule.distance);
		// System.out.println("The total distance for a
		// constraint"+constraints[i].getConstraintExpression()+" is:-
		// "+constraints[i].getDistance());

			return rule;
		}).collect(Collectors.toList());

	}

}
