package SBCR.CG;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class Decoding {

	public Decoding() {
		// TODO Auto-generated constructor stub
	}

	public static void DecodeAllConfigurationsInFile(String sourcePath, String targetPath) {
		String decodedConfigurations = "caller_protocol;callee1_protocol;callee2_protocol;caller_listenport;callee1_listenport;callee2_listenport;caller_transportprotocol;callee1_transportprotocol;callee2_transportprotocol;caller_encryption;callee1_encryption;callee2_encryption;caller_sipzrtpattribute;callee1_sipzrtpattribute;callee2_sipzrtpattribute;caller_maxpeers;callee1_maxpeers;callee2_maxpeers;caller_mtu;callee1_mtu;callee2_mtu;caller_defaultcallrate;callee1_defaultcallrate;callee2_defaultcallrate;caller_maxreceivecallrate;callee1_maxreceivecallrate;callee2_maxreceivecallrate;caller_maxtransmitcallrate;callee1_maxtransmitcallrate;callee2_maxtransmitcallrate;caller_audiocodec;callee1_audiocodec;callee2_audiocodec;caller_videocodec;callee1_videocodec;callee2_videocodec;caller_maxresolution;callee1_maxresolution;callee2_maxresolution;\n";
		try {
			List<String> lines = Files.readAllLines(Paths.get(sourcePath), Charset.defaultCharset());
			for (String line : lines) {
				// System.out.println("encoded configuration: " + line);
				String[] encodedConfigurations = line.split(" ");

				// caller_protocol;
				if (encodedConfigurations[0].equals("0"))
					decodedConfigurations += "SIP;";
				if (encodedConfigurations[0].equals("1"))
					decodedConfigurations += "AIM;";
				if (encodedConfigurations[0].equals("2"))
					decodedConfigurations += "GoogleTalk;";
				if (encodedConfigurations[0].equals("3"))
					decodedConfigurations += "ICQ;";
				if (encodedConfigurations[0].equals("4"))
					decodedConfigurations += "iPPi;";
				if (encodedConfigurations[0].equals("5"))
					decodedConfigurations += "iptel.org;";
				if (encodedConfigurations[0].equals("6"))
					decodedConfigurations += "IRC;";
				// callee1_protocol;
				if (encodedConfigurations[1].equals("0"))
					decodedConfigurations += "SIP;";
				if (encodedConfigurations[1].equals("1"))
					decodedConfigurations += "AIM;";
				if (encodedConfigurations[1].equals("2"))
					decodedConfigurations += "GoogleTalk;";
				if (encodedConfigurations[1].equals("3"))
					decodedConfigurations += "ICQ;";
				if (encodedConfigurations[1].equals("4"))
					decodedConfigurations += "iPPi;";
				if (encodedConfigurations[1].equals("5"))
					decodedConfigurations += "iptel.org;";
				if (encodedConfigurations[1].equals("6"))
					decodedConfigurations += "IRC;";
				// callee2_protocol;
				if (encodedConfigurations[2].equals("0"))
					decodedConfigurations += "SIP;";
				if (encodedConfigurations[2].equals("1"))
					decodedConfigurations += "AIM;";
				if (encodedConfigurations[2].equals("2"))
					decodedConfigurations += "GoogleTalk;";
				if (encodedConfigurations[2].equals("3"))
					decodedConfigurations += "ICQ;";
				if (encodedConfigurations[2].equals("4"))
					decodedConfigurations += "iPPi;";
				if (encodedConfigurations[2].equals("5"))
					decodedConfigurations += "iptel.org;";
				if (encodedConfigurations[2].equals("6"))
					decodedConfigurations += "IRC;";
				// caller_listenport;
				if (encodedConfigurations[3].equals("0"))
					decodedConfigurations += "Off;";
				if (encodedConfigurations[3].equals("1"))
					decodedConfigurations += "On;";
				// callee1_listenport;
				if (encodedConfigurations[4].equals("0"))
					decodedConfigurations += "Off;";
				if (encodedConfigurations[4].equals("1"))
					decodedConfigurations += "On;";
				// callee2_listenport;
				if (encodedConfigurations[5].equals("0"))
					decodedConfigurations += "Off;";
				if (encodedConfigurations[5].equals("1"))
					decodedConfigurations += "On;";
				// caller_transportprotocol
				if (encodedConfigurations[6].equals("0"))
					decodedConfigurations += "Auto;";
				if (encodedConfigurations[6].equals("1"))
					decodedConfigurations += "TCP;";
				if (encodedConfigurations[6].equals("2"))
					decodedConfigurations += "Tls;";
				if (encodedConfigurations[6].equals("3"))
					decodedConfigurations += "UDP;";
				// callee1_transportprotocol
				if (encodedConfigurations[7].equals("0"))
					decodedConfigurations += "Auto;";
				if (encodedConfigurations[7].equals("1"))
					decodedConfigurations += "TCP;";
				if (encodedConfigurations[7].equals("2"))
					decodedConfigurations += "Tls;";
				if (encodedConfigurations[7].equals("3"))
					decodedConfigurations += "UDP;";
				// callee2_transportprotocol
				if (encodedConfigurations[8].equals("0"))
					decodedConfigurations += "Auto;";
				if (encodedConfigurations[8].equals("1"))
					decodedConfigurations += "TCP;";
				if (encodedConfigurations[8].equals("2"))
					decodedConfigurations += "Tls;";
				if (encodedConfigurations[8].equals("3"))
					decodedConfigurations += "UDP;";
				// caller_encryption;
				if (encodedConfigurations[9].equals("0"))
					decodedConfigurations += "Off;";
				if (encodedConfigurations[9].equals("1"))
					decodedConfigurations += "On;";
				if (encodedConfigurations[9].equals("2"))
					decodedConfigurations += "BestEffort;";
				// callee1_encryption;
				if (encodedConfigurations[10].equals("0"))
					decodedConfigurations += "Off;";
				if (encodedConfigurations[10].equals("1"))
					decodedConfigurations += "On;";
				if (encodedConfigurations[10].equals("2"))
					decodedConfigurations += "BestEffort;";
				// callee2_encryption;
				if (encodedConfigurations[11].equals("0"))
					decodedConfigurations += "Off;";
				if (encodedConfigurations[11].equals("1"))
					decodedConfigurations += "On;";
				if (encodedConfigurations[11].equals("2"))
					decodedConfigurations += "BestEffort;";
				// caller_sipzrtpattribute
				if (encodedConfigurations[12].equals("0"))
					decodedConfigurations += "Auto;";
				if (encodedConfigurations[12].equals("1"))
					decodedConfigurations += "TRUE;";
				if (encodedConfigurations[12].equals("2"))
					decodedConfigurations += "FALSE;";
				// callee1_sipzrtpattribute
				if (encodedConfigurations[13].equals("0"))
					decodedConfigurations += "Auto;";
				if (encodedConfigurations[13].equals("1"))
					decodedConfigurations += "TRUE;";
				if (encodedConfigurations[13].equals("2"))
					decodedConfigurations += "FALSE;";
				// callee2_sipzrtpattribute
				if (encodedConfigurations[14].equals("0"))
					decodedConfigurations += "Auto;";
				if (encodedConfigurations[14].equals("1"))
					decodedConfigurations += "TRUE;";
				if (encodedConfigurations[14].equals("2"))
					decodedConfigurations += "FALSE;";
				// caller_maxpeers;
				if (encodedConfigurations[15].equals("0"))
					decodedConfigurations += "2;";
				if (encodedConfigurations[15].equals("1"))
					decodedConfigurations += "3;";
				if (encodedConfigurations[15].equals("2"))
					decodedConfigurations += "4;";
				if (encodedConfigurations[15].equals("3"))
					decodedConfigurations += "5;";
				// callee1_maxpeers;
				if (encodedConfigurations[16].equals("0"))
					decodedConfigurations += "2;";
				if (encodedConfigurations[16].equals("1"))
					decodedConfigurations += "3;";
				if (encodedConfigurations[16].equals("2"))
					decodedConfigurations += "4;";
				if (encodedConfigurations[16].equals("3"))
					decodedConfigurations += "5;";
				// callee2_maxpeers;
				if (encodedConfigurations[17].equals("0"))
					decodedConfigurations += "2;";
				if (encodedConfigurations[17].equals("1"))
					decodedConfigurations += "3;";
				if (encodedConfigurations[17].equals("2"))
					decodedConfigurations += "4;";
				if (encodedConfigurations[17].equals("3"))
					decodedConfigurations += "5;";

				// caller_mtu;
				// callee1_mtu;
				// callee2_mtu;
				decodedConfigurations += encodedConfigurations[18] + ";";
				decodedConfigurations += encodedConfigurations[19] + ";";
				decodedConfigurations += encodedConfigurations[20] + ";";

				// caller_defaultcallrate;
				// callee1_defaultcallrate;
				// callee2_defaultcallrate;
				decodedConfigurations += encodedConfigurations[21] + ";";
				decodedConfigurations += encodedConfigurations[22] + ";";
				decodedConfigurations += encodedConfigurations[23] + ";";

				// caller_maxreceivecallrate;
				// callee1_maxreceivecallrate;
				// callee2_maxreceivecallrate;
				decodedConfigurations += encodedConfigurations[24] + ";";
				decodedConfigurations += encodedConfigurations[25] + ";";
				decodedConfigurations += encodedConfigurations[26] + ";";

				// caller_maxtransmitcallrate;
				// callee1_maxtransmitcallrate;
				// callee2_maxtransmitcallrate;
				decodedConfigurations += encodedConfigurations[27] + ";";
				decodedConfigurations += encodedConfigurations[28] + ";";
				decodedConfigurations += encodedConfigurations[29] + ";";

				// caller_audiocodec;
				if (encodedConfigurations[30].equals("0"))
					decodedConfigurations += "Auto;";
				if (encodedConfigurations[30].equals("1"))
					decodedConfigurations += "opus-48000;";
				if (encodedConfigurations[30].equals("2"))
					decodedConfigurations += "SILK-24000;";
				if (encodedConfigurations[30].equals("3"))
					decodedConfigurations += "SILK-16000;";
				if (encodedConfigurations[30].equals("4"))
					decodedConfigurations += "G722-16000;";
				if (encodedConfigurations[30].equals("5"))
					decodedConfigurations += "speex-32000;";
				if (encodedConfigurations[30].equals("6"))
					decodedConfigurations += "speex-16000;";
				if (encodedConfigurations[30].equals("7"))
					decodedConfigurations += "PCMU-8000;";
				if (encodedConfigurations[30].equals("8"))
					decodedConfigurations += "PCMA-8000;";
				if (encodedConfigurations[30].equals("9"))
					decodedConfigurations += "iLBC-8000;";
				if (encodedConfigurations[30].equals("10"))
					decodedConfigurations += "GSM-8000;";
				if (encodedConfigurations[30].equals("11"))
					decodedConfigurations += "speex-8000;";
				if (encodedConfigurations[30].equals("12"))
					decodedConfigurations += "AMR-WB-16000;";
				if (encodedConfigurations[30].equals("13"))
					decodedConfigurations += "SILK-12000;";
				if (encodedConfigurations[30].equals("14"))
					decodedConfigurations += "SILK-8000;";
				if (encodedConfigurations[30].equals("15"))
					decodedConfigurations += "telephone-event-80000;";
				// callee1_audiocodec;
				if (encodedConfigurations[31].equals("0"))
					decodedConfigurations += "Auto;";
				if (encodedConfigurations[31].equals("1"))
					decodedConfigurations += "opus-48000;";
				if (encodedConfigurations[31].equals("2"))
					decodedConfigurations += "SILK-24000;";
				if (encodedConfigurations[31].equals("3"))
					decodedConfigurations += "SILK-16000;";
				if (encodedConfigurations[31].equals("4"))
					decodedConfigurations += "G722-16000;";
				if (encodedConfigurations[31].equals("5"))
					decodedConfigurations += "speex-32000;";
				if (encodedConfigurations[31].equals("6"))
					decodedConfigurations += "speex-16000;";
				if (encodedConfigurations[31].equals("7"))
					decodedConfigurations += "PCMU-8000;";
				if (encodedConfigurations[31].equals("8"))
					decodedConfigurations += "PCMA-8000;";
				if (encodedConfigurations[31].equals("9"))
					decodedConfigurations += "iLBC-8000;";
				if (encodedConfigurations[31].equals("10"))
					decodedConfigurations += "GSM-8000;";
				if (encodedConfigurations[31].equals("11"))
					decodedConfigurations += "speex-8000;";
				if (encodedConfigurations[31].equals("12"))
					decodedConfigurations += "AMR-WB-16000;";
				if (encodedConfigurations[31].equals("13"))
					decodedConfigurations += "SILK-12000;";
				if (encodedConfigurations[31].equals("14"))
					decodedConfigurations += "SILK-8000;";
				if (encodedConfigurations[31].equals("15"))
					decodedConfigurations += "telephone-event-80000;";
				// callee2_audiocodec;
				if (encodedConfigurations[32].equals("0"))
					decodedConfigurations += "Auto;";
				if (encodedConfigurations[32].equals("1"))
					decodedConfigurations += "opus-48000;";
				if (encodedConfigurations[32].equals("2"))
					decodedConfigurations += "SILK-24000;";
				if (encodedConfigurations[32].equals("3"))
					decodedConfigurations += "SILK-16000;";
				if (encodedConfigurations[32].equals("4"))
					decodedConfigurations += "G722-16000;";
				if (encodedConfigurations[32].equals("5"))
					decodedConfigurations += "speex-32000;";
				if (encodedConfigurations[32].equals("6"))
					decodedConfigurations += "speex-16000;";
				if (encodedConfigurations[32].equals("7"))
					decodedConfigurations += "PCMU-8000;";
				if (encodedConfigurations[32].equals("8"))
					decodedConfigurations += "PCMA-8000;";
				if (encodedConfigurations[32].equals("9"))
					decodedConfigurations += "iLBC-8000;";
				if (encodedConfigurations[32].equals("10"))
					decodedConfigurations += "GSM-8000;";
				if (encodedConfigurations[32].equals("11"))
					decodedConfigurations += "speex-8000;";
				if (encodedConfigurations[32].equals("12"))
					decodedConfigurations += "AMR-WB-16000;";
				if (encodedConfigurations[32].equals("13"))
					decodedConfigurations += "SILK-12000;";
				if (encodedConfigurations[32].equals("14"))
					decodedConfigurations += "SILK-8000;";
				if (encodedConfigurations[32].equals("15"))
					decodedConfigurations += "telephone-event-80000;";
				// caller_videocodec;
				if (encodedConfigurations[33].equals("0"))
					decodedConfigurations += "Auto;";
				if (encodedConfigurations[33].equals("1"))
					decodedConfigurations += "h264;";
				if (encodedConfigurations[33].equals("2"))
					decodedConfigurations += "red;";
				if (encodedConfigurations[33].equals("3"))
					decodedConfigurations += "rtx;";
				if (encodedConfigurations[33].equals("4"))
					decodedConfigurations += "ulpfec;";
				if (encodedConfigurations[33].equals("5"))
					decodedConfigurations += "VP8;";
				// callee1_videocodec;
				if (encodedConfigurations[34].equals("0"))
					decodedConfigurations += "Auto;";
				if (encodedConfigurations[34].equals("1"))
					decodedConfigurations += "h264;";
				if (encodedConfigurations[34].equals("2"))
					decodedConfigurations += "red;";
				if (encodedConfigurations[34].equals("3"))
					decodedConfigurations += "rtx;";
				if (encodedConfigurations[34].equals("4"))
					decodedConfigurations += "ulpfec;";
				if (encodedConfigurations[34].equals("5"))
					decodedConfigurations += "VP8;";
				// callee2_videocodec;
				if (encodedConfigurations[35].equals("0"))
					decodedConfigurations += "Auto;";
				if (encodedConfigurations[35].equals("1"))
					decodedConfigurations += "h264;";
				if (encodedConfigurations[35].equals("2"))
					decodedConfigurations += "red;";
				if (encodedConfigurations[35].equals("3"))
					decodedConfigurations += "rtx;";
				if (encodedConfigurations[35].equals("4"))
					decodedConfigurations += "ulpfec;";
				if (encodedConfigurations[35].equals("5"))
					decodedConfigurations += "VP8;";

				// caller_maxresolution;
				if (encodedConfigurations[36].equals("0"))
					decodedConfigurations += "1080;";
				if (encodedConfigurations[36].equals("1"))
					decodedConfigurations += "720;";
				if (encodedConfigurations[36].equals("2"))
					decodedConfigurations += "480;";
				if (encodedConfigurations[36].equals("3"))
					decodedConfigurations += "360;";
				if (encodedConfigurations[36].equals("4"))
					decodedConfigurations += "240;";
				// callee1_maxresolution;
				if (encodedConfigurations[37].equals("0"))
					decodedConfigurations += "1080;";
				if (encodedConfigurations[37].equals("1"))
					decodedConfigurations += "720;";
				if (encodedConfigurations[37].equals("2"))
					decodedConfigurations += "480;";
				if (encodedConfigurations[37].equals("3"))
					decodedConfigurations += "360;";
				if (encodedConfigurations[37].equals("4"))
					decodedConfigurations += "240;";
				// callee2_maxresolution;
				if (encodedConfigurations[38].equals("0"))
					decodedConfigurations += "1080;";
				if (encodedConfigurations[38].equals("1"))
					decodedConfigurations += "720;";
				if (encodedConfigurations[38].equals("2"))
					decodedConfigurations += "480;";
				if (encodedConfigurations[38].equals("3"))
					decodedConfigurations += "360;";
				if (encodedConfigurations[38].equals("4"))
					decodedConfigurations += "240;";

				decodedConfigurations += "NotDone\n";
			} // end of for loop
			Files.write(Paths.get(targetPath), decodedConfigurations.getBytes());
			// System.out.println("Decoded configuration:\n" + decodedConfigurations);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static String DecodeOneConfiguration(EncodedConfigurableParameters encodedConfiguration) {
		String decodedConfiguration = "";

		// caller_protocol
		if (encodedConfiguration.getCaller_protocol() == 0)
			decodedConfiguration += "SIP;";
		if (encodedConfiguration.getCaller_protocol() == 1)
			decodedConfiguration += "AIM;";
		if (encodedConfiguration.getCaller_protocol() == 2)
			decodedConfiguration += "GoogleTalk;";
		if (encodedConfiguration.getCaller_protocol() == 3)
			decodedConfiguration += "ICQ;";
		if (encodedConfiguration.getCaller_protocol() == 4)
			decodedConfiguration += "iPPi;";
		if (encodedConfiguration.getCaller_protocol() == 5)
			decodedConfiguration += "iptel.org;";
		if (encodedConfiguration.getCaller_protocol() == 6)
			decodedConfiguration += "IRC;";
		// callee1_protocol
		if (encodedConfiguration.getCallee1_protocol() == 0)
			decodedConfiguration += "SIP;";
		if (encodedConfiguration.getCallee1_protocol() == 1)
			decodedConfiguration += "AIM;";
		if (encodedConfiguration.getCallee1_protocol() == 2)
			decodedConfiguration += "GoogleTalk;";
		if (encodedConfiguration.getCallee1_protocol() == 3)
			decodedConfiguration += "ICQ;";
		if (encodedConfiguration.getCallee1_protocol() == 4)
			decodedConfiguration += "iPPi;";
		if (encodedConfiguration.getCallee1_protocol() == 5)
			decodedConfiguration += "iptel.org;";
		if (encodedConfiguration.getCallee1_protocol() == 6)
			decodedConfiguration += "IRC;";
		// callee2_protocol
		if (encodedConfiguration.getCallee2_protocol() == 0)
			decodedConfiguration += "SIP;";
		if (encodedConfiguration.getCallee2_protocol() == 1)
			decodedConfiguration += "AIM;";
		if (encodedConfiguration.getCallee2_protocol() == 2)
			decodedConfiguration += "GoogleTalk;";
		if (encodedConfiguration.getCallee2_protocol() == 3)
			decodedConfiguration += "ICQ;";
		if (encodedConfiguration.getCallee2_protocol() == 4)
			decodedConfiguration += "iPPi;";
		if (encodedConfiguration.getCallee2_protocol() == 5)
			decodedConfiguration += "iptel.org;";
		if (encodedConfiguration.getCallee2_protocol() == 6)
			decodedConfiguration += "IRC;";
		// caller_listenport
		if (encodedConfiguration.getCaller_listenport() == 0)
			decodedConfiguration += "Off;";
		if (encodedConfiguration.getCaller_listenport() == 1)
			decodedConfiguration += "On;";
		// callee1_listenport
		if (encodedConfiguration.getCallee1_listenport() == 0)
			decodedConfiguration += "Off;";
		if (encodedConfiguration.getCallee1_listenport() == 1)
			decodedConfiguration += "On;";
		// callee2_listenport
		if (encodedConfiguration.getCallee2_listenport() == 0)
			decodedConfiguration += "Off;";
		if (encodedConfiguration.getCallee2_listenport() == 1)
			decodedConfiguration += "On;";
		// caller_transportprotocol
		if (encodedConfiguration.getCaller_transportprotocol() == 0)
			decodedConfiguration += "Auto;";
		if (encodedConfiguration.getCaller_transportprotocol() == 1)
			decodedConfiguration += "TCP;";
		if (encodedConfiguration.getCaller_transportprotocol() == 2)
			decodedConfiguration += "Tls;";
		if (encodedConfiguration.getCaller_transportprotocol() == 3)
			decodedConfiguration += "UDP;";
		// callee1_transportprotocol
		if (encodedConfiguration.getCallee1_transportprotocol() == 0)
			decodedConfiguration += "Auto;";
		if (encodedConfiguration.getCallee1_transportprotocol() == 1)
			decodedConfiguration += "TCP;";
		if (encodedConfiguration.getCallee1_transportprotocol() == 2)
			decodedConfiguration += "Tls;";
		if (encodedConfiguration.getCallee1_transportprotocol() == 3)
			decodedConfiguration += "UDP;";
		// callee2_transportprotocol
		if (encodedConfiguration.getCallee2_transportprotocol() == 0)
			decodedConfiguration += "Auto;";
		if (encodedConfiguration.getCallee2_transportprotocol() == 1)
			decodedConfiguration += "TCP;";
		if (encodedConfiguration.getCallee2_transportprotocol() == 2)
			decodedConfiguration += "Tls;";
		if (encodedConfiguration.getCallee2_transportprotocol() == 3)
			decodedConfiguration += "UDP;";
		// caller_encryption
		if (encodedConfiguration.getCaller_encryption() == 0)
			decodedConfiguration += "Off;";
		if (encodedConfiguration.getCaller_encryption() == 1)
			decodedConfiguration += "On;";
		if (encodedConfiguration.getCaller_encryption() == 2)
			decodedConfiguration += "BestEffort;";
		// callee1_encryption
		if (encodedConfiguration.getCallee1_encryption() == 0)
			decodedConfiguration += "Off;";
		if (encodedConfiguration.getCallee1_encryption() == 1)
			decodedConfiguration += "On;";
		if (encodedConfiguration.getCallee1_encryption() == 2)
			decodedConfiguration += "BestEffort;";
		// callee2_encryption
		if (encodedConfiguration.getCallee2_encryption() == 0)
			decodedConfiguration += "Off;";
		if (encodedConfiguration.getCallee2_encryption() == 1)
			decodedConfiguration += "On;";
		if (encodedConfiguration.getCallee2_encryption() == 2)
			decodedConfiguration += "BestEffort;";
		// caller_sipzrtpattribute = new Int(0, 2);
		if (encodedConfiguration.getCaller_sipzrtpattribute() == 0)
			decodedConfiguration += "Auto;";
		if (encodedConfiguration.getCaller_sipzrtpattribute() == 1)
			decodedConfiguration += "TRUE;";
		if (encodedConfiguration.getCaller_sipzrtpattribute() == 2)
			decodedConfiguration += "FALSE;";
		// callee1_sipzrtpattribute = new Int(0, 2);
		if (encodedConfiguration.getCallee1_sipzrtpattribute() == 0)
			decodedConfiguration += "Auto;";
		if (encodedConfiguration.getCallee1_sipzrtpattribute() == 1)
			decodedConfiguration += "TRUE;";
		if (encodedConfiguration.getCallee1_sipzrtpattribute() == 2)
			decodedConfiguration += "FALSE;";
		// callee2_sipzrtpattribute = new Int(0, 2);
		if (encodedConfiguration.getCallee2_sipzrtpattribute() == 0)
			decodedConfiguration += "Auto;";
		if (encodedConfiguration.getCallee2_sipzrtpattribute() == 1)
			decodedConfiguration += "TRUE;";
		if (encodedConfiguration.getCallee2_sipzrtpattribute() == 2)
			decodedConfiguration += "FALSE;";
		// caller_maxpeers;
		if (encodedConfiguration.getCaller_maxpeers() == 0)
			decodedConfiguration += "2;";
		if (encodedConfiguration.getCaller_maxpeers() == 1)
			decodedConfiguration += "3;";
		if (encodedConfiguration.getCaller_maxpeers() == 2)
			decodedConfiguration += "4;";
		if (encodedConfiguration.getCaller_maxpeers() == 3)
			decodedConfiguration += "5;";
		// callee1_maxpeers;
		if (encodedConfiguration.getCallee1_maxpeers() == 0)
			decodedConfiguration += "2;";
		if (encodedConfiguration.getCallee1_maxpeers() == 1)
			decodedConfiguration += "3;";
		if (encodedConfiguration.getCallee1_maxpeers() == 2)
			decodedConfiguration += "4;";
		if (encodedConfiguration.getCallee1_maxpeers() == 3)
			decodedConfiguration += "5;";
		// callee2_maxpeers;
		if (encodedConfiguration.getCallee2_maxpeers() == 0)
			decodedConfiguration += "2;";
		if (encodedConfiguration.getCallee2_maxpeers() == 1)
			decodedConfiguration += "3;";
		if (encodedConfiguration.getCallee2_maxpeers() == 2)
			decodedConfiguration += "4;";
		if (encodedConfiguration.getCallee2_maxpeers() == 3)
			decodedConfiguration += "5;";
		// caller_mtu;
		// callee1_mtu;
		// callee2_mtu;
		decodedConfiguration += encodedConfiguration.getCaller_mtu() + ";";
		decodedConfiguration += encodedConfiguration.getCallee1_mtu() + ";";
		decodedConfiguration += encodedConfiguration.getCallee2_mtu() + ";";
		// caller_defaultcallrate;
		// callee1_defaultcallrate;
		// callee2_defaultcallrate;
		decodedConfiguration += encodedConfiguration.getCaller_defaultcallrate() + ";";
		decodedConfiguration += encodedConfiguration.getCallee1_defaultcallrate() + ";";
		decodedConfiguration += encodedConfiguration.getCallee2_defaultcallrate() + ";";
		// caller_maxreceivecallrate;
		// callee1_maxreceivecallrate;
		// callee2_maxreceivecallrate;
		decodedConfiguration += encodedConfiguration.getCaller_maxreceivecallrate() + ";";
		decodedConfiguration += encodedConfiguration.getCallee1_maxreceivecallrate() + ";";
		decodedConfiguration += encodedConfiguration.getCallee2_maxreceivecallrate() + ";";
		// caller_maxtransmitcallrate;
		// callee1_maxtransmitcallrate;
		// callee2_maxtransmitcallrate;
		decodedConfiguration += encodedConfiguration.getCaller_maxtransmitcallrate() + ";";
		decodedConfiguration += encodedConfiguration.getCallee1_maxtransmitcallrate() + ";";
		decodedConfiguration += encodedConfiguration.getCallee2_maxtransmitcallrate() + ";";
		// caller_audiocodec;
		if (encodedConfiguration.getCaller_audiocodec() == 0)
			decodedConfiguration += "Auto;";
		if (encodedConfiguration.getCaller_audiocodec() == 1)
			decodedConfiguration += "opus-48000;";
		if (encodedConfiguration.getCaller_audiocodec() == 2)
			decodedConfiguration += "SILK-24000;";
		if (encodedConfiguration.getCaller_audiocodec() == 3)
			decodedConfiguration += "SILK-16000;";
		if (encodedConfiguration.getCaller_audiocodec() == 4)
			decodedConfiguration += "G722-16000;";
		if (encodedConfiguration.getCaller_audiocodec() == 5)
			decodedConfiguration += "speex-32000;";
		if (encodedConfiguration.getCaller_audiocodec() == 6)
			decodedConfiguration += "speex-16000;";
		if (encodedConfiguration.getCaller_audiocodec() == 7)
			decodedConfiguration += "PCMU-8000;";
		if (encodedConfiguration.getCaller_audiocodec() == 8)
			decodedConfiguration += "PCMA-8000;";
		if (encodedConfiguration.getCaller_audiocodec() == 9)
			decodedConfiguration += "iLBC-8000;";
		if (encodedConfiguration.getCaller_audiocodec() == 10)
			decodedConfiguration += "GSM-8000;";
		if (encodedConfiguration.getCaller_audiocodec() == 11)
			decodedConfiguration += "speex-8000;";
		if (encodedConfiguration.getCaller_audiocodec() == 12)
			decodedConfiguration += "AMR-WB-16000;";
		if (encodedConfiguration.getCaller_audiocodec() == 13)
			decodedConfiguration += "SILK-12000;";
		if (encodedConfiguration.getCaller_audiocodec() == 14)
			decodedConfiguration += "SILK-8000;";
		if (encodedConfiguration.getCaller_audiocodec() == 15)
			decodedConfiguration += "telephone-event-80000;";
		// callee1_audiocodec;
		if (encodedConfiguration.getCallee1_audiocodec() == 0)
			decodedConfiguration += "Auto;";
		if (encodedConfiguration.getCallee1_audiocodec() == 1)
			decodedConfiguration += "opus-48000;";
		if (encodedConfiguration.getCallee1_audiocodec() == 2)
			decodedConfiguration += "SILK-24000;";
		if (encodedConfiguration.getCallee1_audiocodec() == 3)
			decodedConfiguration += "SILK-16000;";
		if (encodedConfiguration.getCallee1_audiocodec() == 4)
			decodedConfiguration += "G722-16000;";
		if (encodedConfiguration.getCallee1_audiocodec() == 5)
			decodedConfiguration += "speex-32000;";
		if (encodedConfiguration.getCallee1_audiocodec() == 6)
			decodedConfiguration += "speex-16000;";
		if (encodedConfiguration.getCallee1_audiocodec() == 7)
			decodedConfiguration += "PCMU-8000;";
		if (encodedConfiguration.getCallee1_audiocodec() == 8)
			decodedConfiguration += "PCMA-8000;";
		if (encodedConfiguration.getCallee1_audiocodec() == 9)
			decodedConfiguration += "iLBC-8000;";
		if (encodedConfiguration.getCallee1_audiocodec() == 10)
			decodedConfiguration += "GSM-8000;";
		if (encodedConfiguration.getCallee1_audiocodec() == 11)
			decodedConfiguration += "speex-8000;";
		if (encodedConfiguration.getCallee1_audiocodec() == 12)
			decodedConfiguration += "AMR-WB-16000;";
		if (encodedConfiguration.getCallee1_audiocodec() == 13)
			decodedConfiguration += "SILK-12000;";
		if (encodedConfiguration.getCallee1_audiocodec() == 14)
			decodedConfiguration += "SILK-8000;";
		if (encodedConfiguration.getCallee1_audiocodec() == 15)
			decodedConfiguration += "telephone-event-80000;";
		// callee2_audiocodec;
		if (encodedConfiguration.getCallee2_audiocodec() == 0)
			decodedConfiguration += "Auto;";
		if (encodedConfiguration.getCallee2_audiocodec() == 1)
			decodedConfiguration += "opus-48000;";
		if (encodedConfiguration.getCallee2_audiocodec() == 2)
			decodedConfiguration += "SILK-24000;";
		if (encodedConfiguration.getCallee2_audiocodec() == 3)
			decodedConfiguration += "SILK-16000;";
		if (encodedConfiguration.getCallee2_audiocodec() == 4)
			decodedConfiguration += "G722-16000;";
		if (encodedConfiguration.getCallee2_audiocodec() == 5)
			decodedConfiguration += "speex-32000;";
		if (encodedConfiguration.getCallee2_audiocodec() == 6)
			decodedConfiguration += "speex-16000;";
		if (encodedConfiguration.getCallee2_audiocodec() == 7)
			decodedConfiguration += "PCMU-8000;";
		if (encodedConfiguration.getCallee2_audiocodec() == 8)
			decodedConfiguration += "PCMA-8000;";
		if (encodedConfiguration.getCallee2_audiocodec() == 9)
			decodedConfiguration += "iLBC-8000;";
		if (encodedConfiguration.getCallee2_audiocodec() == 10)
			decodedConfiguration += "GSM-8000;";
		if (encodedConfiguration.getCallee2_audiocodec() == 11)
			decodedConfiguration += "speex-8000;";
		if (encodedConfiguration.getCallee2_audiocodec() == 12)
			decodedConfiguration += "AMR-WB-16000;";
		if (encodedConfiguration.getCallee2_audiocodec() == 13)
			decodedConfiguration += "SILK-12000;";
		if (encodedConfiguration.getCallee2_audiocodec() == 14)
			decodedConfiguration += "SILK-8000;";
		if (encodedConfiguration.getCallee2_audiocodec() == 15)
			decodedConfiguration += "telephone-event-80000;";

		// caller_videocodec;
		if (encodedConfiguration.getCaller_videocodec() == 0)
			decodedConfiguration += "Auto;";
		if (encodedConfiguration.getCaller_videocodec() == 1)
			decodedConfiguration += "h264;";
		if (encodedConfiguration.getCaller_videocodec() == 2)
			decodedConfiguration += "red;";
		if (encodedConfiguration.getCaller_videocodec() == 3)
			decodedConfiguration += "rtx;";
		if (encodedConfiguration.getCaller_videocodec() == 4)
			decodedConfiguration += "ulpfec;";
		if (encodedConfiguration.getCaller_videocodec() == 5)
			decodedConfiguration += "VP8;";
		// callee1_videocodec;
		if (encodedConfiguration.getCallee1_videocodec() == 0)
			decodedConfiguration += "Auto;";
		if (encodedConfiguration.getCallee1_videocodec() == 1)
			decodedConfiguration += "h264;";
		if (encodedConfiguration.getCallee1_videocodec() == 2)
			decodedConfiguration += "red;";
		if (encodedConfiguration.getCallee1_videocodec() == 3)
			decodedConfiguration += "rtx;";
		if (encodedConfiguration.getCallee1_videocodec() == 4)
			decodedConfiguration += "ulpfec;";
		if (encodedConfiguration.getCallee1_videocodec() == 5)
			decodedConfiguration += "VP8;";
		// callee2_videocodec;
		if (encodedConfiguration.getCallee2_videocodec() == 0)
			decodedConfiguration += "Auto;";
		if (encodedConfiguration.getCallee2_videocodec() == 1)
			decodedConfiguration += "h264;";
		if (encodedConfiguration.getCallee2_videocodec() == 2)
			decodedConfiguration += "red;";
		if (encodedConfiguration.getCallee2_videocodec() == 3)
			decodedConfiguration += "rtx;";
		if (encodedConfiguration.getCallee2_videocodec() == 4)
			decodedConfiguration += "ulpfec;";
		if (encodedConfiguration.getCallee2_videocodec() == 5)
			decodedConfiguration += "VP8;";

		// caller_maxresolution;
		if (encodedConfiguration.getCaller_maxresolution() == 0)
			decodedConfiguration += "1080;";
		if (encodedConfiguration.getCaller_maxresolution() == 1)
			decodedConfiguration += "720;";
		if (encodedConfiguration.getCaller_maxresolution() == 2)
			decodedConfiguration += "480;";
		if (encodedConfiguration.getCaller_maxresolution() == 3)
			decodedConfiguration += "360;";
		if (encodedConfiguration.getCaller_maxresolution() == 4)
			decodedConfiguration += "240;";
		// callee1_maxresolution;
		if (encodedConfiguration.getCallee1_maxresolution() == 0)
			decodedConfiguration += "1080;";
		if (encodedConfiguration.getCallee1_maxresolution() == 1)
			decodedConfiguration += "720;";
		if (encodedConfiguration.getCallee1_maxresolution() == 2)
			decodedConfiguration += "480;";
		if (encodedConfiguration.getCallee1_maxresolution() == 3)
			decodedConfiguration += "360;";
		if (encodedConfiguration.getCallee1_maxresolution() == 4)
			decodedConfiguration += "240;";
		// callee2_maxresolution;
		if (encodedConfiguration.getCallee2_maxresolution() == 0)
			decodedConfiguration += "1080;";
		if (encodedConfiguration.getCallee2_maxresolution() == 1)
			decodedConfiguration += "720;";
		if (encodedConfiguration.getCallee2_maxresolution() == 2)
			decodedConfiguration += "480;";
		if (encodedConfiguration.getCallee2_maxresolution() == 3)
			decodedConfiguration += "360;";
		if (encodedConfiguration.getCallee2_maxresolution() == 4)
			decodedConfiguration += "240;";

		decodedConfiguration += "\n";
		return decodedConfiguration;
	}

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub

		DecodeAllConfigurationsInFile("VAR", "VAR.csv");

	}

}
