package SBCR.CG;

import SBCR.CPLRules.CPLRule;
import SBCR.CPLRules.JDBC;
import SBCR.CPLRules.ReadCPLRulesFromXL;
import SBCR.CPLRules.RulesManipulationInDB;
import jmetal.core.Problem;
import jmetal.core.Solution;
import jmetal.encodings.solutionType.*;
import jmetal.util.JMException;
import jmetal.util.wrapper.XInt;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;


/**
 * Class representing problem ZDT1
 */
public class ConfigRecommendationProblem extends Problem {
	
public int [][] existingConfigurations;
public static List<CPLRule> rulesList;
public XInt initialSolution;
public boolean flageForInitial=true;
	
 /** 
  * Constructor.
  * Creates a default instance of problem ZDT1 (30 decision variables)
  * @param solutionType The solution type must "Real", "BinaryReal, and "ArrayReal". 
	* ArrayReal, or ArrayRealC".
  */
  public ConfigRecommendationProblem(String solutionType) throws ClassNotFoundException {
    this(solutionType, numberOfVariables_); 
	
  } // ZDT1

	public ConfigRecommendationProblem(String solutionType, Integer numberOfVariables) {
		try {
			// Creating connection with DB
//			Connection connection = JDBC.connectToDB("JitsiRulesDB", "postgres", "gramer");
//			String query = "SELECT * FROM public.refinedrules where iteration>4 AND support>10 AND approach SIMILAR TO '%sbrm-part%'";
//			long t1 = System.currentTimeMillis();

			// getting from DB and removing extra predicates  related to callees from the rules
			String fileName = "CPLRules/JitsiSelectedRules.csv";
			this.rulesList= ReadCPLRulesFromXL.read(fileName).map(rule->{
				Arrays.asList(rule.getExpression().split(" and ")).stream().filter(predicate->predicate.contains("network_"))
						.forEach(predicate->rule.setExpression(rule.getExpression().replaceAll(" and "+predicate, "").replaceAll(predicate+" and ", "")));
//				System.err.println(rule.expression);

/*				// getting from DB and removing extra predicates  related to callees from the rules
				this.rulesList= RulesManipulationInDB.getRulesFromDB(query, connection).map(rule->{
					Arrays.asList(rule.getExpression().split(" and ")).stream().filter(predicate->predicate.contains("network_"))
							.forEach(predicate->rule.setExpression(rule.getExpression().replaceAll(" and "+predicate, "").replaceAll(predicate+" and ", "")));
//				System.err.println(rule.expression);
*/
					// getting from DB and removing extra predicates  related to callees from the rules
//				this.rulesList=RulesManipulationInDB.getRulesFromDB(query, connection).map(rule->{
//					Arrays.asList(rule.getExpression().split(" and ")).stream().filter(predicate->predicate.contains("callee1_")||predicate.contains("callee2_"))
//					.forEach(predicate->rule.setExpression(rule.getExpression().replaceAll(" and "+predicate, "").replaceAll(predicate+" and ", "")));
//					System.err.println(rule.expression);

					return rule;
				}).collect(Collectors.toList());
				//**********or use following****************
//			String [] predicates = rule.expression.split(" and ");
//			for (int i = 0; i < predicates.length; i++) {
//				if (predicates[i].contains("callee1_")||predicates[i].contains("callee2_")){
//					rule.expression=rule.expression.replaceAll(" and "+predicates[i], "").replaceAll(predicates[i]+" and ", "");
//				}
//			}	
			//*****************************************			
					
//			RulesManipulationInDataBase.printListOfRules(this.rulesList);
//			long t2 = System.currentTimeMillis();
//			System.out.println((t2 - t1) + " mili seconds");


			//reading existing configurations which will be used to calculate the fitness for one objective
			this.existingConfigurations= Encoding.ReadExistingConfigurations("Configurations/ExistingConfigurations/EncodedConfigurations.csv", numberOfVariables);

		} catch (Exception e) {
			e.printStackTrace();
		}

	
		numberOfVariables_ = numberOfVariables;
		numberOfObjectives_ = 4;
		numberOfConstraints_ = 0;
		problemName_ = "ConfigProblem";
		upperLimit_ = new double[numberOfVariables_];
		lowerLimit_ = new double[numberOfVariables_];
		
		 // Establishes upper and lower limits for the variables 		
		// private String caller_protocol = new (0,6) ; //{SIP,AIM,GoogleTalk,ICQ,iPPi,iptel.org,IRC}
 		// private String callee1_protocol = new (0,6) ; //{SIP,AIM,GoogleTalk,ICQ,iPPi,iptel.org,IRC}
 		// private String callee2_protocol = new (0,6) ; //{SIP,AIM,GoogleTalk,ICQ,iPPi,iptel.org,IRC}

 		// caller_listenport= new Int(0,1); // {On, Off}
 		// callee1_listenport= new Int(0,1); // {On, Off}
 		// callee2_listenport= new Int(0,1); // {On, Off}

 		// caller_transportprotocol = new Int(0, 3); // {Auto, UDP,TCP,TLS}
 		// callee1_transportprotocol = new Int(0, 3); // {Auto, UDP,TCP,TLS}
 		// callee2_transportprotocol = new Int(0, 3); // {Auto, UDP,TCP,TLS}

 		// caller_encryption= new Int(0,2);// {On, Off, BestEffort}
 		// callee1_encryption= new Int(0,2);// {On, Off, BestEffort}
 		// callee2_encryption= new Int(0,2);// {On, Off, BestEffort}

 		// private String caller_sipzrtpattribute = new (0,2) ; //{Auto, true, false}
 		// private String callee1_sipzrtpattribute = new (0,2) ; //{Auto, true, false}
 		// private String callee2_sipzrtpattribute = new (0,2) ; //{Auto, true, false}

 		// private int caller_maxpeers = new (0,3) ; // {2,3,4,5}
 		// private int callee1_maxpeers = new (0,3) ; // {2,3,4,5}
 		// private int callee2_maxpeers = new (0,3) ; // {2,3,4,5}

 		// caller_mtu = new Int(576, 2000);
 		// callee1_mtu = new Int(576, 2000);
 		// callee2_mtu = new Int(576, 2000);

 		// caller_defaultcallrate = new Int(64, 6000);
 		// callee1_defaultcallrate = new Int(64, 6000);
 		// callee2_defaultcallrate = new Int(64, 6000);
 		
 		// caller_maxreceivecallrate = new Int(64, 6000);
 		// callee1_maxreceivecallrate = new Int(64, 6000);
 		// callee2_maxreceivecallrate = new Int(64, 6000);

 		// caller_maxtransmitcallrate = new Int(64, 6000);
 		// callee1_maxtransmitcallrate = new Int(64, 6000);
 		// callee2_maxtransmitcallrate = new Int(64, 6000);

		// private String caller_audiocodec = new (0,15) ; // {Auto,opus-48000,SILK-24000,SILK-16000,G722-16000,speex-32000,speex-16000,PCMU-8000,PCMA-8000,iLBC-8000,GSM-8000,speex-8000,AMR-WB-16000,SILK-12000,SILK-8000,telephone-event-80000}
 		// private String callee1_audiocodec = new (0,15) ; // {Auto,opus-48000,SILK-24000,SILK-16000,G722-16000,speex-32000,speex-16000,PCMU-8000,PCMA-8000,iLBC-8000,GSM-8000,speex-8000,AMR-WB-16000,SILK-12000,SILK-8000,telephone-event-80000}
 		// private String callee2_audiocodec = new (0,15) ; // {Auto,opus-48000,SILK-24000,SILK-16000,G722-16000,speex-32000,speex-16000,PCMU-8000,PCMA-8000,iLBC-8000,GSM-8000,speex-8000,AMR-WB-16000,SILK-12000,SILK-8000,telephone-event-80000}
 		
 		// private String caller_videocodec = new (0,5) ; // {Auto, h264,red,rtx,ulpfec,VP8}
 		// private String callee1_videocodec = new (0,5) ; // {Auto, h264,red,rtx,ulpfec,VP8}
 		// private String callee2_videocodec = new (0,5) ; // {Auto, h264,red,rtx,ulpfec,VP8}

 		// private int caller_maxresolution = new (0,4) ; //{1080,720,480,360,240}
 		// private int callee1_maxresolution = new (0,4) ; //{1080,720,480,360,240}
 		// private int callee2_maxresolution = new (0,4) ; //{1080,720,480,360,240}
 	

 		for (int var = 0; var < getNumberOfVariables(); var++) {
 			if (var==0 || var==1 || var==2) {
 				lowerLimit_[var] = 0;
				upperLimit_[var] = 6;				
 			}else if (var==3 || var==4 || var==5) {
 				lowerLimit_[var] = 0;
				upperLimit_[var] = 1;
 			} else if (var == 6 || var == 7 || var == 8) {
 				lowerLimit_[var] = 0;
				upperLimit_[var] = 3;
 			} else if (var == 9 || var == 10 || var == 11) {
 				lowerLimit_[var] = 0;
				upperLimit_[var] = 2;
 			} else if ( var == 12 || var == 13 || var == 14) {
 				lowerLimit_[var] = 0;
				upperLimit_[var] = 2;
 			} else if (var == 15 || var == 16 || var == 17) {
 				lowerLimit_[var] = 0;
				upperLimit_[var] = 2;
 			} else if (var == 18 || var == 19 || var == 20) {
 				lowerLimit_[var] = 576;
				upperLimit_[var] = 2000;

 			} else if (var >= 21 && var <= 29) {
 				lowerLimit_[var] = 64;
				upperLimit_[var] = 6000;

 			} else if (var == 30 || var == 31 || var == 32) {
 				lowerLimit_[var] = 0;
				upperLimit_[var] = 15;

 			} else if (var == 33 || var == 34 || var == 35) {
 				lowerLimit_[var] = 0;
				upperLimit_[var] = 5;

 			} else {
 				lowerLimit_[var] = 0;
				upperLimit_[var] = 4;
 			}
		}//end of for
				
	if (solutionType.compareTo("Integer") == 0)
		solutionType_ = new IntSolutionType(this) ;
    else if (solutionType.compareTo("Real") == 0) 
    		solutionType_ = new RealSolutionType(this) ;
    else if (solutionType.compareTo("ArrayReal") == 0)
    		solutionType_ = new ArrayRealSolutionType(this) ;
    else if (solutionType.compareTo("BinaryReal") == 0)
    		solutionType_ = new BinaryRealSolutionType(this) ;
    else if (solutionType.compareTo("IntegerArray") == 0)
    		solutionType_ = new ArrayIntSolutionType(this) ;
    else {
    		System.out.println("Error: solution type " + solutionType + " invalid") ;
    		System.exit(-1) ;
    }
		
  } // ConfigProblem
    
  /** 
   * Evaluates a solution.
   * @param solution The solution to evaluate.
   * @throws JMException
   */
	
  public void evaluate(Solution solution) throws JMException {
	  XInt x = new XInt(solution);
	  //getting initial default configurations for 5th objective
//	  if (flageForInitial){
//		this.initialSolution = new XInt(solution) ;
//		DefaultConfiguration.getDefaultConfigurations(initialSolution);		
//		flageForInitial=false;
//	  }
		
	if(getViolatedConstraints(solution)>0){
	    solution.setObjective(0,1);
	    solution.setObjective(1,1);
	    solution.setObjective(2,1);
	    solution.setObjective(3,1);

	}else{
		
	double [] f = new double[numberOfObjectives_]  ;
    f[0]=this.MaxViolationToAbnormalStateRules(x);
    f[1]=this.MaxConformanceToNormalStateRules(x);
    f[2]=this.MaxConfidence(x);
    f[3]=this.diversConfigurations(x, this.existingConfigurations);
    
    
    solution.setObjective(0,f[0]);
    solution.setObjective(1,f[1]);
    solution.setObjective(2,f[2]);
    solution.setObjective(3,f[3]);
//    solution.setObjective(4, f[4]);
	}//else    
  } // evaluate
    
  /** 
   * Evaluates a solution.
   * @param solution The solution to evaluate.
   * @throws JMException
   */
  public int getViolatedConstraints(Solution solution) throws JMException {
	  int totalConstraitnsViolated=0; 
//	  if (solution.getDecisionVariables()[2].getValue()==3){
//		  totalConstraitnsViolated++;	// Protocol=H320
//	  }
//	  if (solution.getDecisionVariables()[2].getValue()==1 && solution.getDecisionVariables()[10].getValue()==0){
//		  totalConstraitnsViolated++;// Protocol=SIP and SIPMode=off
//	  }
//	  if (solution.getDecisionVariables()[2].getValue()==2 && solution.getDecisionVariables()[11].getValue()==0){
//		  totalConstraitnsViolated++;// Protocol=H323 and H323Mode=off
//	  }
//	  if (solution.getDecisionVariables()[2].getValue()==1 ){
//		  totalConstraitnsViolated++;// Protocol=SIP because it might lead to failed one due to listen pot of receiver 
//	  }
//	  if (solution.getDecisionVariables()[0].getValue()!=2 ){
//		  totalConstraitnsViolated++;// Encryption!=BestEffort 
//	  }
//	  solution.setNumberOfViolatedConstraint(totalConstraitnsViolated);
	  return totalConstraitnsViolated;
  }
  private double MaxViolationToAbnormalStateRules(XInt x) throws JMException {
		// initializing a configuration object with solution
		EncodedConfigurableParameters configuration = new EncodedConfigurableParameters();
		configuration = configuration.intializeConfigurationInstance(x);
		// evaluating constraints using solution
		DistanceCalculation disObject= new DistanceCalculation();
		this.rulesList=disObject.CalculateDistanceForCallerRelatedRules(rulesList.parallelStream(), configuration);		
		return Objective1.CalculateFitnessForMaxViolationToNormalStateRules(this.rulesList);
	}
  private double MaxConformanceToNormalStateRules(XInt x) throws JMException {
	  return Objective2.CalculateFitnessForMaxConformanceToAbnormalStateRules(this.rulesList);
  	}
  private double MaxConfidence(XInt x) throws JMException {
		return Objective3.CalculateFitnessForMaxConfidenceObjective(this.rulesList);
  	}
  private double diversConfigurations(XInt x, int [][] existingConfigurations) throws JMException {
		return Objective4.CalculateFitnessForDiversConfigurationsObjective(x, existingConfigurations);
	}

}