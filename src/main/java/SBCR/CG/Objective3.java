package SBCR.CG;

import SBCR.CPLRules.CPLRule;

import java.util.Iterator;
import java.util.List;

public class Objective3 {

	
	
	  public static double CalculateFitnessForMaxConfidenceObjective(List<CPLRule> rulesList){
		 double fitness=1;
		 double totalconfidenceNormalStateRules =0;
		 double totalconfidenceAbnormalStateRules =0;
		 double conformedNormalStateRules =0;
		 double violatedAbnormalStateRules =0;
		 double confidenceConformedNormalStateRules =0;
		 double confidenceViolatedAbnormalStateRules =0;
		 
		 for (Iterator iterator = rulesList.iterator(); iterator.hasNext();) {
			CPLRule rule = (CPLRule) iterator.next();

			if(!rule.isIsNormalState()){
				totalconfidenceNormalStateRules+=rule.getConfidence();
				if (rule.getDistance()==0){
					conformedNormalStateRules+=1;
					confidenceConformedNormalStateRules+=rule.getConfidence();
				}
			}else{
				totalconfidenceAbnormalStateRules+=rule.getConfidence();
				if (rule.getDistance()>0){
					violatedAbnormalStateRules+=1;
					confidenceViolatedAbnormalStateRules+=rule.getConfidence();
				}
			}
		 }
		 
		 if (totalconfidenceNormalStateRules==0)
			 totalconfidenceNormalStateRules=1;
		 else if (totalconfidenceAbnormalStateRules==0)
			 totalconfidenceAbnormalStateRules=1;
		 else  if (conformedNormalStateRules==0)
			 conformedNormalStateRules=1;
		 else if (violatedAbnormalStateRules==0)
			 violatedAbnormalStateRules=1;
		  
		 fitness= (double)(confidenceConformedNormalStateRules+confidenceViolatedAbnormalStateRules)/(totalconfidenceNormalStateRules+totalconfidenceAbnormalStateRules);		
		 
//		 System.out.println( "MaxConfidence Measure:	"+(fitness));
//		 System.out.println( "MaxConfidence objective:	"+(1-fitness));
	     return 1-fitness;
	    }
	  
	  
	  
	
	
}
