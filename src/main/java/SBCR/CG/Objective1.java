package SBCR.CG;

import SBCR.CPLRules.CPLRule;

import java.util.Iterator;
import java.util.List;

public class Objective1 {

		public static double CalculateFitnessForMaxViolationToNormalStateRules(List<CPLRule> rulesList){
			double sumOfDistances=0;
			double maxSumOfDistances=0;
			for (Iterator iterator = rulesList.iterator(); iterator.hasNext();) {
				CPLRule rule = (CPLRule) iterator.next();
				if (rule.isIsNormalState()){
					//System.out.println(constraints[i].getConstraintExpression()+" {Distance: "+constraints[i].getDistance()+"}");
					sumOfDistances += rule.getDistance();	
					maxSumOfDistances+=1;
				}
			}
	
//			System.out.println("sumOfWeightedDistance is: "+sumOfWeightedDistance);
//			System.out.println("maxWeightedDistance is: "+maxWeightedDistance);
			double normalizedSumOfDistances = (sumOfDistances/maxSumOfDistances);	//(x-min/max-min) normalization function	
			//double normalizedSumOfWeightedDistance = sumOfWeightedDistance/(sumOfWeightedDistance+1); //(x/x+1) normalization function
//			System.out.println("normalizedSumOfWeightedDistance is: "+normalizedSumOfWeightedDistance);
			double fitnessValue = 1-normalizedSumOfDistances;
//			System.out.println("MaxViolationToAbnormalStateRules Objective: "+fitnessValue);
			return fitnessValue;
		}
	
		
}
