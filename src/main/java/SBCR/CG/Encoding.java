package SBCR.CG;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class Encoding {

	public Encoding() {
		// TODO Auto-generated constructor stub
	}

	public static void EncodeAllConfigurationsInFile(String sourcePath, String targetPath) {

		String encodedConfigurations = "";
		try {
			List<String> lines = Files.readAllLines(Paths.get(sourcePath), Charset.defaultCharset());
			for (String line : lines) {
				// System.out.println("encoded configuration: " + line);
				String[] decodedConfigurations = line.split(";");

				// caller_protocol
				if (decodedConfigurations[0].equals("SIP"))
					encodedConfigurations += "0 ";
				if (decodedConfigurations[0].equals("AIM"))
					encodedConfigurations += "1 ";
				if (decodedConfigurations[0].equals("GoogleTalk"))
					encodedConfigurations += "2 ";
				if (decodedConfigurations[0].equals("ICQ"))
					encodedConfigurations += "3 ";
				if (decodedConfigurations[0].equals("iPPi"))
					encodedConfigurations += "4 ";
				if (decodedConfigurations[0].equals("iptel.org"))
					encodedConfigurations += "5 ";
				if (decodedConfigurations[0].equals("IRC"))
					encodedConfigurations += "6 ";
				// callee1_protocol
				if (decodedConfigurations[1].equals("SIP"))
					encodedConfigurations += "0 ";
				if (decodedConfigurations[1].equals("AIM"))
					encodedConfigurations += "1 ";
				if (decodedConfigurations[1].equals("GoogleTalk"))
					encodedConfigurations += "2 ";
				if (decodedConfigurations[1].equals("ICQ"))
					encodedConfigurations += "3 ";
				if (decodedConfigurations[1].equals("iPPi"))
					encodedConfigurations += "4 ";
				if (decodedConfigurations[1].equals("iptel.org"))
					encodedConfigurations += "5 ";
				if (decodedConfigurations[1].equals("IRC"))
					encodedConfigurations += "6 ";
				// callee2_protocol
				if (decodedConfigurations[2].equals("SIP"))
					encodedConfigurations += "0 ";
				if (decodedConfigurations[2].equals("AIM"))
					encodedConfigurations += "1 ";
				if (decodedConfigurations[2].equals("GoogleTalk"))
					encodedConfigurations += "2 ";
				if (decodedConfigurations[2].equals("ICQ"))
					encodedConfigurations += "3 ";
				if (decodedConfigurations[2].equals("iPPi"))
					encodedConfigurations += "4 ";
				if (decodedConfigurations[2].equals("iptel.org"))
					encodedConfigurations += "5 ";
				if (decodedConfigurations[2].equals("IRC"))
					encodedConfigurations += "6 ";
				// caller_listenport
				if (decodedConfigurations[3].equals("Off"))
					encodedConfigurations += "0 ";
				if (decodedConfigurations[3].equals("On"))
					encodedConfigurations += "1 ";
				// callee1_listenport
				if (decodedConfigurations[4].equals("Off"))
					encodedConfigurations += "0 ";
				if (decodedConfigurations[4].equals("On"))
					encodedConfigurations += "1 ";
				// callee2_listenport
				if (decodedConfigurations[5].equals("Off"))
					encodedConfigurations += "0 ";
				if (decodedConfigurations[5].equals("On"))
					encodedConfigurations += "1 ";
				// caller_transportprotocol
				if (decodedConfigurations[6].equals("Auto"))
					encodedConfigurations += "0 ";
				if (decodedConfigurations[6].equals("TCP"))
					encodedConfigurations += "1 ";
				if (decodedConfigurations[6].equals("TLS"))
					encodedConfigurations += "2 ";
				if (decodedConfigurations[6].equals("UDP"))
					encodedConfigurations += "3 ";
				// callee1_transportprotocol
				if (decodedConfigurations[7].equals("Auto"))
					encodedConfigurations += "0 ";
				if (decodedConfigurations[7].equals("TCP"))
					encodedConfigurations += "1 ";
				if (decodedConfigurations[7].equals("TLS"))
					encodedConfigurations += "2 ";
				if (decodedConfigurations[7].equals("UDP"))
					encodedConfigurations += "3 ";
				// callee2_transportprotocol
				if (decodedConfigurations[8].equals("Auto"))
					encodedConfigurations += "0 ";
				if (decodedConfigurations[8].equals("TCP"))
					encodedConfigurations += "1 ";
				if (decodedConfigurations[8].equals("TLS"))
					encodedConfigurations += "2 ";
				if (decodedConfigurations[8].equals("UDP"))
					encodedConfigurations += "3 ";
				// caller_encryption
				if (decodedConfigurations[9].equals("Off"))
					encodedConfigurations += "0 ";
				if (decodedConfigurations[9].equals("On"))
					encodedConfigurations += "1 ";
				if (decodedConfigurations[9].equals("BestEffort"))
					encodedConfigurations += "2 ";
				// callee1_encryption
				if (decodedConfigurations[10].equals("Off"))
					encodedConfigurations += "0 ";
				if (decodedConfigurations[10].equals("On"))
					encodedConfigurations += "1 ";
				if (decodedConfigurations[10].equals("BestEffort"))
					encodedConfigurations += "2 ";
				// callee2_encryption
				if (decodedConfigurations[11].equals("Off"))
					encodedConfigurations += "0 ";
				if (decodedConfigurations[11].equals("On"))
					encodedConfigurations += "1 ";
				if (decodedConfigurations[11].equals("BestEffort"))
					encodedConfigurations += "2 ";
				// caller_sipzrtpattribute
				if (decodedConfigurations[12].equals("Auto"))
					encodedConfigurations += "0 ";
				if (decodedConfigurations[12].equals("TRUE"))
					encodedConfigurations += "1 ";
				if (decodedConfigurations[12].equals("FALSE"))
					encodedConfigurations += "2 ";
				// callee1_sipzrtpattribute
				if (decodedConfigurations[13].equals("Auto"))
					encodedConfigurations += "0 ";
				if (decodedConfigurations[13].equals("TRUE"))
					encodedConfigurations += "1 ";
				if (decodedConfigurations[13].equals("FALSE"))
					encodedConfigurations += "2 ";
				// callee2_sipzrtpattribute
				if (decodedConfigurations[14].equals("Auto"))
					encodedConfigurations += "0 ";
				if (decodedConfigurations[14].equals("TRUE"))
					encodedConfigurations += "1 ";
				if (decodedConfigurations[14].equals("FALSE"))
					encodedConfigurations += "2 ";

				// caller_maxpeers
				if (decodedConfigurations[15].equals("2"))
					encodedConfigurations += "0 ";
				if (decodedConfigurations[15].equals("3"))
					encodedConfigurations += "1 ";
				if (decodedConfigurations[15].equals("4"))
					encodedConfigurations += "2 ";
				if (decodedConfigurations[15].equals("5"))
					encodedConfigurations += "3 ";
				// callee1_maxpeers
				if (decodedConfigurations[16].equals("2"))
					encodedConfigurations += "0 ";
				if (decodedConfigurations[16].equals("3"))
					encodedConfigurations += "1 ";
				if (decodedConfigurations[16].equals("4"))
					encodedConfigurations += "2 ";
				if (decodedConfigurations[16].equals("5"))
					encodedConfigurations += "3 ";
				// callee2_maxpeers
				if (decodedConfigurations[17].equals("2"))
					encodedConfigurations += "0 ";
				if (decodedConfigurations[17].equals("3"))
					encodedConfigurations += "1 ";
				if (decodedConfigurations[17].equals("4"))
					encodedConfigurations += "2 ";
				if (decodedConfigurations[17].equals("5"))
					encodedConfigurations += "3 ";
				// caller_mtu
				// callee1_mtu
				// callee2_mtu
				encodedConfigurations += decodedConfigurations[18] + " ";
				encodedConfigurations += decodedConfigurations[19] + " ";
				encodedConfigurations += decodedConfigurations[20] + " ";

				// caller_defaultcallrate
				// callee1_defaultcallrate
				// callee2_defaultcallrate
				encodedConfigurations += decodedConfigurations[21] + " ";
				encodedConfigurations += decodedConfigurations[22] + " ";
				encodedConfigurations += decodedConfigurations[23] + " ";

				// caller_maxreceivecallrate
				// callee1_maxreceivecallrate
				// callee2_maxreceivecallrate
				encodedConfigurations += decodedConfigurations[24] + " ";
				encodedConfigurations += decodedConfigurations[25] + " ";
				encodedConfigurations += decodedConfigurations[26] + " ";
				// caller_maxtransmitcallrate
				// callee1_maxtransmitcallrate
				// callee2_maxtransmitcallrate
				encodedConfigurations += decodedConfigurations[27] + " ";
				encodedConfigurations += decodedConfigurations[28] + " ";
				encodedConfigurations += decodedConfigurations[29] + " ";

				// caller_audiocodec
				if (decodedConfigurations[30].equals("Auto"))
					encodedConfigurations += "0 ";
				if (decodedConfigurations[30].equals("opus-48000"))
					encodedConfigurations += "1 ";
				if (decodedConfigurations[30].equals("SILK-24000"))
					encodedConfigurations += "2 ";
				if (decodedConfigurations[30].equals("SILK-16000"))
					encodedConfigurations += "3 ";
				if (decodedConfigurations[30].equals("G722-16000"))
					encodedConfigurations += "4 ";
				if (decodedConfigurations[30].equals("speex-32000"))
					encodedConfigurations += "5 ";
				if (decodedConfigurations[30].equals("speex-16000"))
					encodedConfigurations += "6 ";
				if (decodedConfigurations[30].equals("PCMU-8000"))
					encodedConfigurations += "7 ";
				if (decodedConfigurations[30].equals("PCMA-8000"))
					encodedConfigurations += "8 ";
				if (decodedConfigurations[30].equals("iLBC-8000"))
					encodedConfigurations += "9 ";
				if (decodedConfigurations[30].equals("GSM-8000"))
					encodedConfigurations += "10 ";
				if (decodedConfigurations[30].equals("speex-8000"))
					encodedConfigurations += "11 ";
				if (decodedConfigurations[30].equals("AMR-WB-16000"))
					encodedConfigurations += "12 ";
				if (decodedConfigurations[30].equals("SILK-12000"))
					encodedConfigurations += "13 ";
				if (decodedConfigurations[30].equals("SILK-8000"))
					encodedConfigurations += "14 ";
				if (decodedConfigurations[30].equals("telephone-event-80000"))
					encodedConfigurations += "15 ";
				// callee1_audiocodec
				if (decodedConfigurations[31].equals("Auto"))
					encodedConfigurations += "0 ";
				if (decodedConfigurations[31].equals("opus-48000"))
					encodedConfigurations += "1 ";
				if (decodedConfigurations[31].equals("SILK-24000"))
					encodedConfigurations += "2 ";
				if (decodedConfigurations[31].equals("SILK-16000"))
					encodedConfigurations += "3 ";
				if (decodedConfigurations[31].equals("G722-16000"))
					encodedConfigurations += "4 ";
				if (decodedConfigurations[31].equals("speex-32000"))
					encodedConfigurations += "5 ";
				if (decodedConfigurations[31].equals("speex-16000"))
					encodedConfigurations += "6 ";
				if (decodedConfigurations[31].equals("PCMU-8000"))
					encodedConfigurations += "7 ";
				if (decodedConfigurations[31].equals("PCMA-8000"))
					encodedConfigurations += "8 ";
				if (decodedConfigurations[31].equals("iLBC-8000"))
					encodedConfigurations += "9 ";
				if (decodedConfigurations[31].equals("GSM-8000"))
					encodedConfigurations += "10 ";
				if (decodedConfigurations[31].equals("speex-8000"))
					encodedConfigurations += "11 ";
				if (decodedConfigurations[31].equals("AMR-WB-16000"))
					encodedConfigurations += "12 ";
				if (decodedConfigurations[31].equals("SILK-12000"))
					encodedConfigurations += "13 ";
				if (decodedConfigurations[31].equals("SILK-8000"))
					encodedConfigurations += "14 ";
				if (decodedConfigurations[31].equals("telephone-event-80000"))
					encodedConfigurations += "15 ";
				// callee2_audiocodec
				if (decodedConfigurations[32].equals("Auto"))
					encodedConfigurations += "0 ";
				if (decodedConfigurations[32].equals("opus-48000"))
					encodedConfigurations += "1 ";
				if (decodedConfigurations[32].equals("SILK-24000"))
					encodedConfigurations += "2 ";
				if (decodedConfigurations[32].equals("SILK-16000"))
					encodedConfigurations += "3 ";
				if (decodedConfigurations[32].equals("G722-16000"))
					encodedConfigurations += "4 ";
				if (decodedConfigurations[32].equals("speex-32000"))
					encodedConfigurations += "5 ";
				if (decodedConfigurations[32].equals("speex-16000"))
					encodedConfigurations += "6 ";
				if (decodedConfigurations[32].equals("PCMU-8000"))
					encodedConfigurations += "7 ";
				if (decodedConfigurations[32].equals("PCMA-8000"))
					encodedConfigurations += "8 ";
				if (decodedConfigurations[32].equals("iLBC-8000"))
					encodedConfigurations += "9 ";
				if (decodedConfigurations[32].equals("GSM-8000"))
					encodedConfigurations += "10 ";
				if (decodedConfigurations[32].equals("speex-8000"))
					encodedConfigurations += "11 ";
				if (decodedConfigurations[32].equals("AMR-WB-16000"))
					encodedConfigurations += "12 ";
				if (decodedConfigurations[32].equals("SILK-12000"))
					encodedConfigurations += "13 ";
				if (decodedConfigurations[32].equals("SILK-8000"))
					encodedConfigurations += "14 ";
				if (decodedConfigurations[32].equals("telephone-event-80000"))
					encodedConfigurations += "15 ";

				// caller_videocodec
				if (decodedConfigurations[33].equals("Auto"))
					encodedConfigurations += "0 ";
				if (decodedConfigurations[33].equals("h264"))
					encodedConfigurations += "1 ";
				if (decodedConfigurations[33].equals("red"))
					encodedConfigurations += "2 ";
				if (decodedConfigurations[33].equals("rtx"))
					encodedConfigurations += "3 ";
				if (decodedConfigurations[33].equals("ulpfec"))
					encodedConfigurations += "4 ";
				if (decodedConfigurations[33].equals("VP8"))
					encodedConfigurations += "5 ";
				// callee1_videocodec
				if (decodedConfigurations[34].equals("Auto"))
					encodedConfigurations += "0 ";
				if (decodedConfigurations[34].equals("h264"))
					encodedConfigurations += "1 ";
				if (decodedConfigurations[34].equals("red"))
					encodedConfigurations += "2 ";
				if (decodedConfigurations[34].equals("rtx"))
					encodedConfigurations += "3 ";
				if (decodedConfigurations[34].equals("ulpfec"))
					encodedConfigurations += "4 ";
				if (decodedConfigurations[34].equals("VP8"))
					encodedConfigurations += "5 ";
				// callee2_videocodec
				if (decodedConfigurations[35].equals("Auto"))
					encodedConfigurations += "0 ";
				if (decodedConfigurations[35].equals("h264"))
					encodedConfigurations += "1 ";
				if (decodedConfigurations[35].equals("red"))
					encodedConfigurations += "2 ";
				if (decodedConfigurations[35].equals("rtx"))
					encodedConfigurations += "3 ";
				if (decodedConfigurations[35].equals("ulpfec"))
					encodedConfigurations += "4 ";
				if (decodedConfigurations[35].equals("VP8"))
					encodedConfigurations += "5 ";

				// caller_maxresolution
				if (decodedConfigurations[36].equals("1080"))
					encodedConfigurations += "0 ";
				if (decodedConfigurations[36].equals("720"))
					encodedConfigurations += "1 ";
				if (decodedConfigurations[36].equals("480"))
					encodedConfigurations += "2 ";
				if (decodedConfigurations[36].equals("360"))
					encodedConfigurations += "3 ";
				if (decodedConfigurations[36].equals("240"))
					encodedConfigurations += "4 ";
				// callee1_maxresolution
				if (decodedConfigurations[37].equals("1080"))
					encodedConfigurations += "0 ";
				if (decodedConfigurations[37].equals("720"))
					encodedConfigurations += "1 ";
				if (decodedConfigurations[37].equals("480"))
					encodedConfigurations += "2 ";
				if (decodedConfigurations[37].equals("360"))
					encodedConfigurations += "3 ";
				if (decodedConfigurations[37].equals("240"))
					encodedConfigurations += "4 ";
				// callee2_maxresolution
				if (decodedConfigurations[38].equals("1080"))
					encodedConfigurations += "0 ";
				if (decodedConfigurations[38].equals("720"))
					encodedConfigurations += "1 ";
				if (decodedConfigurations[38].equals("480"))
					encodedConfigurations += "2 ";
				if (decodedConfigurations[38].equals("360"))
					encodedConfigurations += "3 ";
				if (decodedConfigurations[38].equals("240"))
					encodedConfigurations += "4 ";

				encodedConfigurations += "\n";
			} // end of for loop
			Files.write(Paths.get(targetPath), encodedConfigurations.getBytes());
			System.out.println("Encoded configurations\n");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static int[][] ReadExistingConfigurations(String sourcePath, int numberOfVariables) {

		try {
			List<String> lines = Files.readAllLines(Paths.get(sourcePath), Charset.defaultCharset());
			int totalConfigurations = lines.size();
			int encodedConfigurations[][] = new int[totalConfigurations][numberOfVariables];

			for (int i = 0; i < lines.size(); i++) {
				// System.out.println("encoded configuration: " + lines.get(i));
				String[] variables = lines.get(i).split(" ");
				for (int j = 0; j < variables.length; j++) {
					encodedConfigurations[i][j] = Integer.parseInt(variables[j]);
				}
			}

			// for (int i = 0; i < encodedConfigurations.length; i++) {
			// for (int j = 0; j < encodedConfigurations[i].length; j++) {
			// System.out.print(encodedConfigurations[i][j] + " ");
			// }
			// System.out.println("\n");
			// }
			return encodedConfigurations;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
//		 ReadExistingConfigurations("Configurations/ExistingConfigurations/EncodedConfigurations.csv", 27);
		EncodeAllConfigurationsInFile("Configurations/ExistingConfigurations/DecodedConfigurations.csv",
				"Configurations/ExistingConfigurations/EncodedConfigurations.csv");

	}

}
