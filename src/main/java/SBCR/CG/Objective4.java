package SBCR.CG;

import jmetal.util.JMException;
import jmetal.util.wrapper.XInt;

public class Objective4 {
	public static double CalculateFitnessForDiversConfigurationsObjective(XInt currentSolution, int[][] exisitngConfigurations) {
		double fitness = 0;
		double normalizedValueForOneConfiguration = 0;
		double normalizedValueForAllConfigurations = 0;

		try {
			for (int rowIndex = 0; rowIndex < exisitngConfigurations.length; rowIndex++) {
				double sumOfDifferences = 0;
				double maxSumOfDifferences = 0;
				for (int variableIndex = 0; variableIndex < currentSolution.getNumberOfDecisionVariables(); variableIndex++) {
//					System.out.println("exisitngConfigurations: "+exisitngConfigurations[rowIndex][variableIndex] );
//					System.out.println("currentSolution: "+currentSolution.getValue(variableIndex));
					sumOfDifferences += Math.abs(exisitngConfigurations[rowIndex][variableIndex] - currentSolution.getValue(variableIndex));
					maxSumOfDifferences += currentSolution.getUpperBound(variableIndex)-currentSolution.getLowerBound(variableIndex);
				}
				normalizedValueForOneConfiguration = (sumOfDifferences/maxSumOfDifferences); //minSumOfDifferences=0, //(x-min/max-min) normalizaation function
				normalizedValueForAllConfigurations += normalizedValueForOneConfiguration;
			}
			fitness = 1- (normalizedValueForAllConfigurations / exisitngConfigurations.length);
//			System.out.println("Fitness for diverse configurations is : "+fitness);
			return fitness;

		} catch (JMException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return fitness;
	}



}
