package SBCR.CPLRules;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class RulesManipulationInDB {

	//*************Get Rules From DataBase for input query ******************
	public static Stream<CPLRule> getRulesFromDB(String query, Connection connection) throws SQLException{
		// Creating list of rules 
		List<CPLRule> listOfRules = new ArrayList<CPLRule>();		
		// Executing query 
		if (connection!=null){
			Statement statement = connection.createStatement();
			ResultSet resultSet = statement.executeQuery(query);
	
			while (resultSet.next()) {
				CPLRule rule = new CPLRule();
				rule.setExpression(resultSet.getString("expression"));
				rule.setSystemState(resultSet.getString("class"));
				rule.setSupport(resultSet.getInt("support"));
				rule.setViolation(resultSet.getInt("violation"));
				rule.setConfidence(resultSet.getDouble("confidence"));
				rule.setIteration(resultSet.getInt("iteration"));
				rule.setCluster(resultSet.getString("cluster"));
				rule.setIsNormalState(resultSet.getBoolean("isnormal"));
				listOfRules.add(rule);
			}
		}
		return listOfRules.parallelStream();
	}
	
	//*************Print the list of rules ******************
	public static void printListOfRules(List<CPLRule> listOfRules) {
		 for(CPLRule ruleItem : listOfRules) {
				System.out.println("{"+ ruleItem.getExpression()+" -> "+ ruleItem.getSystemState() +"} ("+ ruleItem.getSupport() +"/"+ ruleItem.getViolation() +"="+ ruleItem.getConfidence()+")");
		 }
	}

	//************* Execute insert query ****************** 
	public static void InsertRuleInToDB(String query,Connection connection) throws SQLException{
		if (connection!=null){
			Statement statement = connection.createStatement();
			statement.executeUpdate(query);
			System.out.println("Record added!!");
		}	
	}
	
	//************* read rules From a text file and add to DB ****************** 
	public static void readRulesFromTXTFileAndAddToDB(String inputFilePath, Connection connection) throws IOException, SQLException{
		//decided which one system is caller and which ones are callees in the rules.

		//read file into stream
		Stream<String> stream = Files.lines(Paths.get(inputFilePath));
		List<String> ruleList = stream.skip(1).map(String::toLowerCase).collect(Collectors.toList());
//			list.forEach(System.out::println);			
		for (String rule : ruleList) {
			String ruleParts [] =  rule.split("	");
//				String ruleParts [] =  rule.split("		");
			CPLRule currentRule= new CPLRule();
			currentRule.setExpression(ruleParts[0]);
			currentRule.setSystemState(ruleParts[1]);
			currentRule.setSupport(Integer.parseInt(ruleParts[2]));
			currentRule.setViolation(Integer.parseInt(ruleParts[3]));
			currentRule.setConfidence(Double.parseDouble(ruleParts[4]));
			currentRule.setCluster(ruleParts[5]);
			currentRule.setApparoach(ruleParts[6]);
			currentRule.setIteration(Integer.parseInt(ruleParts[7]));
			if(currentRule.getSystemState().contains("failed")){
				currentRule.setIsNormalState(false);
			}else{
				currentRule.setIsNormalState(true);
			}
//				currentRule.printRule();
			if (currentRule.getExpression().length()>3 && ( currentRule.getExpression().contains("caller")|| currentRule.getExpression().contains("network"))){
			String insertQuery = "INSERT INTO public.rules (expression,class,support,violation,confidence, cluster, approach, iteration,isnormal)  VALUES ('"+currentRule.getExpression()+"', '"+currentRule.getSystemState()+"', "+currentRule.getSupport()+","+currentRule.getViolation()+","+currentRule.getConfidence()+", '"+currentRule.getCluster()+"','"+currentRule.getApparoach()+"','"+(currentRule.getIteration()+1)+"','"+currentRule.isIsNormalState()+"');";
//				System.out.println(insertQuery);
			InsertRuleInToDB(insertQuery,connection) ;
			}
		}
	}
	
	//************* Execute insert query ****************** 
	public static void reaadRulesFromXLSXAndAddToTXTFile(String inputFilePath, String outputFilePath, String approach) throws IOException{
		//decided which one system is caller and which ones are callees in the rules.
		String caller="p1";
		String callee1="s2";
		String callee2="s3";
		int sheetNumber=-1;
		String content ="";
		
		File myFile = new File(inputFilePath);
		FileInputStream fis = new FileInputStream(myFile);
		XSSFWorkbook myWorkBook = new XSSFWorkbook(fis);
		sheetNumber=myWorkBook.getNumberOfSheets();
		while (sheetNumber>0){
			sheetNumber--;
			XSSFSheet mySheet = myWorkBook.getSheetAt(sheetNumber);				
			for (int i = 1; i < mySheet.getPhysicalNumberOfRows(); i++) {
				Row row = mySheet.getRow(i);		
				String currentRule = row.getCell(0).getStringCellValue()+"		"
						+row.getCell(1).getStringCellValue()+"		"
						+(int) row.getCell(3).getNumericCellValue()+"		"
						+(int) row.getCell(2).getNumericCellValue()+"		"
						+(double) row.getCell(4).getNumericCellValue()+"		"
						+row.getCell(5).getStringCellValue()+"		"
						+approach+"		"
						+(sheetNumber+1);
				if(!(currentRule.contains("expression")||row.getCell(0).getStringCellValue().length()<3)){
					content+=currentRule+"\n";
				}
			}	// end of for loop
		}// end of while loop
		content=content.replaceAll(callee1, "callee1").replaceAll(callee2, "callee2");
		System.err.println(content);
		String text = "Text to save to file";
		Files.write(Paths.get(outputFilePath), content.getBytes(),StandardOpenOption.APPEND);
	}

	//************* Remove Duplicate rules ****************** 
	public static void removeExactlyDuplicateRules(String filePath) throws IOException{
		Set<String> set = Files.lines(Paths.get(filePath)).collect(Collectors.toSet());
			String content = "";
		    if(set != null) {
		        StringBuilder sb = new StringBuilder();
		        Iterator<String> it = set.iterator();
		        if(it.hasNext()) {
		            sb.append(it.next());
		        }
		        while(it.hasNext()) {
		            sb.append("\n").append(it.next());
		        }
		        content = sb.toString();
		    }
		content=content.toLowerCase();
		System.out.println(content);
		Files.write(Paths.get(filePath),content.getBytes());
	}

	//************* Fix Variable names in rules for Jitsi****************** 
	public static void fixVariableNamesInRulesForJitsi(String filePath) throws IOException{
		File file = new File(filePath);
		String content = new String(Files.readAllBytes(file.toPath()), Charset.forName("UTF-8"));
	    content=content.replaceAll("protocol ","caller_protocol ").
	    replaceAll("protocol_callee1","callee1_protocol").
		replaceAll("protocol_callee2","callee2_protocol").
		replaceAll("siplistenport ","caller_listenport ").
		replaceAll("siplistenport_callee1","callee1_listenport").
		replaceAll("siplistenport_callee2","callee2_listenport").
		replaceAll("transportprotocol ","caller_transportprotocol ").
		replaceAll("transportprotocol_callee1","callee1_transportprotocol").
		replaceAll("transportprotocol_callee2","callee2_transportprotocol").
		replaceAll("encryption ","caller_encryption ").
		replaceAll("encryption_callee1","callee1_encryption").
		replaceAll("encryption_callee2","callee2_encryption").
		replaceAll("sipzrtpattribute ","caller_sipzrtpattribute ").
		replaceAll("sipzrtpattribute_callee1","callee1_sipzrtpattribute").
		replaceAll("sipzrtpattribute_callee2","callee2_sipzrtpattribute").
		replaceAll("maxpeersallowedinconferencecall ","caller_maxpeers ").
		replaceAll("maxpeersallowedinconferencecall_callee1","callee1_maxpeers").
		replaceAll("maxpeersallowedinconferencecall_callee2","callee2_maxpeers").
		replaceAll("mtu ","caller_mtu ").
		replaceAll("mtu_callee1","callee1_mtu").
		replaceAll("mtu_callee2","callee2_mtu").
		replaceAll("defaultcallrate ","caller_defaultcallrate ").
		replaceAll("defaultcallrate_callee1","callee1_defaultcallrate").
		replaceAll("defaultcallrate_callee2","callee2_defaultcallrate").
		replaceAll("maxreceivecallrate ","caller_maxreceivecallrate ").
		replaceAll("maxreceivecallrate_callee1","callee1_maxreceivecallrate").
		replaceAll("maxreceivecallrate_callee2","callee2_maxreceivecallrate	").
		replaceAll("maxtransmitcallrate ","caller_maxtransmitcallrate ").
		replaceAll("maxtransmitcallrate_callee1","callee1_maxtransmitcallrate").
		replaceAll("maxtransmitcallrate_callee2","callee2_maxtransmitcallrate").
		replaceAll("audiocodec ","caller_audioCodec ").
		replaceAll("audiocodec_callee1","callee1_audiocodec").
		replaceAll("audiocodec_callee2","callee2_audiocodec").
		replaceAll("videocodec","caller_videocodec ").
		replaceAll("videocodec_callee1","callee1_videocodec").
		replaceAll("videocodec_callee2","callee2_videocodec").
		replaceAll("maxresolution ","caller_maxresolution ").
		replaceAll("maxresolution_callee1","callee1_maxresolution").
		replaceAll("maxresolution_callee2","callee2_maxresolution");
		System.out.println(content);
		Files.write(Paths.get(filePath),content.getBytes());

/*				"caller_protocol"
				"callee1_protocol"
				"callee2_protocol"
				"caller_listenport"
				"callee1_listenport"
				"callee2_listenport"
				"caller_transportprotocol"
				"callee1_transportprotocol"
				"callee2_transportprotocol"
				"caller_encryption"
				"callee1_encryption"
				"callee2_encryption"
				"caller_sipzrtpattribute"
				"callee1_sipzrtpattribute"
				"callee2_sipzrtpattribute"
				"caller_maxpeers"
				"callee1_maxpeers"
				"callee2_maxpeers"
				"caller_mtu"
				"callee1_mtu"
				"callee2_mtu"
				"caller_defaultcallrate"
				"callee1_defaultcallrate"
				"callee2_defaultcallrate"
				"caller_maxreceivecallrate"
				"callee1_maxreceivecallrate"
				"callee2_maxreceivecallrate	"
				"caller_maxtransmitcallrate"
				"callee1_maxtransmitcallrate"
				"callee2_maxtransmitcallrate"
				"caller_audioCodec"
				"audioCodec_s2"
				"callee2_audioCodec_s3"
				"caller_videocodec"
				"callee1_videocodec"
				"callee2_videocodec"
				"caller_maxresolution"
				"callee1_maxresolution"
				"callee2_maxresolution"
*/
	}

	//************* Fix Variable names in rules for Jitsi****************** 
	public static void fixVariableNamesInRulesForCisco(String filePath) throws IOException{
		File file = new File(filePath);
		String content = new String(Files.readAllBytes(file.toPath()), Charset.forName("UTF-8"));
		content=content.replaceAll("encryption ","caller_encryption ").
		replaceAll("encryption_s2","callee1_encryption").
		replaceAll("encryption_s3","callee2_encryption").
		replaceAll("listenport ","caller_listenport ").
		replaceAll("listenport_s2","callee1_listenport").
		replaceAll("listenport_s3","callee2_listenport").
	    replaceAll("defaultcall_protocol ","caller_protocol ").
	    replaceAll("defaultcall_protocol_s1","callee1_protocol").
		replaceAll("defaultcall_protocol_s3","callee2_protocol").
		replaceAll("defaulttransport ","caller_transportprotocol ").
		replaceAll("defaulttransport_s2","callee1_transportprotocol").
		replaceAll("defaulttransport_s3","callee2_transportprotocol").
		replaceAll("ice_defaultcandidate ","caller_icedefaultcandidate ").
		replaceAll("ice_defaultcandidate_s2","callee1_icedefaultcandidate").
		replaceAll("ice_defaultcandidate_s3","callee2_icedefaultcandidate").
		replaceAll("mtu ","caller_mtu ").
		replaceAll("mtu_s2","callee1_mtu").
		replaceAll("mtu_s3","callee2_mtu").
		replaceAll("defaultcall_rate ","caller_defaultcallrate ").
		replaceAll("defaultcall_rate_s2","callee1_defaultcallrate").
		replaceAll("defaultcall_rate_s3","callee2_defaultcallrate").
		replaceAll("maxtransmitcallrate ","caller_maxtransmitcallrate ").
		replaceAll("maxtransmitcallrate_s2","callee1_maxtransmitcallrate").
		replaceAll("maxtransmitcallrate_s3","callee2_maxtransmitcallrate").
		replaceAll("maxreceivecallrate ","caller_maxreceivecallrate ").
		replaceAll("maxreceivecallrate_s2","callee1_maxreceivecallrate").
		replaceAll("maxreceivecallrate_s3","callee2_maxreceivecallrate	");	
	    System.out.println(content);
		Files.write(Paths.get(filePath),content.getBytes());
	}
		
	public static void main(String[] args) throws SQLException, IOException {
		
		//********* Creating connection with DB ********* 
		Connection connection = JDBC.connectToDB("JitsiRulesDB", "postgres", "gramer");

		//********* Converting rules form XLSX file to txtFile ********* 
//		for (int i=1;i<10;i++){
//		reaadRulesFromXLSXAndAddToTXTFile( "RulesFolder/Cisco Rules/constraints-Random-C45-R"+i+".xlsx", "RulesFolder/CiscoRules.txt","SBRM-C45") ;
//		}
		
		//********* removes duplicate rules from text file ********* 
//		removeExactlyDuplicateRules("RulesFolder/CiscoRules.txt");
		

		//********* fix variable names in the rules ********* 
//		fixVariableNamesInRulesForCisco("RulesFolder/CiscoRules.txt");
		
		
		//********* Adding to DB from txtFile ********* 	
		readRulesFromTXTFileAndAddToDB("RulesFolder/CiscoRules.txt", connection);
		
		//********* retrieving rules from DB ********* 	
//		String query = "SELECT * FROM public.rules";
//		long t1= System.currentTimeMillis();
//		List<CPLRules> listOfRules = getRulesFromDB(query,connection) ;
//		printListOfRules(listOfRules) ;
//		long t2= System.currentTimeMillis();
//		System.out.println((t2-t1)+" mili seconds" );
	} 



}