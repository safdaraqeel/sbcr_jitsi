package SBCR.CPLRules;

public class CPLRule {
	private String expression;
	private String systemState;
	private double support;
	private double violation;
	private double confidence;
	private String cluster;
	private String apparoach;
	private int iteration;
	private double distance;
	private boolean isNormalState;

	public CPLRule(){
		this.expression="";
		this.systemState="";
		this.support=-1;
		this.violation=-1;
		this.confidence=-1;
		this.cluster="";
		this.apparoach="";
		this.iteration=-1;
		this.distance=0;
	}
	
	
	public String getExpression() {
		return expression;
	}


	public void setExpression(String expression) {
		this.expression = expression;
	}


	public String getSystemState() {
		return systemState;
	}


	public void setSystemState(String systemState) {
		this.systemState = systemState;
	}


	public double getSupport() {
		return support;
	}


	public void setSupport(double support) {
		this.support = support;
	}


	public double getViolation() {
		return violation;
	}


	public void setViolation(double violation) {
		this.violation = violation;
	}


	public double getConfidence() {
		return confidence;
	}


	public void setConfidence(double confidence) {
		this.confidence = confidence;
	}


	public String getCluster() {
		return cluster;
	}


	public void setCluster(String cluster) {
		this.cluster = cluster;
	}


	public String getApparoach() {
		return apparoach;
	}


	public void setApparoach(String apparoach) {
		this.apparoach = apparoach;
	}


	public int getIteration() {
		return iteration;
	}


	public void setIteration(int iteration) {
		this.iteration = iteration;
	}


	public double getDistance() {
		return distance;
	}


	public void setDistance(double distance) {
		this.distance = distance;
	}


	public boolean isIsNormalState() {
		return isNormalState;
	}


	public void setIsNormalState(boolean isNormalState) {
		this.isNormalState = isNormalState;
	}


	public void printRule() {
		System.out.println("{" + this.expression + " -> System State: " + this.systemState+ "}	(" + this.support
		+ "/" + this.violation +"=" + this.confidence+ ")["+this.cluster+"	"+this.apparoach+"	"+this.iteration+"]");
	}

	
	
	
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
