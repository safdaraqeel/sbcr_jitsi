package SBCR.CPLRules;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public class ReadCPLRulesFromXL {

	public static Stream<CPLRule> read(String filePath) {
		List<CPLRule> listOfRules = new ArrayList<CPLRule>();
		try (Stream<String> lines = Files.lines(Paths.get(filePath), Charset.defaultCharset())) {
			lines.forEachOrdered(line -> listOfRules.add(convert(line)));
//			System.out.println("Total Rules are: " + listOfRules.size());
		} catch (IOException e) {
			e.printStackTrace();
		}
		return listOfRules.parallelStream();
	}

	public static CPLRule convert(String line){
		CPLRule rule = new CPLRule ();
		if (!line.contains("expression")){
//			System.out.println(line);
			String [] stringArray = line.split(";");
			if (stringArray.length>= 10){
				//0-expression
				rule.setExpression(stringArray[0]);
				//1-class
				rule.setSystemState(stringArray[1]);
				//2-support
				rule.setSupport(Integer.parseInt(stringArray[2]));
				// 3-violation
				rule.setViolation(Integer.parseInt(stringArray[3]));
				// 4-confidence
				rule.setConfidence(Double.parseDouble(stringArray[4]));
				// 5-rid
				// 6-cluster
				rule.setCluster(stringArray[6]);
				// 7-approach
				// 8-iteration
				rule.setIteration(Integer.parseInt(stringArray[8]));
				// 9-isnormal
				if (stringArray[9].contains("t")){
					rule.setIsNormalState(Boolean.TRUE);
				} else if (stringArray[9].contains("f")){
					rule.setIsNormalState(Boolean.FALSE);
				}
			}
		}
//		rule.printRule();
		return rule;
	}


	public static void main(String[] args) {
		String fileName = "irace/execDir/CPLRules/JitsiSelectedRules.csv";
		read(fileName);
	}

}
