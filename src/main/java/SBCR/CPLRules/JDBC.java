package SBCR.CPLRules;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;


public class JDBC {

	public JDBC() {
		// TODO Auto-generated constructor stub
	}
	

	
	public static Connection connectToDB(String db, String username, String password) {
		Connection connection = null;
//		System.out.println("-------- PostgreSQL " + "JDBC Connection Testing ------------");

		try {
			Class.forName("org.postgresql.Driver");
		} catch (ClassNotFoundException e) {
			System.out.println("Where is your PostgreSQL JDBC Driver? " + "Include in your library path!");
			e.printStackTrace();
		}
//		System.out.println("PostgreSQL JDBC Driver Registered!");
		try {
			connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/"+db,username, password);
			
			if (connection != null) {
//				System.out.println("You are connected!");
			} else {
				System.out.println("Failed to make connection!");
			}
		} catch (SQLException e) {
			System.out.println("Connection Failed! Check output console");
			e.printStackTrace();
		}

		return connection;
	}

	
	
	
	
	public static void main(String[] args) throws SQLException {
	// TODO Auto-generated method stub
	Connection connection = JDBC.connectToDB("JitsiRulesDB", "postgres", "gramer");
	}

}