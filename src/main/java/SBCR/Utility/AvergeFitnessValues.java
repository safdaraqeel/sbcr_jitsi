package SBCR.Utility;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

public class AvergeFitnessValues {
	public static void getAllAvgFitnessValues() throws IOException{
		int totalRuns=30;
		String [] searchAlgo={"NSGA2","IBEA","MoCell","SPEA2","SMPSO","PAES","RS"};
		String data= "Algo;Run; Obj-1; Obj-2; Obj-3; Obj-4\n";
			for (int s = 0; s < searchAlgo.length; s++) {
				for (int r = 1; r <= totalRuns; r++) {
					String path = "ExperimentResultsWithBP/Jitsi_" + searchAlgo[s] + "/RUN " + r + "/FUN_" + r;
					System.out.println(path);
					double [] avgFitness=getAverage(path);
					data+= searchAlgo[s] +";"+ r+";" + avgFitness[0]+";"+avgFitness[1]+";"+avgFitness[2]+";"+avgFitness[3]+"\n";
				}// end of for (r)
			}// end of for (s)

		System.out.println(data);
		
		String destinationPath="temp/AvgFitnessValues.csv";
    	boolean f= new File(destinationPath).createNewFile();
    	Files.write(Paths.get(destinationPath), data.getBytes());
    	System.out.println("Done");		
	}
	
	public static double [] getAverage(String path) throws IOException{
						String fitnessValues = new String(Files.readAllBytes(Paths.get(path)), StandardCharsets.UTF_8);
						String []fitnessArray=fitnessValues.split(System.lineSeparator());
						double [] avgFitness={0,0,0,0};
						System.out.println("Total Solutions are"+ fitnessArray.length);
						for (int f = 0; f < fitnessArray.length; f++) {
							avgFitness[0]+= Double.parseDouble(fitnessArray[f].split(" ")[0]);
							avgFitness[1]+= Double.parseDouble(fitnessArray[f].split(" ")[1]);
							avgFitness[2]+= Double.parseDouble(fitnessArray[f].split(" ")[2]);
							avgFitness[3]+= Double.parseDouble(fitnessArray[f].split(" ")[3]);
						}
						avgFitness[0]=avgFitness[0]/fitnessArray.length;
						avgFitness[1]=avgFitness[1]/fitnessArray.length;
						avgFitness[2]=avgFitness[2]/fitnessArray.length;
						avgFitness[3]=avgFitness[3]/fitnessArray.length;
						System.out.println("{"+avgFitness[0]+","+avgFitness[1]+","+avgFitness[2]+","+avgFitness[3]+"}");
						return avgFitness;
	}// end of getAverage
	
	public static void main(String[] args) throws IOException {
		getAllAvgFitnessValues() ;
	}
}
