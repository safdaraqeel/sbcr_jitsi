package SBCR.Utility;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

public class FaultyConfigurationsStatistics {

	public static void main(String[] args) throws IOException {

		String [] searchAlgos={"NSGA2","IBEA","MoCell","SPEA2","SMPSO","PAES","RS"};
		String results="Algorithm; Run; Faulty; Non-Faulty; Total; Faulty %; Non-Faulty %\n";
		for (int i = 0; i < searchAlgos.length; i++) {
			String filePath= "ExperimentResultsWithBP/Jitsi_"+searchAlgos[i]+"/AllRecommenededConfigurations"+searchAlgos[i]+".csv";
			results += getStatisticsForFaultyConfigurations(filePath,searchAlgos[i]);
		}

		Files.write(Paths.get("AnalysisResults/FaultyConfigurationsStatistics/FaultyConfigurationResults.csv"), results.getBytes());
	}




	public static String getStatisticsForFaultyConfigurations(String sourceFilePath, String algoName) throws IOException {
		String data = "";
		String statistics = new String(Files.readAllBytes(Paths.get(sourceFilePath)), StandardCharsets.UTF_8);
		double [] percentage= {0,0,0,0,0}; //faulty,non-faulty, total,faulty%,non-faulty%,
		String[] statisticsForAllRuns = statistics.split("\ncaller_protocol;callee1_protocol;callee2_protocol;caller_listenport;callee1_listenport;callee2_listenport;caller_transportprotocol;callee1_transportprotocol;callee2_transportprotocol;caller_encryption;callee1_encryption;callee2_encryption;caller_sipzrtpattribute;callee1_sipzrtpattribute;callee2_sipzrtpattribute;caller_maxpeers;callee1_maxpeers;callee2_maxpeers;caller_mtu;callee1_mtu;callee2_mtu;caller_defaultcallrate;callee1_defaultcallrate;callee2_defaultcallrate;caller_maxreceivecallrate;callee1_maxreceivecallrate;callee2_maxreceivecallrate;caller_maxtransmitcallrate;callee1_maxtransmitcallrate;callee2_maxtransmitcallrate;caller_audiocodec;callee1_audiocodec;callee2_audiocodec;caller_videocodec;callee1_videocodec;callee2_videocodec;caller_maxresolution;callee1_maxresolution;callee2_maxresolution;");
		System.out.println("\nTotal Runs ar: "+statisticsForAllRuns.length);
		double overalFaulty = 0;
		double overalNonFaulty = 0;
		double overalTotal = 0;
		for (int i = 0; i < statisticsForAllRuns.length; i++) {
			double faultyPerRun = 0;
			double nonFaultyPerRun = 0;
			statisticsForAllRuns[i] = statisticsForAllRuns[i].replaceAll("caller_protocol;callee1_protocol;callee2_protocol;caller_listenport;callee1_listenport;callee2_listenport;caller_transportprotocol;callee1_transportprotocol;callee2_transportprotocol;caller_encryption;callee1_encryption;callee2_encryption;caller_sipzrtpattribute;callee1_sipzrtpattribute;callee2_sipzrtpattribute;caller_maxpeers;callee1_maxpeers;callee2_maxpeers;caller_mtu;callee1_mtu;callee2_mtu;caller_defaultcallrate;callee1_defaultcallrate;callee2_defaultcallrate;caller_maxreceivecallrate;callee1_maxreceivecallrate;callee2_maxreceivecallrate;caller_maxtransmitcallrate;callee1_maxtransmitcallrate;callee2_maxtransmitcallrate;caller_audiocodec;callee1_audiocodec;callee2_audiocodec;caller_videocodec;callee1_videocodec;callee2_videocodec;caller_maxresolution;callee1_maxresolution;callee2_maxresolution;","");
			for (int j = 0; j < statisticsForAllRuns[i].split("\n").length; j++) {
				if (statisticsForAllRuns[i].split("\n")[j].length() > 50) { // to avoid empty lines or with ";;;;;;;;;;"
					if (statisticsForAllRuns[i].split("\n")[j].split(";")[40].contains("ConnectedConnected")||statisticsForAllRuns[i].split("\n")[j].split(";")[40].contains("Disconnecting")) {
						nonFaultyPerRun+=1;
					} else if (statisticsForAllRuns[i].split("\n")[j].split(";")[40].contains("Failed")||statisticsForAllRuns[i].split("\n")[j].split(";")[40].contains("Ringing")||statisticsForAllRuns[i].split("\n")[j].split(";")[40].contains("Connecting")) {
						faultyPerRun+=1;
					}
				}
			}

			double totalPerRun = faultyPerRun+nonFaultyPerRun;
			overalFaulty += faultyPerRun;
			overalNonFaulty += nonFaultyPerRun;
			overalTotal += totalPerRun;
			data +=  algoName + ";Run-"+(i+1)+";" + faultyPerRun + ";" + nonFaultyPerRun + ";" + totalPerRun + ";" + (faultyPerRun/totalPerRun)+ ";" + (nonFaultyPerRun/totalPerRun)+"\n";
		}
		data +=  algoName + ";Overal"+";" + overalFaulty + ";" + overalNonFaulty + ";" + overalTotal + ";" + (overalFaulty/overalTotal)+ ";" + (overalNonFaulty/overalTotal)+"\n";
		System.out.println(data);
		return data;
	}

}
