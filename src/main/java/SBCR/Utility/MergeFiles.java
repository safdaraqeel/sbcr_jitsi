package SBCR.Utility;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

public class MergeFiles {
	
	public static void mergeTwoFiles(String sourceFilePath, String destinationFilePath) {

		try {
			String sourceData = new String(Files.readAllBytes(Paths.get(sourceFilePath)), StandardCharsets.UTF_8);
//			sourceData=sourceData.replaceAll("product1_DefaultCallProtocol;product1_ListenPort;product1_DefaultTransportProtocol;product1_Encryption;product1_SipZrtpAttribute;product1_AudioCodec;product1_VideoCodec;product1_Resolution;product1_SipMode;product1_H323Mode;product1_MTU;product1_DefaultCallRate;product1_MaxReceivedCallRate;product1_MaxTransmitedCallRate;product2_DefaultCallProtocol;product2_ListenPort;product2_DefaultTransportProtocol;product2_Encryption;product2_SipZrtpAttribute;product2_AudioCodec;product2_VideoCodec;product2_Resolution;product2_SipMode;product2_H323Mode;product2_MTU;product2_DefaultCallRate;product2_MaxReceivedCallRate;product2_MaxTransmitedCallRate;State", "");
			File file = new File(destinationFilePath);
			if(!file.exists()){
			  file.createNewFile();
			}
			Files.write(Paths.get(destinationFilePath), sourceData.getBytes(),StandardOpenOption.APPEND);
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public static void mergeAllFiles(String firstFolderPath, String fileName, String destinationFilePath, int indexForSolutionToPick) {

		try {

			String aLLData="";
			int version = 1;
			File sourceFile = new File(firstFolderPath+version);
			while (sourceFile.exists()){

				String [] rows =new String(Files.readAllBytes(Paths.get(firstFolderPath+version+"/"+fileName)), StandardCharsets.UTF_8).split("\n");
				while (rows.length<=indexForSolutionToPick){
					indexForSolutionToPick-=1;
				}
				
//				System.out.println(firstFolderPath+version+fileName+", "+rows.length+" and "+indexForSolutionToPick);
//				String selectedSolution = new String(Files.readAllBytes(Paths.get(firstFolderPath+version+"/"+fileName)), StandardCharsets.UTF_8).split("\n")[indexForSolutionToPick];
				aLLData += sourceFile.getName()+";"+rows[indexForSolutionToPick]+"\n";
				sourceFile = new File(firstFolderPath + (++version));
			}
			// lines to skip
//			aLLData=aLLData.replaceAll("caller_protocol;callee1_protocol;callee2_protocol;caller_listenport;callee1_listenport;callee2_listenport;caller_transportprotocol;callee1_transportprotocol;callee2_transportprotocol;caller_encryption;callee1_encryption;callee2_encryption;caller_sipzrtpattribute;callee1_sipzrtpattribute;callee2_sipzrtpattribute;caller_maxpeers;callee1_maxpeers;callee2_maxpeers;caller_mtu;callee1_mtu;callee2_mtu;caller_defaultcallrate;callee1_defaultcallrate;callee2_defaultcallrate;caller_maxreceivecallrate;callee1_maxreceivecallrate;callee2_maxreceivecallrate;caller_maxtransmitcallrate;callee1_maxtransmitcallrate;callee2_maxtransmitcallrate;caller_audiocodec;callee1_audiocodec;callee2_audiocodec;caller_videocodec;callee1_videocodec;callee2_videocodec;caller_maxresolution;callee1_maxresolution;callee2_maxresolution;", "");
	
			Files.write(Paths.get(destinationFilePath), aLLData.getBytes(),StandardOpenOption.APPEND);
			System.out.println("All Files are merged");
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	
	public static void fetchAndMergeFitnessValues(String firstFolderPath, String fileName, String destinationFilePath) {

		try {

			String aLLData="";
			int version = 1;
			File sourceFile = new File(firstFolderPath+version);
			while (sourceFile.exists()){
				String selectedSolution = new String(Files.readAllBytes(Paths.get(firstFolderPath+version+"/"+fileName)), StandardCharsets.UTF_8).split("\n")[1];
				aLLData += sourceFile.getName()+";"+selectedSolution+"\n";
				sourceFile = new File(firstFolderPath + (++version));
			}
			// lines to skip
//			aLLData=aLLData.replaceAll("caller_protocol;callee1_protocol;callee2_protocol;caller_listenport;callee1_listenport;callee2_listenport;caller_transportprotocol;callee1_transportprotocol;callee2_transportprotocol;caller_encryption;callee1_encryption;callee2_encryption;caller_sipzrtpattribute;callee1_sipzrtpattribute;callee2_sipzrtpattribute;caller_maxpeers;callee1_maxpeers;callee2_maxpeers;caller_mtu;callee1_mtu;callee2_mtu;caller_defaultcallrate;callee1_defaultcallrate;callee2_defaultcallrate;caller_maxreceivecallrate;callee1_maxreceivecallrate;callee2_maxreceivecallrate;caller_maxtransmitcallrate;callee1_maxtransmitcallrate;callee2_maxtransmitcallrate;caller_audiocodec;callee1_audiocodec;callee2_audiocodec;caller_videocodec;callee1_videocodec;callee2_videocodec;caller_maxresolution;callee1_maxresolution;callee2_maxresolution;", "");
			Files.write(Paths.get(destinationFilePath), aLLData.getBytes(),StandardOpenOption.APPEND);
			System.out.println("All Files are merged");
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

		
	public static void main(String[] args) {
//		mergeTwoFiles("AllFiles/InitialConfigurationsAndStatuses.csv", "AllFiles/ConfigurationsAndStatusesRun1.csv"); 
	
	
		mergeAllFiles("Configurations/Recommended/C", "VAR.csv", "Configurations/AllGeneratedConfigurations.csv",1) ;
	
	
	}
	
	
	
	

}
