package SBCR.Utility;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;

public class AlreadyRecommendedConfigurations {

	public static String[] readInitialConfigurationForCaller() {
		String[] variables = null;
		try {

			File file = new File("Configurations/InitialConfiguration.txt");
			String content = new String(Files.readAllBytes(file.toPath()), Charset.forName("UTF-8"));
			content = content.toLowerCase().replaceAll(" ", "").replaceAll("caller_encryption", "").replaceAll("caller_listenport", "").replaceAll("caller_protocol", "").replaceAll("caller_transportprotocol", "").replaceAll("caller_icedefaultcandidate", "").replaceAll("caller_mtu", "").replaceAll("caller_defaultcallrate", "").replaceAll("caller_maxtransmitcallrate", "")
					.replaceAll("caller_maxreceivecallrate", "").replaceAll("caller_qosmode", "").replaceAll("caller_sipmode", "").replaceAll("caller_h323mode", "").replaceAll("caller_qosaudio", "").replaceAll("caller_qosvideo", "").replaceAll("=", "").replaceAll("network_packetloss", "").replaceAll("network_audiojitter", "").replaceAll("network_videojitter", "").replaceAll("=", "");
			variables = content.split("\n");

			// encryption
			variables[0] = variables[0].replace("off", "0");
			variables[0] = variables[0].replace("on", "1");
			variables[0] = variables[0].replace("besteffort", "2");
			// listen port
			variables[1] = variables[1].replace("off", "0");
			variables[1] = variables[1].replace("on", "1");
			// protocol
			variables[2] = variables[2].replace("auto", "0");
			variables[2] = variables[2].replace("sip", "1");
			variables[2] = variables[2].replace("h323", "2");
			variables[2] = variables[2].replace("h320", "3");
			// transport protocol
			variables[3] = variables[3].replace("auto", "0");
			variables[3] = variables[3].replace("tcp", "1");
			variables[3] = variables[3].replace("tls", "2");
			variables[3] = variables[3].replace("udp", "3");
			// ice default candidate
			variables[4] = variables[4].replace("host", "0");
			variables[4] = variables[4].replace("relay", "1");
			variables[4] = variables[4].replace("rflx", "2");

			// caller_mtu = 600
			// caller_defaultcallrate = 2000
			// caller_maxtransmitcallrate = 3000
			// caller_maxreceivecallrate = 3000
			
//			QoSMode = x.getValue(9);
			variables[9] = variables[9].replace("off", "0");
			variables[9] = variables[9].replace("diffserv", "1");
//			SipMode = x.getValue(10);
			variables[10] = variables[10].replace("off", "0");
			variables[10] = variables[10].replace("on", "1");
//			H323Mode = x.getValue(11);
			variables[11] = variables[11].replace("off", "0");
			variables[11] = variables[11].replace("on", "1");
//			QoSAudio = x.getValue(12);
//			QoSVideo = x.getValue(13);

//			network_packetloss = x.getValue(14);
//			network_audiojitter = x.getValue(15);
//			network_videojitter = x.getValue(16);
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return variables;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String [] variables =readInitialConfigurationForCaller();
		

		for (int i = 0; i < variables.length; i++) {
			System.err.println(variables[i]);
		}
		
		
	}

}
