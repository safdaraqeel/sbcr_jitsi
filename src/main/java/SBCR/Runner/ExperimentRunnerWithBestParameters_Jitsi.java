package SBCR.Runner;

import SBCR.Utility.MergeFiles;
import jmetal.metaheuristics.ibea.IBEA_main;
import jmetal.metaheuristics.mocell.MOCell_main;
import jmetal.metaheuristics.nsgaII.NSGAII_main;
import jmetal.metaheuristics.paes.PAES_main;
import jmetal.metaheuristics.smpso.SMPSO_main;
import jmetal.metaheuristics.spea2.SPEA2_main;
import jmetal.util.JMException;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

public class ExperimentRunnerWithBestParameters_Jitsi {
	private static void runMoCell() throws SecurityException, ClassNotFoundException, JMException, IOException {
		for (int candidateId = 1; candidateId <=30; candidateId++) {
			String[] para = {Integer.toString(candidateId),
					Integer.toString(100),//populationSize
					Integer.toString(50000),//maxEvaluations
					Double.toString(0.29),//crossoverProbability
					Double.toString(0.01),    //mutationProbability
					"MoCell"//algorithmName
//					1  --algorithm MoCell --populationSize=100 --maxEvaluations=50000 --selectionOperator=KTournament --tournamentSize 2 --crossoverOperator=SinglePoint --crossoverProbability=0.29 --mutationOperator=BitFlipMutation --mutationProbability 0.01
//					2  --algorithm MoCell --populationSize=100 --maxEvaluations=50000 --selectionOperator=KTournament --tournamentSize 2 --crossoverOperator=SinglePoint --crossoverProbability=0.3 --mutationOperator=BitFlipMutation --mutationProbability 0.01
//					3  --algorithm MoCell --populationSize=100 --maxEvaluations=50000 --selectionOperator=KTournament --tournamentSize 2 --crossoverOperator=SinglePoint --crossoverProbability=0.21 --mutationOperator=BitFlipMutation --mutationProbability 0.01
			};

			long estimatedTime = MOCell_main.main(para);
			// writing time for generated configuration solution
			String time = "Time Taken For MoCell R" + (candidateId) + ":-	 " + estimatedTime + "\n";
			System.out.println(time);
			new File("MoCellTime.csv").createNewFile();
			Files.write(Paths.get("MoCellTime.csv"), time.getBytes(), StandardOpenOption.APPEND);

//				Merging files
			String destinationFilePath = "AllRecommenededConfigurationsMoCell.csv";
			if (!new File(destinationFilePath).exists()) {
				new File(destinationFilePath).createNewFile();
			}
			MergeFiles.mergeTwoFiles("VAR_" + candidateId + ".csv", destinationFilePath);

			// Moving files
			File dest = new File("RUN " + candidateId + "/");
			if (!dest.exists()) {
				dest.mkdirs();
			}
			FileUtils.moveFileToDirectory(new File("VAR_" + candidateId), dest, false);
			FileUtils.moveFileToDirectory(new File("FUN_" + candidateId), dest, false);
			FileUtils.moveFileToDirectory(new File("VAR_" + candidateId + ".csv"), dest, false);
			System.out.println("Done Executing");
		}
	}

	private static void runSMPSO() throws SecurityException, ClassNotFoundException, JMException, IOException {
		for (int candidateId=1; candidateId<=30;candidateId++){
			String[] para = {Integer.toString(candidateId),
					Integer.toString(100),//populationSize
					Integer.toString(50000),//maxEvaluations
					Double.toString(0.87),//crossoverProbability
					Double.toString(0.18),	//mutationProbability
					"SMPSO"//algorithmName
//			1  --algorithm SMPSO --populationSize=100 --maxEvaluations=50000 --selectionOperator=KTournament --tournamentSize 2 --crossoverOperator=SinglePoint --crossoverProbability=0.65 --mutationOperator=BitFlipMutation --mutationProbability 0.29
//			2  --algorithm SMPSO --populationSize=100 --maxEvaluations=50000 --selectionOperator=KTournament --tournamentSize 2 --crossoverOperator=SinglePoint --crossoverProbability=0.87 --mutationOperator=BitFlipMutation --mutationProbability 0.18 (selected)
//			3  --algorithm SMPSO --populationSize=100 --maxEvaluations=50000 --selectionOperator=KTournament --tournamentSize 2 --crossoverOperator=SinglePoint --crossoverProbability=0.76 --mutationOperator=BitFlipMutation --mutationProbability 0.41
			};

			long estimatedTime = SMPSO_main.main(para);
			// writing time for generated configuration solution
			String time="Time Taken For SMPSO R"+(candidateId)+":-	 "+estimatedTime+"\n";
			System.out.println(time);
			new File("SMPSOTime.csv").createNewFile();
			Files.write(Paths.get("SMPSOTime.csv"), time.getBytes(), StandardOpenOption.APPEND);

//				Merging files
			String destinationFilePath="AllRecommenededConfigurationsSMPSO.csv";
			if (!new File(destinationFilePath).exists()){
				new File(destinationFilePath).createNewFile();
			}
			MergeFiles.mergeTwoFiles("VAR_" + candidateId + ".csv", destinationFilePath);

			// Moving files
			File dest = new File("RUN " + candidateId + "/");
			if (!dest.exists()) {
				dest.mkdirs();
			}
			FileUtils.moveFileToDirectory(new File("VAR_" + candidateId), dest, false);
			FileUtils.moveFileToDirectory(new File("FUN_" + candidateId), dest, false);
			FileUtils.moveFileToDirectory(new File("VAR_" + candidateId + ".csv"), dest, false);
			System.out.println("Done Executing");
		}
	}

	private static void runPAES() throws SecurityException, ClassNotFoundException, JMException, IOException {
		for (int candidateId=1; candidateId<=30;candidateId++){
			String[] para = {Integer.toString(candidateId),
					Integer.toString(100),//populationSize
					Integer.toString(50000),//maxEvaluations
					Double.toString(0.6),//crossoverProbability
					Double.toString(0.75),	//mutationProbability
					"PAES"//algorithmName
//					1  --algorithm PAES --populationSize=100 --maxEvaluations=50000 --selectionOperator=KTournament --tournamentSize 2 --crossoverOperator=SinglePoint --crossoverProbability=0.47 --mutationOperator=BitFlipMutation --mutationProbability 0.79
//					2  --algorithm PAES --populationSize=100 --maxEvaluations=50000 --selectionOperator=KTournament --tournamentSize 2 --crossoverOperator=SinglePoint --crossoverProbability=0.43 --mutationOperator=BitFlipMutation --mutationProbability 0.74
//					3  --algorithm PAES --populationSize=100 --maxEvaluations=50000 --selectionOperator=KTournament --tournamentSize 2 --crossoverOperator=SinglePoint --crossoverProbability=0.6 --mutationOperator=BitFlipMutation --mutationProbability 0.75
			};

			long estimatedTime = PAES_main.main(para);
			// writing time for generated configuration solution
			String time="Time Taken For PAES R"+(candidateId)+":-	 "+estimatedTime+"\n";
			System.out.println(time);
			new File("PAESTime.csv").createNewFile();
			Files.write(Paths.get("PAESTime.csv"), time.getBytes(), StandardOpenOption.APPEND);

//				Merging files
			String destinationFilePath="AllRecommenededConfigurationsPAES.csv";
			if (!new File(destinationFilePath).exists()){
				new File(destinationFilePath).createNewFile();
			}
			MergeFiles.mergeTwoFiles("VAR_" + candidateId + ".csv", destinationFilePath);

			// Moving files
			File dest = new File("RUN " + candidateId + "/");
			if (!dest.exists()) {
				dest.mkdirs();
			}
			FileUtils.moveFileToDirectory(new File("VAR_" + candidateId), dest, false);
			FileUtils.moveFileToDirectory(new File("FUN_" + candidateId), dest, false);
			FileUtils.moveFileToDirectory(new File("VAR_" + candidateId + ".csv"), dest, false);
			System.out.println("Done Executing");
		}
	}

	private static void runSPEA2() throws SecurityException, ClassNotFoundException, JMException, IOException {
		for (int candidateId=1; candidateId<=30;candidateId++){
			String[] para = {Integer.toString(candidateId),
					Integer.toString(100),//populationSize
					Integer.toString(50000),//maxEvaluations
					Double.toString(0.62),//crossoverProbability
					Double.toString(0.01),	//mutationProbability
					"SPEA2"//algorithmName
//					1  --algorithm SPEA2 --populationSize=100 --maxEvaluations=50000 --selectionOperator=KTournament --tournamentSize 2 --crossoverOperator=SinglePoint --crossoverProbability=0.38 --mutationOperator=BitFlipMutation --mutationProbability 0.01
//					2  --algorithm SPEA2 --populationSize=100 --maxEvaluations=50000 --selectionOperator=KTournament --tournamentSize 2 --crossoverOperator=SinglePoint --crossoverProbability=0.62 --mutationOperator=BitFlipMutation --mutationProbability 0.01
//					3  --algorithm SPEA2 --populationSize=100 --maxEvaluations=50000 --selectionOperator=KTournament --tournamentSize 2 --crossoverOperator=SinglePoint --crossoverProbability=0.5 --mutationOperator=BitFlipMutation --mutationProbability 0.01
			};

			long estimatedTime = SPEA2_main.main(para);
			// writing time for generated configuration solution
			String time="Time Taken For SPEA2 R"+(candidateId)+":-	 "+estimatedTime+"\n";
			System.out.println(time);
			new File("SPEA2Time.csv").createNewFile();
			Files.write(Paths.get("SPEA2Time.csv"), time.getBytes(), StandardOpenOption.APPEND);

//				Merging files
			String destinationFilePath="AllRecommenededConfigurationsSPEA2.csv";
			if (!new File(destinationFilePath).exists()){
				new File(destinationFilePath).createNewFile();
			}
			MergeFiles.mergeTwoFiles("VAR_" + candidateId + ".csv", destinationFilePath);

			// Moving files
			File dest = new File("RUN " + candidateId + "/");
			if (!dest.exists()) {
				dest.mkdirs();
			}
			FileUtils.moveFileToDirectory(new File("VAR_" + candidateId), dest, false);
			FileUtils.moveFileToDirectory(new File("FUN_" + candidateId), dest, false);
			FileUtils.moveFileToDirectory(new File("VAR_" + candidateId + ".csv"), dest, false);
			System.out.println("Done Executing");
		}
	}

	private static void runIBEA() throws SecurityException, ClassNotFoundException, JMException, IOException {
		for (int candidateId=1; candidateId<=30;candidateId++){
			String[] para = {Integer.toString(candidateId),
					Integer.toString(100),//populationSize
					Integer.toString(50000),//maxEvaluations
					Double.toString(0.07),//crossoverProbability
					Double.toString(0.01),	//mutationProbability
					"IBEA"//algorithmName
//					1  --algorithm IBEA --populationSize=100 --maxEvaluations=50000 --selectionOperator=KTournament --tournamentSize 2 --crossoverOperator=SinglePoint --crossoverProbability=0.07 --mutationOperator=BitFlipMutation --mutationProbability 0.01
//					2  --algorithm IBEA --populationSize=100 --maxEvaluations=50000 --selectionOperator=KTournament --tournamentSize 2 --crossoverOperator=SinglePoint --crossoverProbability=0.05 --mutationOperator=BitFlipMutation --mutationProbability 0.01
			};

			long estimatedTime = IBEA_main.main(para);
			// writing time for generated configuration solution
			String time="Time Taken For IBEA R"+(candidateId)+":-	 "+estimatedTime+"\n";
			System.out.println(time);
			new File("IBEATime.csv").createNewFile();
			Files.write(Paths.get("IBEATime.csv"), time.getBytes(), StandardOpenOption.APPEND);

//				Merging files
			String destinationFilePath="AllRecommenededConfigurationsIBEA.csv";
			if (!new File(destinationFilePath).exists()){
				new File(destinationFilePath).createNewFile();
			}
			MergeFiles.mergeTwoFiles("VAR_" + candidateId + ".csv", destinationFilePath);

			// Moving files
			File dest = new File("RUN " + candidateId + "/");
			if (!dest.exists()) {
				dest.mkdirs();
			}
			FileUtils.moveFileToDirectory(new File("VAR_" + candidateId), dest, false);
			FileUtils.moveFileToDirectory(new File("FUN_" + candidateId), dest, false);
			FileUtils.moveFileToDirectory(new File("VAR_" + candidateId + ".csv"), dest, false);
			System.out.println("Done Executing");
		}
	}

	private static void runNSGAII() throws SecurityException, ClassNotFoundException, JMException, IOException {
		for (int candidateId=1; candidateId<=30;candidateId++){
			String[] para = {Integer.toString(candidateId),
					Integer.toString(100),//populationSize
					Integer.toString(50000),//maxEvaluations
					Double.toString(0.85),//crossoverProbability
					Double.toString(0.76),	//mutationProbability
					"NSGAII"//algorithmName
//					1  --algorithm NSGA-II --populationSize=100 --maxEvaluations=50000 --selectionOperator=KTournament --tournamentSize 2 --crossoverOperator=SinglePoint --crossoverProbability=0.85 --mutationOperator=BitFlipMutation --mutationProbability 0.76
//					2  --algorithm NSGA-II --populationSize=100 --maxEvaluations=50000 --selectionOperator=KTournament --tournamentSize 2 --crossoverOperator=SinglePoint --crossoverProbability=0.28 --mutationOperator=BitFlipMutation --mutationProbability 0.64
//					3  --algorithm NSGA-II --populationSize=100 --maxEvaluations=50000 --selectionOperator=KTournament --tournamentSize 2 --crossoverOperator=SinglePoint --crossoverProbability=0.29 --mutationOperator=BitFlipMutation --mutationProbability 0.65
			};

			long estimatedTime = NSGAII_main.main(para);
			// writing time for generated configuration solution
			String time="Time Taken For NSGAII R"+(candidateId)+":-	 "+estimatedTime+"\n";
			System.out.println(time);
			new File("NSGAIITime.csv").createNewFile();
			Files.write(Paths.get("NSGAIITime.csv"), time.getBytes(), StandardOpenOption.APPEND);

//				Merging files
			String destinationFilePath="AllRecommenededConfigurationsNSGAII.csv";
			if (!new File(destinationFilePath).exists()){
				new File(destinationFilePath).createNewFile();
			}
			MergeFiles.mergeTwoFiles("VAR_" + candidateId + ".csv", destinationFilePath);

			// Moving files
			File dest = new File("RUN " + candidateId + "/");
			if (!dest.exists()) {
				dest.mkdirs();
			}
			FileUtils.moveFileToDirectory(new File("VAR_" + candidateId), dest, false);
			FileUtils.moveFileToDirectory(new File("FUN_" + candidateId), dest, false);
			FileUtils.moveFileToDirectory(new File("VAR_" + candidateId + ".csv"), dest, false);
			System.out.println("Done Executing");
		}
	}

	public static void main(String [] args) throws JMException, IOException, ClassNotFoundException {
		if (args.length>0){
			if (args[0].contains("NSGA-II"))
				runNSGAII();
			else if (args[0].contains("IBEA"))
				runIBEA();
			else if (args[0].contains("SPEA2"))
				runSPEA2();
			else if (args[0].contains("PAES"))
				runPAES();
			else if (args[0].contains("SMPSO"))
				runSMPSO();
			else if (args[0].contains("MoCell"))
				runMoCell();
		} else {
			System.out.println("Provide the Algorithm (NSGA-II, IBEA, SPEA2, PAES, SMPSO, MoCell) name to run ");
		}
	}
}
