package SBCR.Runner;

import br.ufpr.inf.gres.irace.core.HookRunCommands;
import br.ufpr.inf.gres.irace.core.IHookRun;
import br.ufpr.inf.gres.irace.runner.ExperimentAlgorithmRunner;
import jmetal.metaheuristics.ibea.IBEA;
import jmetal.metaheuristics.ibea.IBEA_main;
import jmetal.metaheuristics.mocell.MOCell_main;
import jmetal.metaheuristics.nsgaII.NSGAII_main;
import jmetal.metaheuristics.paes.PAES_main;
import jmetal.metaheuristics.smpso.SMPSO_main;
import jmetal.metaheuristics.spea2.SPEA2_main;
import jmetal.util.JMException;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

public class ParameterTunningExperimentRunner_Jitsi extends ExperimentAlgorithmRunner implements IHookRun {
	private void runMoCell(HookRunCommands jct) throws SecurityException, ClassNotFoundException, JMException, IOException {
		String[] para = {Integer.toString(jct.candidateId),
				Integer.toString(jct.populationSize),
				Integer.toString(jct.maxEvaluations),
				Double.toString(jct.crossoverProbability),
				Double.toString(jct.mutationProbability),
				jct.algorithmName
		};
		long estimatedTime = MOCell_main.main(para);
		//Writing all the information about run
		String information  = jct.candidateId + "	" + jct.algorithmName + "	" +
				estimatedTime + "	" + jct.populationSize + "	" + jct.maxEvaluations + "	" + jct.crossoverProbability +
				"	" +jct.mutationProbability + "\n";
		new File("InformationAboutRuns.csv").createNewFile();
		Files.write(Paths.get("InformationAboutRuns.csv"), information.getBytes(), StandardOpenOption.APPEND);
	}

	private void runSMPSO(HookRunCommands jct) throws SecurityException, ClassNotFoundException, JMException, IOException {
		String[] para = {Integer.toString(jct.candidateId),
				Integer.toString(jct.populationSize),
				Integer.toString(jct.maxEvaluations),
				Double.toString(jct.crossoverProbability),
				Double.toString(jct.mutationProbability),
				jct.algorithmName
		};
		long estimatedTime = SMPSO_main.main(para);
		//Writing all the information about run
		String information  = jct.candidateId + "	" + jct.algorithmName + "	" +
				estimatedTime + "	" + jct.populationSize + "	" + jct.maxEvaluations + "	" + jct.crossoverProbability +
				"	" +jct.mutationProbability + "\n";
		new File("InformationAboutRuns.csv").createNewFile();
		Files.write(Paths.get("InformationAboutRuns.csv"), information.getBytes(), StandardOpenOption.APPEND);
	}

	private void runPAES(HookRunCommands jct) throws SecurityException, ClassNotFoundException, JMException, IOException {
		String[] para = {Integer.toString(jct.candidateId),
				Integer.toString(jct.populationSize),
				Integer.toString(jct.maxEvaluations),
				Double.toString(jct.crossoverProbability),
				Double.toString(jct.mutationProbability),
				jct.algorithmName
		};
		long estimatedTime = PAES_main.main(para);
		//Writing all the information about run
		String information  = jct.candidateId + "	" + jct.algorithmName + "	" +
				estimatedTime + "	" + jct.populationSize + "	" + jct.maxEvaluations + "	" + jct.crossoverProbability +
				"	" +jct.mutationProbability + "\n";
		new File("InformationAboutRuns.csv").createNewFile();
		Files.write(Paths.get("InformationAboutRuns.csv"), information.getBytes(), StandardOpenOption.APPEND);
	}

	private void runSPEA2(HookRunCommands jct) throws SecurityException, ClassNotFoundException, JMException, IOException {
		String[] para = {Integer.toString(jct.candidateId),
				Integer.toString(jct.populationSize),
				Integer.toString(jct.maxEvaluations),
				Double.toString(jct.crossoverProbability),
				Double.toString(jct.mutationProbability),
				jct.algorithmName
		};
		long estimatedTime = SPEA2_main.main(para);
		//Writing all the information about run
		String information  = jct.candidateId + "	" + jct.algorithmName + "	" +
				estimatedTime + "	" + jct.populationSize + "	" + jct.maxEvaluations + "	" + jct.crossoverProbability +
				"	" +jct.mutationProbability + "\n";
		new File("InformationAboutRuns.csv").createNewFile();
		Files.write(Paths.get("InformationAboutRuns.csv"), information.getBytes(), StandardOpenOption.APPEND);
	}

	private void runIBEA(HookRunCommands jct) throws SecurityException, ClassNotFoundException, JMException, IOException {
		String[] para = {Integer.toString(jct.candidateId),
				Integer.toString(jct.populationSize),
				Integer.toString(jct.maxEvaluations),
				Double.toString(jct.crossoverProbability),
				Double.toString(jct.mutationProbability),
				jct.algorithmName
		};
		long estimatedTime = IBEA_main.main(para);
		//Writing all the information about run
		String information  = jct.candidateId + "	" + jct.algorithmName + "	" +
				estimatedTime + "	" + jct.populationSize + "	" + jct.maxEvaluations + "	" + jct.crossoverProbability +
				"	" +jct.mutationProbability + "\n";
		new File("InformationAboutRuns.csv").createNewFile();
		Files.write(Paths.get("InformationAboutRuns.csv"), information.getBytes(), StandardOpenOption.APPEND);
	}

	public static void runNSGAII(HookRunCommands jct) throws SecurityException, ClassNotFoundException, JMException, IOException {
		String[] para = {Integer.toString(jct.candidateId),
				Integer.toString(jct.populationSize),
				Integer.toString(jct.maxEvaluations),
				Double.toString(jct.crossoverProbability),
				Double.toString(jct.mutationProbability),
				jct.algorithmName
		};
		long estimatedTime = NSGAII_main.main(para);
		//Writing all the information about run
		String information  = jct.candidateId + "	" + jct.algorithmName + "	" +
				estimatedTime + "	" + jct.populationSize + "	" + jct.maxEvaluations + "	" + jct.crossoverProbability +
				"	" +jct.mutationProbability + "\n";
			new File("InformationAboutRuns.csv").createNewFile();
			Files.write(Paths.get("InformationAboutRuns.csv"), information.getBytes(), StandardOpenOption.APPEND);
	}

	@Override
	public void run(HookRunCommands jct){
		try {
			new File(jct.algorithmName+"_Complete_Log.txt").createNewFile();
			Files.write(Paths.get(jct.algorithmName+"_Complete_Log.txt"), (jct.toString()+"\n\n").getBytes(), StandardOpenOption.APPEND);
			if (jct.algorithmName.contains("NSGA-II"))
				runNSGAII(jct);
			else if (jct.algorithmName.contains("IBEA"))
				runIBEA(jct);
			else if (jct.algorithmName.contains("SPEA2"))
				runSPEA2(jct);
			else if (jct.algorithmName.contains("PAES"))
				runPAES(jct);
			else if (jct.algorithmName.contains("SMPSO"))
				runSMPSO(jct);
			else if (jct.algorithmName.contains("MoCell"))
				runMoCell(jct);
		} catch (JMException|IOException|ClassNotFoundException e){
			System.err.println(e);
		}
	}


	public static void main(String [] args) throws JMException, IOException, ClassNotFoundException {
		String[] para = {"1","10","50","0.9","0.1", "MoCell"};
		long estimatedTime = IBEA_main.main(para);
	}

}
