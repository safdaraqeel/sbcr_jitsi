package jmetal.util.point.util;

public interface PointDistance {
	  public double compute(double[] trueParetoFront, double[] front) ; 
	}
