package jmetal.qualityIndicator.AllIndicators;

public class RunAllIIndicators {

	public static void main(String[] args) throws Exception {
		GenerateReferenceFront.main(args); //generating reference front
		Hypervolume_4Objectives.main(args);// Calculating HV
		Epsilon_4Objectives.main(args);// Calculating Epsilon
		GeneralizedSpread_4Objectives.main(args);// Calculating GS
		GenerationalDistance_4Objectives.main(args);// Calculating GD
		InvertedGenerationalDistance_4Objectives.main(args);// Calculating IGD
		ParetoFrontSize.main(args);// Calculating PFS
		EuclideanDistance_4Objectives.main(args);// Calculating ED

	}

}
