//  GenerationalDistance.java
//
//  Author:
//       Antonio J. Nebro <antonio@lcc.uma.es>
//       Juan J. Durillo <durillo@lcc.uma.es>
//
//  Copyright (c) 2011 Antonio J. Nebro, Juan J. Durillo
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

package jmetal.qualityIndicator.AllIndicators;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

// not done
/**
 * This class implements the generational distance indicator. It can be used
 * also as a command line by typing: "java
 * jmetal.qualityIndicator.GenerationalDistance <solutionFrontFile>
 * <trueFrontFile> <getNumberOfObjectives>" Reference: Van Veldhuizen, D.A.,
 * Lamont, G.B.: Multiobjective Evolutionary Algorithm Research: A History and
 * Analysis. Technical Report TR-98-03, Dept. Elec. Comput. Eng., Air Force
 * Inst. Technol. (1998)
 */
public class EuclideanDistance_4Objectives {
	public jmetal.qualityIndicator.util.MetricsUtil utils_; // utils_ is used to access to the
	// MetricsUtil funcionalities
	static int numberCases = 1000;
	static final double pow_ = 2.0; // pow

	private static File fileName, fileTotalName;
	private static BufferedWriter file, fileTotal;

	/**
	 * Constructor. Creates a new instance of the generational distance metric.
	 */
	public EuclideanDistance_4Objectives() {
		utils_ = new jmetal.qualityIndicator.util.MetricsUtil();
	} // GenerationalDistance

	/**
	 * Returns the generational distance value for a given front
	 * 
	 * @param front
	 *            The front
	 * @param trueParetoFront
	 *            The true pareto front
	 */
	public double euclideanDistance(double[][] front, double[][] trueParetoFront, int numberOfObjectives) {

		/**
		 * Stores the maximum values of true pareto front.
		 */
		double[] maximumValue = { 1, 1, 1, 1 };

		/**
		 * Stores the minimum values of the true pareto front.
		 */
		double[] minimumValue = { 0, 0, 0, 0 };

		/**
		 * Stores the normalized front.
		 */
		double[][] normalizedFront;

		/**
		 * Stores the normalized true Pareto front.
		 */
		double[][] normalizedParetoFront;

		// STEP 2. Get the normalized front and true Pareto fronts
		normalizedFront = utils_.getNormalizedFront(front, maximumValue, minimumValue);
		normalizedParetoFront = utils_.getNormalizedFront(trueParetoFront, maximumValue, minimumValue);

		// STEP 3. Sum the distances between each point of the front and the
		// nearest point in the true Pareto front
		double lowestDistance = 1000;
		for (int i = 0; i < front.length; i++) {
			double distance = utils_.distanceToClosedPoint(normalizedFront[i], normalizedParetoFront);
			if (lowestDistance > distance)
				lowestDistance = distance;
		}

		return lowestDistance;
	} // generationalDistance

	/**
	 * This class can be invoqued from the command line. Two params are required: 1)
	 * the name of the file containing the front, and 2) the name of the file
	 * containig the true Pareto front
	 * 
	 * @throws Exception
	 **/

	public static void main(String args[]) throws Exception {

		int numberObjectives = 4;
		int totalRuns = 30;
		String[] algorithmNameList_ = new String[] {"NSGA2","IBEA","MoCell","SPEA2","SMPSO","PAES","RS"};
		String inputHomePath = "ExperimentResultsWithBP/";
		String nonDominated = "AnalysisResults/Indicators/ReferenceFront/nondominated";
		String outputHomePath = "AnalysisResults/Indicators/";
		String caseStudy= "Jitsi_";

		double[][] allHVs = new double[totalRuns][algorithmNameList_.length];
		String targetFilePath = outputHomePath + "ED.csv";

		for (int algorithmIndex = 0; algorithmIndex < algorithmNameList_.length; algorithmIndex++) { // algorithms loop
			for (int runNumber = 0; runNumber < totalRuns; runNumber++) { // runs loop

				// constructing the sourceFilePath
				String sourceFilePath = inputHomePath + caseStudy + algorithmNameList_[algorithmIndex] + "/RUN " + (runNumber+1) +"/FUN_" + (runNumber+1);
				System.out.println(sourceFilePath);

				// reading solutionFront from file
				double[][] solutionFront = readFile(sourceFilePath);
				double[][] trueFront=readFile(nonDominated);

				// Obtain delta value
				double value = new EuclideanDistance_4Objectives().euclideanDistance(solutionFront, trueFront, numberObjectives);
				System.out.println(value + "\t");
				allHVs[runNumber][algorithmIndex] = value;
			}
		}

		// saving indicator values to the file
		String data = "NSGA-II;IBEA;MoCell;SPEA2;SMPSO;PAES;RS;X\n";
		for (int runNumber = 0; runNumber < totalRuns; runNumber++) {
			for (int algorithmIndex = 0; algorithmIndex < algorithmNameList_.length; algorithmIndex++) { // algorithms
				// loop
				data += allHVs[runNumber][algorithmIndex] + ";";
			}
			data += "\n";
		}

		Files.write(Paths.get(targetFilePath), data.getBytes());

	} // main

	public static double[][] readFile(String sourceFilePath) {
		try {
			String FintessValues = new String(Files.readAllBytes(Paths.get(sourceFilePath)), StandardCharsets.UTF_8);
			String[] allSolutions = FintessValues.split("\n");
			double[][] solutionFront = new double[allSolutions.length][FintessValues.split("\n")[0].split(" ").length];
			for (int solNum = 0; solNum < allSolutions.length; solNum++) {
				String[] objectiveValues = allSolutions[solNum].split(" ");
				for (int objNum = 0; objNum < objectiveValues.length; objNum++) {
					solutionFront[solNum][objNum] = Double.parseDouble(objectiveValues[objNum]);
				}
			}
			return solutionFront;
		} catch (IOException e) {

		}
		return null;
	}




} // EuclideanDistance


	

