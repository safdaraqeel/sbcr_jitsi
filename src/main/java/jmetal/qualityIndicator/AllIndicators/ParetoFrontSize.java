package jmetal.qualityIndicator.AllIndicators;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

public class ParetoFrontSize {

	public static void main(String args[]) throws Exception {
		int numberObjectives = 4;
		int totalRuns = 30;


		String[] algorithmNameList_ = new String[] {"NSGA2","IBEA","MoCell","SPEA2","SMPSO","PAES","RS"};
		String inputHomePath = "ExperimentResultsWithBP/";
		String outputHomePath = "AnalysisResults/Indicators/";
		String caseStudy= "Jitsi_";

		double[][] allHVs = new double[totalRuns][algorithmNameList_.length];
		String targetFilePath = outputHomePath + "PFS.csv";

		for (int algorithmIndex = 0; algorithmIndex < algorithmNameList_.length; algorithmIndex++) { // algorithms loop
			for (int runNumber = 0; runNumber < totalRuns; runNumber++) { // runs loop

				// constructing the sourceFilePath
				String sourceFilePath = inputHomePath + caseStudy + algorithmNameList_[algorithmIndex] + "/RUN " + (runNumber+1) +"/FUN_" + (runNumber+1);
				System.out.println(sourceFilePath);

				// Obtain PFS value
				int value = getPFS( sourceFilePath,  numberObjectives);
				System.out.println(value + "\t");
				allHVs[runNumber][algorithmIndex] = value;
			}
		}

		// saving indicator values to the file
		String data = "NSGA-II;IBEA;MoCell;SPEA2;SMPSO;PAES;RS;X\n";
		for (int runNumber = 0; runNumber < totalRuns; runNumber++) {
			for (int algorithmIndex = 0; algorithmIndex < algorithmNameList_.length; algorithmIndex++) { // algorithms
																											// loop
				data += allHVs[runNumber][algorithmIndex] + ";";
			}
			data += "\n";
		}

		Files.write(Paths.get(targetFilePath), data.getBytes());

	} // main

	public static int getPFS(String sourceFilePath, int numberOfObjectives) {
		int count=0;
		try {
			String FintessValues = new String(Files.readAllBytes(Paths.get(sourceFilePath)), StandardCharsets.UTF_8);
//			System.out.println(FintessValues);
			FintessValues=FintessValues.replaceAll("[*a-zA-Z]", "");
			String[] allSolutions = FintessValues.split("\n");
			for (int i = 0; i < allSolutions.length; i++) {
				if (allSolutions[i].split(" ").length==numberOfObjectives) {
					// or use this "	"
					count++;
				}
			}
		} catch (IOException e) {

		}
		return count;
	}

	
	
	
	
	
	
	
	
	
	

} // PFS
