//  GeneralizedSpread.java
//
//  Author:
//       Antonio J. Nebro <antonio@lcc.uma.es>
//       Juan J. Durillo <durillo@lcc.uma.es>
//
//  Copyright (c) 2011 Antonio J. Nebro, Juan J. Durillo
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

package jmetal.qualityIndicator.AllIndicators;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;

/**
 * This class implements the generalized spread metric for two or more
 * dimensions. It can be used also as command line program just by typing. $
 * java jmetal.qualityIndicator.GeneralizedSpread <solutionFrontFile>
 * <trueFrontFile> <getNumberOfObjectives> Reference: A. Zhou, Y. Jin, Q. Zhang,
 * B. Sendhoff, and E. Tsang Combining model-based and genetics-based offspring
 * generation for multi-objective optimization using a convergence criterion,
 * 2006 IEEE Congress on Evolutionary Computation, 2006, pp. 3234-3241.
 */
public class GeneralizedSpread_4Objectives {

	public static jmetal.qualityIndicator.util.MetricsUtil utils_;

	// utilities for implementing
	// the metric

	/**
	 * Constructor Creates a new instance of GeneralizedSpread
	 */
	public GeneralizedSpread_4Objectives() {
		utils_ = new jmetal.qualityIndicator.util.MetricsUtil();
	} // GeneralizedSpread

	/**
	 * Calculates the generalized spread metric. Given the pareto front, the true
	 * pareto front as <code>double []</code> and the number of objectives, the
	 * method return the value for the metric.
	 * 
	 * @param paretoFront
	 *            The pareto front.
	 * @param paretoTrueFront
	 *            The true pareto front.
	 * @param numberOfObjectives
	 *            The number of objectives.
	 * @return the value of the generalized spread metric
	 **/
	public double generalizedSpread(double[][] paretoFront, double[][] paretoTrueFront, int numberOfObjectives) {

		/**
		 * Stores the maximum values of true pareto front.
		 */
		double[] maximumValue = { 1, 1, 1, 1 };

		/**
		 * Stores the minimum values of the true pareto front.
		 */
		double[] minimumValue = { 0, 0, 0, 0 };

		/**
		 * Stores the normalized front.
		 */
		double[][] normalizedFront;

		/**
		 * Stores the normalized true Pareto front.
		 */
		double[][] normalizedParetoFront;

		// // STEP 1. Obtain the maximum and minimum values of the Pareto front
		// maximumValue =
		// utils_.getMaximumValues(paretoTrueFront,numberOfObjectives);
		// minimumValue =
		// utils_.getMinimumValues(paretoTrueFront,numberOfObjectives);

		normalizedFront = utils_.getNormalizedFront(paretoFront, maximumValue, minimumValue);

		// STEP 2. Get the normalized front and true Pareto fronts
		normalizedParetoFront = utils_.getNormalizedFront(paretoTrueFront, maximumValue, minimumValue);

		// STEP 3. Find extremal values
		double[][] extremValues = new double[numberOfObjectives][numberOfObjectives];
		for (int i = 0; i < numberOfObjectives; i++) {
			Arrays.sort(normalizedParetoFront, new jmetal.qualityIndicator.util.ValueComparator(i));
			System.arraycopy(normalizedParetoFront[normalizedParetoFront.length - 1], 0, extremValues[i], 0,
					numberOfObjectives);
		}

		int numberOfPoints = normalizedFront.length;
		int numberOfTruePoints = normalizedParetoFront.length;

		// STEP 4. Sorts the normalized front
		Arrays.sort(normalizedFront, new jmetal.qualityIndicator.util.LexicoGraphicalComparator());

		// STEP 5. Calculate the metric value. The value is 1.0 by default
		if (utils_.distance(normalizedFront[0], normalizedFront[normalizedFront.length - 1]) == 0.0) {
			return 1.0;
		} else {

			double dmean = 0.0;

			// STEP 6. Calculate the mean distance between each point and its
			// nearest neighbor
			for (double[] aNormalizedFront : normalizedFront) {
				dmean += utils_.distanceToNearestPoint(aNormalizedFront, normalizedFront);
			}

			dmean = dmean / (numberOfPoints);

			// STEP 7. Calculate the distance to extremal values
			double dExtrems = 0.0;
			for (double[] extremValue : extremValues) {
				dExtrems += utils_.distanceToClosedPoint(extremValue, normalizedFront);
			}

			// STEP 8. Computing the value of the metric
			double mean = 0.0;
			for (double[] aNormalizedFront : normalizedFront) {
				mean += Math.abs(utils_.distanceToNearestPoint(aNormalizedFront, normalizedFront) - dmean);
			}

			double value = (dExtrems + mean) / (dExtrems + (numberOfPoints * dmean));
			return value;

		}
	} // generalizedSpread

	/**
	 * This class can be invoked from the command line. Three params are required:
	 * 1) the name of the file containing the front, 2) the name of the file
	 * containig the true Pareto front 3) the number of objectives
	 * 
	 * @throws Exception
	 */
	

	public static void main(String args[]) throws Exception {
		int numberObjectives = 4;
		int totalRuns = 30;
		String[] algorithmNameList_ = new String[] {"NSGA2","IBEA","MoCell","SPEA2","SMPSO","PAES","RS"};
		String inputHomePath = "ExperimentResultsWithBP/";
		String nonDominated = "AnalysisResults/Indicators/ReferenceFront/nondominated";
		String outputHomePath = "AnalysisResults/Indicators/";
		String caseStudy= "Jitsi_";
		
		
		double[][] allHVs = new double[totalRuns][algorithmNameList_.length];
		String targetFilePath = outputHomePath + "GS.csv";

		for (int algorithmIndex = 0; algorithmIndex < algorithmNameList_.length; algorithmIndex++) { // algorithms loop
			for (int runNumber = 0; runNumber < totalRuns; runNumber++) { // runs loop

				// constructing the sourceFilePath
				String sourceFilePath = inputHomePath + caseStudy + algorithmNameList_[algorithmIndex] + "/RUN " + (runNumber+1) +"/FUN_" + (runNumber+1);
				System.out.println(sourceFilePath);

				// reading solutionFront from file
				double[][] solutionFront = readFile(sourceFilePath);
				double[][] trueFront=readFile(nonDominated);
				
				
				// Obtain delta value
				double value = new GeneralizedSpread_4Objectives().generalizedSpread(solutionFront, trueFront, numberObjectives);
				
				
				System.out.println(value + "\t");
				allHVs[runNumber][algorithmIndex] = value;
			}
		}

		// saving indicator values to the file
		String data = "NSGA-II;IBEA;MoCell;SPEA2;SMPSO;PAES;RS;X\n";
		for (int runNumber = 0; runNumber < totalRuns; runNumber++) {
			for (int algorithmIndex = 0; algorithmIndex < algorithmNameList_.length; algorithmIndex++) { // algorithms
																											// loop
				data += allHVs[runNumber][algorithmIndex] + ";";
			}
			data += "\n";
		}

		Files.write(Paths.get(targetFilePath), data.getBytes());

	} // main

	public static double[][] readFile(String sourceFilePath) {
		try {
			String FintessValues = new String(Files.readAllBytes(Paths.get(sourceFilePath)), StandardCharsets.UTF_8);
			String[] allSolutions = FintessValues.split("\n");
			double[][] solutionFront = new double[allSolutions.length][FintessValues.split("\n")[0].split(" ").length];
			for (int solNum = 0; solNum < allSolutions.length; solNum++) {
				String[] objectiveValues = allSolutions[solNum].split(" ");
				for (int objNum = 0; objNum < objectiveValues.length; objNum++) {
					solutionFront[solNum][objNum] = Double.parseDouble(objectiveValues[objNum]);
				}
			}
			return solutionFront;
		} catch (IOException e) {

		}
		return null;
	}
	
} // GeneralizedSpread
