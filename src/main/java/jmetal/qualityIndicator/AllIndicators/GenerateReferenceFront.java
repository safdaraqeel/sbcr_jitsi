package jmetal.qualityIndicator.AllIndicators;

import jmetal.qualityIndicator.util.MetricsUtil;
import jmetal.util.NonDominatedSolutionList;

import java.io.IOException;

public class GenerateReferenceFront {

	private static String inputHomePath = "ExperimentResultsWithBP/";
	private static String outputHomePath = "AnalysisResults/Indicators/ReferenceFront/";
	private static String caseStudy = "Jitsi_";
	private static String [] algorithmNameList_ = {"NSGA2","IBEA","MoCell","SPEA2","SMPSO","PAES","RS"};

//	private static String [] algorithmNameList_ = {"NSGA2","IBEA","MoCell","SPEA2","SMPSO","PAES"};
	int independentRuns_ = 30;

	public void generateReferenceFronts() {
		generateReferenceFront();
	}

	private void generateReferenceFront() {
		String frontPath_ = outputHomePath + "nondominated";

		MetricsUtil metricsUtils = new MetricsUtil();
		NonDominatedSolutionList solutionSet = new NonDominatedSolutionList();
		for (int algorithmIndex = 0; algorithmIndex < algorithmNameList_.length; algorithmIndex++) {
			for (int runNumber = 1; runNumber <= independentRuns_; runNumber++) {
				String outputParetoFrontFilePath = inputHomePath + caseStudy+ algorithmNameList_[algorithmIndex] + "/RUN " + runNumber + "/FUN_" + runNumber;
				String solutionFrontFile = outputParetoFrontFilePath;

				metricsUtils.readNonDominatedSolutionSet(solutionFrontFile, solutionSet);
			} // for
		} // for
		solutionSet.printObjectivesToFile(frontPath_);
		System.out.println("Reference front is generated, see: "+frontPath_);
	}

	public static void main(String[] args) throws IOException {
		GenerateReferenceFront qi = new GenerateReferenceFront();
		qi.generateReferenceFronts();
	}

}
