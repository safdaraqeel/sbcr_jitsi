//  PAES.main
//
//  Author:
//       Antonio J. Nebro <antonio@lcc.uma.es>
//       Juan J. Durillo <durillo@lcc.uma.es>
//
//  Copyright (c) 2011 Antonio J. Nebro, Juan J. Durillo
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

package jmetal.metaheuristics.paes;

import SBCR.CG.ConfigRecommendationProblem;
import SBCR.CG.Decoding;
import jmetal.core.Algorithm;
import jmetal.core.Operator;
import jmetal.core.Problem;
import jmetal.core.SolutionSet;
import jmetal.operators.mutation.MutationFactory;
import jmetal.problems.ProblemFactory;
import jmetal.qualityIndicator.QualityIndicator;
import jmetal.util.Configuration;
import jmetal.util.JMException;

import java.io.IOException;
import java.util.HashMap;
import java.util.logging.FileHandler;
import java.util.logging.Logger;

/**
 * Class for configuring and running the PAES algorithm
 */
public class PAES_main {
  public static Logger      logger_ ;      // Logger object
  public static FileHandler fileHandler_ ; // FileHandler object

  /**
   * @param args Command line arguments. The first (optional) argument specifies 
   *             the problem to solve.
 * @return 
   * @throws JMException
   * @throws IOException 
   * @throws SecurityException 
   * Usage: three options
   *      - jmetal.metaheuristics.mocell.MOCell_main
   *      - jmetal.metaheuristics.mocell.MOCell_main problemName
   *      - jmetal.metaheuristics.mocell.MOCell_main problemName ParetoFrontFile
   */
  public static long main(String [] args) throws JMException, IOException, ClassNotFoundException {
    Problem problem   ;         // The problem to solve
    Algorithm algorithm ;         // The algorithm to use
    Operator mutation  ;         // Mutation operator
    QualityIndicator indicators ; // Object to get quality indicators
    HashMap  parameters ; // Operator parameters

    // Logger object and file to store log messages
    logger_      = Configuration.logger_ ;
    fileHandler_ = new FileHandler("PAES_main.log");
    logger_.addHandler(fileHandler_) ;
    
    indicators = null ;
    problem = new ConfigRecommendationProblem("Integer", 39);
    algorithm = new PAES(problem);

    //Algorithm parameters initialization
    int candidateId = 0, populationSize = 0, maxEvaluations = 0;
    double crossoverProbability = 0, mutationProbability = 0;
    String algorithmName = "";
    if (args[0]!=null)
      candidateId = Integer.parseInt(args[0]);
    if (args[1]!=null)
      populationSize = Integer.parseInt(args[1]);
    if (args[2]!=null)
      maxEvaluations = Integer.parseInt(args[2]);
    if (args[3]!=null)
      crossoverProbability = Double.parseDouble(args[3]);
    if (args[4]!=null)
      mutationProbability = Double.parseDouble(args[4]);
    if (args[5]!=null)
      algorithmName = args[5];
    
    algorithm.setInputParameter("archiveSize",populationSize);
    algorithm.setInputParameter("biSections",5);
    algorithm.setInputParameter("maxEvaluations",maxEvaluations);
      
    // Mutation (Real variables)
    parameters = new HashMap() ;
    parameters.put("probability", crossoverProbability) ;
    parameters.put("distributionIndex", 20.0) ;
    mutation = MutationFactory.getMutationOperator("BitFlipMutation", parameters);

    // Add the operators to the algorithm
    algorithm.addOperator("mutation", mutation);
    
    // Execute the Algorithm 
    long initTime = System.currentTimeMillis();
    SolutionSet population = algorithm.execute();
    long estimatedTime = System.currentTimeMillis() - initTime;

    // Print the results
    population.printVariablesToFile("VAR_" + candidateId);
    //    logger_.info("Objectives values have been writen to file FUN");
    population.printObjectivesToFile("FUN_" + candidateId);
  
    if (indicators != null) {
      logger_.info("Quality indicators") ;
      logger_.info("Hypervolume: " + indicators.getHypervolume(population)) ;
      logger_.info("GD         : " + indicators.getGD(population)) ;
      logger_.info("IGD        : " + indicators.getIGD(population)) ;
      logger_.info("Spread     : " + indicators.getSpread(population)) ;
      logger_.info("Epsilon    : " + indicators.getEpsilon(population)) ;  
    } // if

  //System.out.println("Total Time taken for generating configurations is:-"+estimatedTime);
    Decoding.DecodeAllConfigurationsInFile( "VAR_" + candidateId, "VAR_" + candidateId + ".csv");
    return estimatedTime;
  }//main
} // PAES_main
